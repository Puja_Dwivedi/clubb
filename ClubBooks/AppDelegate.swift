//
//  AppDelegate.swift
//  ClubBooks
//
//  Created by cis on 20/11/18.
//  Copyright © 2018 CIS. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase
import FirebaseDatabase
import FirebaseMessaging
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
#if DEBUG
    let stripePublishedKey = "pk_test_YzHjfsjZmet7GtyMYK34mRy9"
    let merchantID = "merchant.com.juan.development"
#else
    let stripePublishedKey = "https://buy.itunes.apple.com/verifyReceipt"
    let merchantID = ""
#endif
    
    static let shared =  UIApplication.shared.delegate as! AppDelegate
    var langId : String = LangId.english
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        if AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) == nil{
            ManageLocalization.deviceLang = "en"
        }else{
            ManageLocalization.deviceLang = RV_CheckDataType.getString(anyString: AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as AnyObject)
        }
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        //
        self.registerForPushNotifications(application)
        //and then
        if launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] != nil {
            let data = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification]
            print(data as Any)
        }
        //        STPPaymentConfiguration.shared.appleMerchantIdentifier =  merchantID
        //        STPPaymentConfiguration.shared.publishableKey = stripePublishedKey
        FirebaseApp.configure()
        Database.database().isPersistenceEnabled = true
        Messaging.messaging().delegate = self
        //Firebase Setup
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        let topWindow: UIWindow? = UIWindow(frame: UIScreen.main.bounds)
        topWindow?.rootViewController = UIViewController()
        topWindow?.windowLevel = UIWindow.Level.alert + 1
        application.registerForRemoteNotifications()
        let remoteNotif = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [String: Any]
        if remoteNotif != nil {
            if RV_CheckDataType.getString(anyString: remoteNotif!["chatID"] as AnyObject) != RV_CheckDataType.getString(anyString: AppTheme.sharedInstance.currentChatID as AnyObject){
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                initialViewController.isNotificationLaunch = "1"
                initialViewController.notifcaitonObject = remoteNotif!
                self.window?.rootViewController = initialViewController
                self.window?.makeKeyAndVisible()
            }
        }
        
        if let option = launchOptions {
            let info = option[UIApplication.LaunchOptionsKey.remoteNotification]
                 if (info != nil) {
                    self.openChat()
             }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.goToVc(notification:)), name:NSNotification.Name("chat_notify"), object: nil)
        
        return true
    }
    

    
    @objc func goToVc(notification:Notification) {
        openChat()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        HSRealTimeMessagingLIbrary.function_UpdateOnlineStatus(HSRealTimeMessagingLIbrary.HSCurrentUserID(), false)
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        //HSRealTimeMessagingLIbrary.function_UpdateOnlineStatus(UInt64(AppTheme.sharedInstance.currentUserData[0].UserID) ?? 0, true)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        let handled: Bool = ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        // Add any custom logic here.
        return handled
    }
    func logoutUser(){
        if let topController = UIApplication.topViewController() {
            let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
            let IntroViewController = storyboard.instantiateViewController(withIdentifier: "IntroViewController") as! IntroViewController
            IntroViewController.hidesBottomBarWhenPushed = true
            topController.navigationController?.pushViewController(IntroViewController, animated: false)
        }
    }
    
    func openChat() {
        let vc = Class_StageTwo.init(nibName: "Class_StageTwo", bundle: Bundle.main)
        let chatID = HSRealTimeMessagingLIbrary.function_CreatChatID(147, UInt64(AppTheme.sharedInstance.currentUserData[0].UserID) ?? 0)
        vc.chatDetail = HSChatUser.init(isChatID: chatID, isID: 147, isName: "Rahul")
//        self.navigationController?.pushViewController(vc, animated: true)
        window?.rootViewController = vc
        window?.makeKeyAndVisible()
    }
    
    /*
     guard let window = UIApplication.shared.keyWindow else { return }

       let storyboard = UIStoryboard(name: "YourStoryboard", bundle: nil)
       let yourVC = storyboard.instantiateViewController(identifier: "yourVCIdentifier")
       
       let navController = UINavigationController(rootViewController: yourVC)
       navController.modalPresentationStyle = .fullScreen

       // you can assign your vc directly or push it in navigation stack as follows:
       window.rootViewController = navController
       window.makeKeyAndVisible()
     */
    
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        //        if let messageID = userInfo[gcmMessageIDKey] {
        //            print("Message ID: \(messageID)")
        //        }
        //
        //        // Print full message.
        //        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    //MARK:- Apple Push Notification
    func registerForPushNotifications(_ application: UIApplication) {
        // iOS 10 support
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
        // iOS 9 support
        else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        // iOS 8 support
        //        else if #available(iOS 8, *) {
        //            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
        //            UIApplication.shared.registerForRemoteNotifications()
        //        }
        // iOS 7 support
        else {
            application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }
    }
    
    //Mark:- Called when APNs has assigned the device a unique token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        // Print it to console
        print("APNs device token: \(deviceTokenString)")
        
        AppTheme.sharedInstance.strDeviceToken = deviceTokenString as String
    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        
        AppTheme.sharedInstance.strDeviceToken = "1234567890"
        print("APNs registration failed: \(error)")
    }
    
    // Push notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        // Print notification payload data
        print("Push notification received: \(userInfo)")
        
        if application.applicationState == .background || application.applicationState ==  .inactive
        {
        }
        else
        {
            let aps = RV_CheckDataType.getDictionary(anyDict: userInfo["aps"] as AnyObject)
            if aps.count > 0
            {
                if let alert = aps.value(forKey: "alert") as? NSDictionary
                {
                    let message = RV_CheckDataType.getString(anyString: alert.value(forKey: "body") as AnyObject)
                    let title = RV_CheckDataType.getString(anyString: alert.value(forKey: "title") as AnyObject)
                    let alert = UIAlertController(title: title, message:message, preferredStyle: UIAlertController.Style.alert)
                    // add an action (button)
                    alert.addAction(UIAlertAction(title: "alert.ok".localized, style: UIAlertAction.Style.default, handler: nil))
                    // show the alert
                    self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
}
extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        AppTheme.sharedInstance.fcmToken = fcmToken
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}


extension AppDelegate : UNUserNotificationCenterDelegate
{
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        if RV_CheckDataType.getString(anyString: notification.request.content.userInfo["chatID"] as AnyObject) != RV_CheckDataType.getString(anyString: AppTheme.sharedInstance.currentChatID as AnyObject){
            completionHandler([.alert, .badge, .sound])
        } else {
            completionHandler([.alert, .badge, .sound])
        }
        
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        //        if RV_CheckDataType.getString(anyString: response.notification.request.content.userInfo["chatID"] as AnyObject) != RV_CheckDataType.getString(anyString: AppTheme.sharedInstance.currentChatID as AnyObject){
        //            if let isNc = self.window?.rootViewController as? LYTabBar{
        //                isNc.selectedIndex = 3
        //                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
        //                    NotificationCenter.default.post(name: Notification.Name("firebaseChatNotifications"), object: nil, userInfo: response.notification.request.content.userInfo)
        //                })
        //            }
        //        }
        if let isNc = self.window?.rootViewController as? LYTabBar {
            isNc.selectedIndex = 3
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                NotificationCenter.default.post(name: Notification.Name("firebaseChatNotifications"), object: nil, userInfo: response.notification.request.content.userInfo)
            })
        } else {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let homeVC = storyboard.instantiateViewController(withIdentifier: "LYTabBar") as! LYTabBar
            homeVC.selectedIndex = 3
            appDelegate.window?.rootViewController = homeVC
            NotificationCenter.default.post(name: Notification.Name("firebaseChatNotifications"), object: nil, userInfo: response.notification.request.content.userInfo)
        }
    }
}

