//
//  FriendProfileVC.swift
//  ClubBooks
//
//  Created by cis on 15/05/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit
import MBProgressHUD

enum Gender : String {
    case male = "Male"
    case female = "Female"
    case other = "other"
}

class ProfileVC : BaseViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate  {
    @IBOutlet var viewProfileDetail : UIView!
    //    @IBOutlet var viewProfileBackground : UIView!
    @IBOutlet var tblDetails : UITableView!
    @IBOutlet var collPost : UICollectionView!
    @IBOutlet var lblSchoolName : UILabel!
    @IBOutlet var lblUserName : UILabel!
    @IBOutlet var lblFavBook : UILabel!
    @IBOutlet var btnFollow : UIButton!
    @IBOutlet var btnFriends : UIButton!
    @IBOutlet var btnFollowers : UIButton!
    @IBOutlet var btnFollowing : UIButton!
    @IBOutlet var btnNotification : UIButton!
    @IBOutlet var viewRating : CosmosView!
    
    @IBOutlet var heightCollView : NSLayoutConstraint!
    
    
    @IBOutlet var tblViewHozizontalWidth : NSLayoutConstraint!
    
    var dicLabelsTitles = [String : [String]]()
    var arrPostTypes = ["books", "arrclassnotes", "exams", "arrquizes", "meetups", "chatonbooks", "chatsontopic"]
    var intPagenumber : Int = 1
    var arrayPost = [PostDetails]()
    var listType : ListType = ListType.init(rawValue: 1)!
    
    var strUserID : String!
    var strUserName : String!
    var friendProfile = FriendProfileDetails()
    var selectedSection : Int!
    var isPreviousViewController = false
    
    @IBOutlet var tblPostTitlesHeaderView : UIView!
    @IBOutlet var collPostTitles : UICollectionView!
    @IBOutlet var scrollViewPosts : UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
        //--Setting navigation Bar
        
        showSettingButton()
        viewProfileDetail.layer.cornerRadius = viewProfileDetail.frame.size.height/2
        viewProfileDetail.layer.borderColor = .none
        viewProfileDetail.layer.borderWidth = 2

        self.strUserID = AppTheme.sharedInstance.currentUserData[0].UserID
        self.navigationItem.title = "nav.Profile".localized.uppercased()
        
        let nibBook = UINib.init(nibName: "DetailCell", bundle: nil)
        self.tblDetails.register(nibBook, forCellReuseIdentifier: "DetailCell")
        
        if isPreviousViewController{
            self.showLogoWithBackButon()
        }else{
            self.showlogoLeft()
        }
        
        self.tblDetails.register(UINib.init(nibName: "ProfileBookPostCell", bundle: nil), forCellReuseIdentifier: "ProfileBookPostCell")
        
        self.tblDetails.register(UINib.init(nibName: "ChatTopicsCell", bundle: nil), forCellReuseIdentifier: "ChatTopicsCell")
        
        self.tblDetails.register(UINib.init(nibName: "ProfileClassNotesPostCell", bundle: nil), forCellReuseIdentifier: "ProfileClassNotesPostCell")
        
        self.tblDetails.register(UINib.init(nibName: "ProfileMeetupsPostCell", bundle: nil), forCellReuseIdentifier: "ProfileMeetupsPostCell")
        
        let nibPost = UINib.init(nibName: "PostCell", bundle: nil)
        self.collPost.register(nibPost, forCellWithReuseIdentifier: "PostCell")
        selectedSection = 0
        let arrBooksTitles = ["lbl.title.postTitle".localized, "lbl.title.DetailAuthor".localized, "\("lbl.title.priceTitle".localized) $", "lbl.title.City".localized, "txtFld_School".localized, "lbl.title.DetailDepartment".localized, "lbl.title.course".localized, "lbl.title.Professor".localized, "lbl.title.editionTitle".localized, "lbl.title.isbn".localized, "lbl.title.condition".localized, "btn.title.delete".localized]
        let arrMeetupTitles = ["lbl.title.postTitle".localized, "\("lbl.title.dateTitle".localized)\n\("lbl.title.timeTitle".localized)", "lbl.title.placeTitle".localized, "lbl.title.postComments".localized, "btn.title.delete".localized]
        let arrChatsBookTitles = ["lbl.title.topicTitle".localized, "lbl.title.DetailAuthor".localized, "\("lbl.title.dateTitle".localized)\n\("lbl.title.timeTitle".localized)", "btn.title.delete".localized]
        let arrChatsTopicTitles = ["lbl.title.topicTitle".localized, "\("lbl.title.dateTitle".localized)\n\("lbl.title.timeTitle".localized)", "btn.title.delete".localized]
        let arrExamQuizesClassNotesTitles = ["lbl.title.postTitle".localized,"\("lbl.title.priceTitle".localized) $","lbl.title.City".localized, "txtFld_School".localized, "lbl.title.DetailDepartment".localized,"lbl.title.course".localized, "lbl.title.Professor".localized, "lbl.title.noOfPages".localized, "btn.title.delete".localized]
        
        dicLabelsTitles = ["books" : arrBooksTitles,
                           "arrclassnotes" : arrExamQuizesClassNotesTitles,
                           "exams" : arrExamQuizesClassNotesTitles,
                           "arrquizes" : arrExamQuizesClassNotesTitles,
                           "meetups" : arrMeetupTitles,
                           "chatonbooks" : arrChatsBookTitles,
                           "chatsontopic" : arrChatsTopicTitles]
        
        listType = .Book
        self.tblViewHozizontalWidth.constant = 1400
        self.tblDetails.layoutIfNeeded()
        if #available(iOS 15, *) {
            tblDetails.sectionHeaderTopPadding = 0
        }
        selectedSection = 0
        DispatchQueue.main.async {
            let indexPath = IndexPath.init(row: 0, section: 0)
            self.collPost.scrollToItem(at: indexPath, at: .left, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationBar()
        btnNotification.removeTarget(nil, action: nil, for: .allEvents)
        if self.strUserID == AppTheme.sharedInstance.currentUserData[0].UserID{
            //Own Profile
            btnFriends.isHidden = false
            btnNotification.setImage(#imageLiteral(resourceName: "email_count"), for: .normal)
            btnNotification.addTarget(self, action: #selector(btnNotifications(_:)), for: .touchUpInside)
        }
        self.btnFollow.isHidden = true
        
        self.getProfileDetails()
        if ManageLocalization.deviceLang == "es"{
            heightCollView.constant = 100
        }else {
            heightCollView.constant = 85
        }
    }
    
    func setupNavigationBar()
    {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.tintColor = UIColor.lightGray
        //--Setting navigation Bar
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.lightGray
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
    }
    
    func setUpView(){
        self.lblFavBook.text = friendProfile.favBook
        self.lblUserName.text = friendProfile.userName
        self.lblSchoolName.text = friendProfile.schoolName
        self.btnFriends.setTitle("\("lbl.buddies".localized) \(friendProfile.buddiesCount)", for: .normal)
        self.btnFollowers.setTitle("\("lbl.title.followers".localized) \(friendProfile.followersCount)", for: .normal)
        self.btnFollowing.setTitle("\("lbl.title.followings".localized) \(friendProfile.followingCount)", for: .normal)
        viewRating.rating = RV_CheckDataType.getDouble(anyString: friendProfile.userRating as AnyObject)
        self.collPost.reloadData()
        //follower_ID - Who sent the follow request.
        //leader_id - To whome the follow request has been sent
        //is_accepted - Whether the request is accepted ot not
        if AppTheme.sharedInstance.currentUserData[0].UserID == strUserID{
            // Own Profile
            //Hide follow button
            btnFollow.isHidden = true
        }
        
        // corner radius
        
        if friendProfile.gender == Gender.male.rawValue {
            //Male
            viewProfileDetail.layer.borderColor = UIColor.init(red: 40/255.0, green: 162/255.0, blue: 199/255.0, alpha: 1.0).cgColor
            lblUserName.textColor = UIColor.init(red: 40/255.0, green: 162/255.0, blue: 199/255.0, alpha: 1.0)
        }else if friendProfile.gender == Gender.female.rawValue {
            //Female
            viewProfileDetail.layer.borderColor = UIColor.systemPink.cgColor
            lblUserName.textColor = UIColor.systemPink
        }else{
            //Other
            viewProfileDetail.layer.borderColor = UIColor.gray.cgColor
            lblUserName.textColor = UIColor.gray
        }
    }
    @IBAction func btnNotifications( _ Sender : UIButton){
        let objTarget = storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(objTarget, animated: true)
    }
    @IBAction func btnFriends(sender : UIButton)
    {
        let friendVC : FriendsVC = storyBoard.instantiateViewController(withIdentifier: "FriendsVC") as! FriendsVC
        friendVC.isFromConnectScreen = false
        friendVC.userID = AppTheme.sharedInstance.currentUserData[0].UserID
        if sender.tag == 1{
            friendVC.listType = .buddies
        }else if sender.tag == 2{
            friendVC.listType = .followers
        }else if sender.tag == 3 {
            friendVC.listType = .following
        }
        friendVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(friendVC, animated: true)
    }
    @IBAction func btnViewRatingClicked( _ sender: UIButton){
        let rateuser = self.storyboard?.instantiateViewController(withIdentifier: "RateUserVC") as! RateUserVC
        rateuser.strFriendID = strUserID
        rateuser.useDetails = self.friendProfile
        self.navigationController?.pushViewController(rateuser, animated: true)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print(scrollView)
    }
    // MARK: - tableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayPost.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
       
        let post = arrayPost[indexPath.row]
        if indexPath.row == arrayPost.count - 1 && arrayPost.count > 20{
            intPagenumber = intPagenumber+1
            getFeedType()
        }
        if listType == .Book{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileBookPostCell", for: indexPath as IndexPath) as! ProfileBookPostCell
            cell.btnCell.tag = indexPath.row
            cell.btnDelete.tag = indexPath.row
            cell.configureCellwith(post: post)
            cell.btnCell.addTarget(self, action: #selector(didTapBooksCell(_:)), for: .touchUpInside)
            cell.btnDelete.addTarget(self, action: #selector(didTapDeleteCell(_:)), for: .touchUpInside)
            return cell
        }else if listType == .Meetup{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileMeetupsPostCell", for: indexPath as IndexPath) as! ProfileMeetupsPostCell
            cell.configureCellwith(post: post)
            cell.btnCell.tag = indexPath.row
            cell.btnDelete.tag = indexPath.row
            cell.btnCell.addTarget(self, action: #selector(didTapMeetupsCell(_:)), for: .touchUpInside)
            cell.btnDelete.addTarget(self, action: #selector(didTapDeleteCell(_:)), for: .touchUpInside)
            return cell
        }else if listType == .ChatonBooks{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath as IndexPath) as! DetailCell
            cell.lblTitle.text = post.postTitle
            cell.lblTitle.textColor = UIColor.init(red: 65/255.0, green: 196/255.0, blue: 221/255.0, alpha: 1.0)
            cell.lblPlace.text = post.postMeetupPlace
            cell.lblTime.text = AppTheme.sharedInstance.getTimeFromString(strTime: post.postCreatedAt)
            cell.lblDate.text = AppTheme.sharedInstance.getDateFromString(strDate: post.postCreatedAt)
            cell.lblDate.textAlignment = .center
            cell.lblTime.textAlignment = .center
            cell.btnCell.tag = indexPath.row
            cell.btnDelete.tag = indexPath.row
            cell.btnCell.addTarget(self, action: #selector(didTapChatOnBooksCell(_:)), for: .touchUpInside)
            cell.btnDelete.addTarget(self, action: #selector(didTapDeleteCell(_:)), for: .touchUpInside)
            return cell
        }else if listType == .ChatsonTopics{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatTopicsCell", for: indexPath as IndexPath) as! ChatTopicsCell
            cell.lblTitle.text = post.postTitle
            cell.lblTitle.textColor = UIColor.init(red: 65/255.0, green: 196/255.0, blue: 221/255.0, alpha: 1.0)
            cell.lblTime.text = AppTheme.sharedInstance.getTimeFromString(strTime: post.postCreatedAt)
            cell.lblDate.text = AppTheme.sharedInstance.getDateFromString(strDate: post.postCreatedAt)
            cell.lblDate.textAlignment = .center
            cell.lblTime.textAlignment = .center
            cell.btnCell.tag = indexPath.row
            cell.btnDelete.tag = indexPath.row
            cell.btnCell.addTarget(self, action: #selector(didTapChatOnTopicsCell(_:)), for: .touchUpInside)
            cell.btnDelete.addTarget(self, action: #selector(didTapDeleteCell(_:)), for: .touchUpInside)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileClassNotesPostCell", for: indexPath as IndexPath) as! ProfileClassNotesPostCell
            cell.configureCellwith(post: post)
            cell.btnCell.tag = indexPath.row
            cell.btnDelete.tag = indexPath.row
            cell.btnCell.addTarget(self, action: #selector(didTapClassCell(_:)), for: .touchUpInside)
            cell.btnDelete.addTarget(self, action: #selector(didTapDeleteCell(_:)), for: .touchUpInside)
            return cell
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return tblPostTitlesHeaderView
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        view.layoutIfNeeded() // force any pending operations to finish
        let post = arrayPost[indexPath.row]
        print(post.postTitle)
        if post.postCat == "1" && (listType == .Book) {
            //Book Detail Screen
//            let objTarget = storyboard?.instantiateViewController(withIdentifier: "BookDetailVC") as! BookDetailVC
//            objTarget.strPostID = post.postID
//            objTarget.hidesBottomBarWhenPushed = true
//            objTarget.reserveBookWithPostID = { bookid, isReserved in
//                let foundItems = self.arrayPost.filter {$0.postID == bookid}
//                foundItems[0].isReserved = isReserved
//                self.tblDetails.reloadData()
//            }
//            self.navigationController?.pushViewController(objTarget, animated: true)
        }else if post.postCat == "2" && (listType == .Notes){
            //Notes Detail Screen
//            let objTarget = storyboard?.instantiateViewController(withIdentifier: "PostDetailsVC") as! PostDetailsVC
//            objTarget.strPostID = post.postID
//            objTarget.postType = .PostClassNotes
//            objTarget.hidesBottomBarWhenPushed = true
//            self.navigationController?.pushViewController(objTarget, animated: true)
        }else if post.postCat == "3" && (listType == .Exam) {
            //Exam Detail Screen
//            let objTarget = storyboard?.instantiateViewController(withIdentifier: "PostDetailsVC") as! PostDetailsVC
//            objTarget.strPostID = post.postID
//            objTarget.postType = .PostExam
//            objTarget.hidesBottomBarWhenPushed = true
//            self.navigationController?.pushViewController(objTarget, animated: true)
        }else if post.postCat == "4" && (listType == .Quiz) {
            //Quiz Detail Screen
//            let objTarget = storyboard?.instantiateViewController(withIdentifier: "PostDetailsVC") as! PostDetailsVC
//            objTarget.strPostID = post.postID
//            objTarget.postType = .PostQuiz
//            objTarget.hidesBottomBarWhenPushed = true
//            self.navigationController?.pushViewController(objTarget, animated: true)
        }else if post.postCat == "5" && (listType == .Meetup) {
            //MeetUps Detail Screen
//            let objTarget = storyboard?.instantiateViewController(withIdentifier: "MeetupDetailVC") as! MeetupDetailVC
//            objTarget.strPostID = post.postID
//            objTarget.hidesBottomBarWhenPushed = true
//            self.navigationController?.pushViewController(objTarget, animated: true)
        }else if post.postCat == "6" && (listType == .ChatonBooks) {
//            let objTarget = storyboard?.instantiateViewController(withIdentifier: "BooksChatVC") as! BooksChatVC
//            objTarget.strPostID = post.postID
//            objTarget.strPostCreator = post.postCreator
//            objTarget.strPostCreatorId = post.userid
//            objTarget.strPostTitle = post.postTitle
//            objTarget.isChatOnbooks = true
//            objTarget.hidesBottomBarWhenPushed = true
//            if post.postIsAnnonymous == "true" || post.postIsAnnonymous == "1"{
//                objTarget.isAnonymous = true
//            }else{
//                objTarget.isAnonymous = false
//            }
//            self.navigationController?.pushViewController(objTarget, animated: true)
        }else {
            //ChatsOnTopic Detail Screen
//            let objTarget = storyboard?.instantiateViewController(withIdentifier: "BooksChatVC") as! BooksChatVC
//            objTarget.strPostID = post.postID
//            objTarget.strPostCreator = post.postCreator
//            objTarget.strPostCreatorId = post.userid
//            objTarget.strPostTitle = post.postTitle
//            objTarget.isChatOnbooks = false
//            objTarget.hidesBottomBarWhenPushed = true
//            if post.postIsAnnonymous == "true" || post.postIsAnnonymous == "1"{
//                objTarget.isAnonymous = true
//            }else{
//                objTarget.isAnonymous = false
//            }
//            self.navigationController?.pushViewController(objTarget, animated: true)
        }
    }
    
    // MARK: - Collection view delegate methods --
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collPost{
            return 7
        }else{
            return dicLabelsTitles[arrPostTypes[selectedSection]]!.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collPost{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCell", for: indexPath as IndexPath) as! PostCell
        if selectedSection == indexPath.row{
//            cell.backgroundColor = UIColor(red: 217.0/255.0, green: 243.0/255.0, blue: 248.0/255.0, alpha: 1.0)
            cell.backgroundColor = .clear
            cell.lblPostName.textColor = UIColor.init(red: 65/255.0, green: 196/255.0, blue: 221/255.0, alpha: 1.0)
        }else{
            cell.backgroundColor = UIColor.clear
            cell.lblPostName.textColor = .darkGray
        }
        switch indexPath.item {
        case 0:
            cell.lblPostName.text = ManageLocalization.getLocalaizedString(key: "lbl.title.booksLabel").uppercased()
            cell.lblCount.text = friendProfile.posttedBooksCount
        case 1:
            cell.lblPostName.text = ManageLocalization.getLocalaizedString(key: "lbl.title.notesLabel").uppercased()
            cell.lblCount.text = friendProfile.posttedClassNotesCount
        case 2:
            cell.lblPostName.text = ManageLocalization.getLocalaizedString(key: "lbl.title.examLabel").uppercased()
            cell.lblCount.text = friendProfile.postedExamsCount
        case 3:
            cell.lblPostName.text = ManageLocalization.getLocalaizedString(key: "lbl.title.quizeLabel").uppercased()
            cell.lblCount.text = friendProfile.posttedQuizesCount
        case 4:
            cell.lblPostName.text = ManageLocalization.getLocalaizedString(key: "lbl.title.meetupLabel").uppercased()
            cell.lblCount.text = friendProfile.posttedMeetupsCount
        case 5:
            cell.lblPostName.text = ManageLocalization.getLocalaizedString(key: "lbl.title.bookChatLabel").uppercased()
            cell.lblCount.text = friendProfile.posttedChatsOnBooksCount
        case 6:
            cell.lblPostName.text = ManageLocalization.getLocalaizedString(key: "lbl.title.topicLabel").uppercased()
            cell.lblCount.text = friendProfile.posttedChatsOnTopicCount
        default:
            cell.lblPostName.text = ""
        }
        return cell
        }else{
            let profileHeaderPostTitleCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfilePostTitlesHeaderCell", for: indexPath) as! ProfilePostTitlesHeaderCell
            profileHeaderPostTitleCell.lblHeading.text = dicLabelsTitles[arrPostTypes[selectedSection]]?[indexPath.row]
            if indexPath.row == 0{
                profileHeaderPostTitleCell.lblHeading.textAlignment = .left
            }
            else if indexPath.row == dicLabelsTitles[arrPostTypes[selectedSection]]!.count-1 && listType == .ChatsonTopics{
                profileHeaderPostTitleCell.lblHeading.textAlignment = .center
            }
            else{
                profileHeaderPostTitleCell.lblHeading.textAlignment = .center
            }
            return profileHeaderPostTitleCell
        }
    }
   
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        if collectionView == collPost{
            selectedSection = indexPath.row
            listType = ListType.init(rawValue: indexPath.row+1)!
            self.arrayPost.removeAll()
            tblDetails.reloadData()
            intPagenumber = 1
            self.getFeedType()
            self.collPost.reloadData()
            self.collPostTitles.reloadData()
            UIView.animate(withDuration: 0.5) {
                self.scrollViewPosts.contentOffset.x = 0
            }
            UIView.animate(withDuration: 0.5) {
                self.scrollViewPosts.contentOffset.y = 0
            }

            if indexPath.row == 0{
                self.tblViewHozizontalWidth.constant = 1400
            }else if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3{
                self.tblViewHozizontalWidth.constant = 1110
            }else if indexPath.row == 4{
                self.tblViewHozizontalWidth.constant = 1050
            }else{
                self.tblViewHozizontalWidth.constant = self.view.frame.width
            }
            self.tblDetails.layoutIfNeeded()
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        if collectionView != collPostTitles{
            if selectedSection == 6 {
                return CGSize.init(width: 80, height: heightCollView.constant)
            }
            return CGSize.init(width: 100, height: heightCollView.constant)
        }else{
            if selectedSection == 0{
                if indexPath.row == 0{
                    return CGSize.init(width: 200, height: 40)
                }else{
                    return CGSize.init(width: 100, height: 40)
                }
            }else if selectedSection == 1 || selectedSection == 2 || selectedSection == 3{
                if indexPath.row == 0{
                    return CGSize.init(width: 200, height: 40)
                }else{
                    return CGSize.init(width: 100, height: 40)
                }
            }else if selectedSection == 4{
                return CGSize.init(width: 200, height: 40)
            }
            else if selectedSection == 5{
                return CGSize.init(width: (self.tblDetails.frame.width-20)/4, height: 40)
            }else{
                return CGSize.init(width: (self.tblDetails.frame.width-20)/3, height: 40)

            }
        }
    }
    
    //MARK: Webservice Methods
    func getProfileDetails(){
        let when = DispatchTime.now() + 0.0
        MBProgressHUD.showAdded(to: self.view, animated: true)
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"
        WebAPI.httpJSONRequest(viewController: self, url: APPURL.getFriendProfile, params: ["user_id": strUserID as AnyObject,"lang": lang as AnyObject], SuccessHandler: { (responseData) in
            MBProgressHUD.hide(for: self.view, animated: false)
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.friendProfile.parser(profileData: responseData["responseData"])
                    self.setUpView()
                    self.arrayPost.removeAll()
                    self.tblDetails.reloadData()
                    self.intPagenumber = 1
                    self.getFeedType()
                    self.collPost.reloadData()
                }
            }else{
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
        }) { (error) in
            MBProgressHUD.hide(for: self.view, animated: false)
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
        
    }
    func respondFriendRequest(status : String){
        let lang = AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as! String
        let params: [String : Any] = ["follower_id": strUserID, "lang": lang]
        var strURL = ""
        if status == "Accept"{
            strURL = APPURL.acceptFriendRequest
        }else {
            strURL = APPURL.rejectFriendRequest
        }
        
        let when = DispatchTime.now() + 0.0
        WebAPI.httpJSONRequest(viewController: self, url: strURL, params: params as [String : AnyObject], SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    
                    Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject))
                    self.view.layoutIfNeeded()
                    UIView.animate(withDuration: 0.5, animations: { () -> Void in
                        self.view.layoutIfNeeded()
                    })
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
            
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
    func getFeedType(){
        if listType == .All{
            getPostForCategory(URLType: APPURL.getAllPost)
        }else if listType == .Book{
            getPostForCategory(URLType: APPURL.getAllBooks)
        }else if listType == .ChatonBooks{
            getPostForCategory(URLType: APPURL.getChatsOnBook)
        }else if listType == .ChatsonTopics{
            getPostForCategory(URLType: APPURL.getChatsOnTopic)
        }else if listType == .Exam{
            getPostForCategory(URLType: APPURL.getAllExams)
        }else if listType == .Meetup{
            getPostForCategory(URLType: APPURL.getAllMeetups)
        }else if listType == .Notes{
            getPostForCategory(URLType: APPURL.getAllNotes)
        }else if listType == .Quiz{
            getPostForCategory(URLType: APPURL.getAllQuizz)
        }
    }
    func getPostForCategory(URLType : String){
        let when = DispatchTime.now() + 0.0
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"
        WebAPI.httpJSONRequest(viewController: self, url: URLType, params: ["page": intPagenumber as AnyObject, "user_id" : strUserID as AnyObject, "lang": lang as AnyObject], SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    
                    if responseData["responseData"].arrayValue.count > 0{
                        for json in responseData["responseData"].arrayValue {
                            let postFeed = PostDetails()
                            postFeed.parser(json)
                            self.arrayPost.append(postFeed)
                        }
                        self.tblDetails.reloadData()
                    }
                    
                    if self.intPagenumber == 1 && self.arrayPost.count > 0{
                        DispatchQueue.main.async {
                            let indexPath = NSIndexPath.init(row: 0, section: 0)
                            self.tblDetails.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
                        }
                    }
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
        
    }
}

extension ProfileVC {
    @objc func didTapBooksCell(_ sender : UIButton) {
        let post = arrayPost[sender.tag]
        print(post.postTitle)
        if post.postCat == "1" && (listType == .Book) {
            //Book Detail Screen
            let objTarget = storyboard?.instantiateViewController(withIdentifier: "BookDetailVC") as! BookDetailVC
            objTarget.strPostID = post.postID
            objTarget.hidesBottomBarWhenPushed = true
            objTarget.reserveBookWithPostID = { bookid, isReserved in
                let foundItems = self.arrayPost.filter {$0.postID == bookid}
                foundItems[0].isReserved = isReserved
                self.tblDetails.reloadData()
            }
            self.navigationController?.pushViewController(objTarget, animated: true)
        }
    }
    
    @objc func didTapMeetupsCell(_ sender: UIButton) {
        let post = arrayPost[sender.tag]
        print(post.postTitle)
        if post.postCat == "5" && (listType == .Meetup) {
            let objTarget = storyboard?.instantiateViewController(withIdentifier: "MeetupDetailVC") as! MeetupDetailVC
            objTarget.strPostID = post.postID
            objTarget.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objTarget, animated: true)
        }
    }
    
    @objc func didTapClassCell(_ sender: UIButton) {
        let post = arrayPost[sender.tag]
        print(post.postTitle)
        if post.postCat == "2" && (listType == .Notes){
            //Notes Detail Screen
            let objTarget = storyboard?.instantiateViewController(withIdentifier: "PostDetailsVC") as! PostDetailsVC
            objTarget.strPostID = post.postID
            objTarget.postType = .PostClassNotes
            objTarget.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objTarget, animated: true)
        }else if post.postCat == "3" && (listType == .Exam) {
            //Exam Detail Screen
            let objTarget = storyboard?.instantiateViewController(withIdentifier: "PostDetailsVC") as! PostDetailsVC
            objTarget.strPostID = post.postID
            objTarget.postType = .PostExam
            objTarget.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objTarget, animated: true)
        }else if post.postCat == "4" && (listType == .Quiz) {
            //Quiz Detail Screen
            let objTarget = storyboard?.instantiateViewController(withIdentifier: "PostDetailsVC") as! PostDetailsVC
            objTarget.strPostID = post.postID
            objTarget.postType = .PostQuiz
            objTarget.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objTarget, animated: true)
        }
    }
    
    @objc func didTapChatOnBooksCell(_ sender: UIButton) {
        let post = arrayPost[sender.tag]
        print(post.postTitle)
        if post.postCat == "6" && (listType == .ChatonBooks) {
            let objTarget = storyboard?.instantiateViewController(withIdentifier: "BooksChatVC") as! BooksChatVC
            objTarget.strPostID = post.postID
            objTarget.strPostCreator = post.postCreator
            objTarget.strPostCreatorId = post.userid
            objTarget.strPostTitle = post.postTitle
            objTarget.isChatOnbooks = true
            objTarget.hidesBottomBarWhenPushed = true
            if post.postIsAnnonymous == "true" || post.postIsAnnonymous == "1"{
                objTarget.isAnonymous = true
            }else{
                objTarget.isAnonymous = false
            }
            self.navigationController?.pushViewController(objTarget, animated: true)
        }
    }
    
    @objc func didTapChatOnTopicsCell(_ sender: UIButton) {
        let post = arrayPost[sender.tag]
        print(post.postTitle)
        if post.postCat == "7" && (listType == .ChatsonTopics) {
            let objTarget = storyboard?.instantiateViewController(withIdentifier: "BooksChatVC") as! BooksChatVC
            objTarget.strPostID = post.postID
            objTarget.strPostCreator = post.postCreator
            objTarget.strPostCreatorId = post.userid
            objTarget.strPostTitle = post.postTitle
            objTarget.isChatOnbooks = false
            objTarget.hidesBottomBarWhenPushed = true
            if post.postIsAnnonymous == "true" || post.postIsAnnonymous == "1"{
                objTarget.isAnonymous = true
            }else{
                objTarget.isAnonymous = false
            }
            self.navigationController?.pushViewController(objTarget, animated: true)
        }
    }



    
    @objc func didTapDeleteCell(_ sender : UIButton) {
        let post = arrayPost[sender.tag]
        print(post.postTitle)
        
        let alert : UIAlertController = UIAlertController(title: "alert.deletePostTitle".localized, message: "alert.deletePost".localized, preferredStyle: .alert)
        
        let YesAction = UIAlertAction(title: "alert.ok".localized, style: .default, handler: { (action) in
            self.deletePostWithData(post: post, index: sender.tag)
        })
        let CancelAction = UIAlertAction.init(title: "alert.cancel".localized, style: .destructive) { (action) in
            
        }
        alert.addAction(YesAction)
        alert.addAction(CancelAction)
        self.present(alert, animated: true, completion: {
        })
    }
    
}

extension ProfileVC {
    /*
    API URL : - https://clubbooks.com/api/deletePostWithData
    Params : lang , post_id
      */
    
    func deletePostWithData(post: PostDetails, index: Int) {
        let when = DispatchTime.now() + 0.0
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"
        WebAPI.httpJSONRequest(viewController: self, url: APPURL.deletePostWithData, params: ["post_id" : post.postID as AnyObject, "lang": lang as AnyObject], SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
//                    self.arrayPost.remove(at: index)
//                    self.tblDetails.reloadData()
                    self.getProfileDetails()
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
        
    }

}
