//
//  RateUserVC.swift
//  ClubBooks
//
//  Created by cis on 03/09/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class RateUserVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
  
    @IBOutlet var tblRatings : UITableView!
    @IBOutlet var lblSchoolName : UILabel!
    @IBOutlet var lblUserName : UILabel!
    @IBOutlet var lblFavBook : UILabel!
    @IBOutlet var viewRating : CosmosView!
    var arrRatings = [UserRatingDetails]()
    var strFriendID = String()
    var useDetails = FriendProfileDetails()
    @IBOutlet var viewProfileDetail : UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showBackButtonWith(Image: #imageLiteral(resourceName: "btn.back"))
        self.getAllUserRating()
        tblRatings.tableFooterView = UIView()
        self.lblFavBook.text = useDetails.favBook
        self.lblUserName.text = useDetails.name
        self.lblSchoolName.text = useDetails.schoolName
        viewRating.rating = RV_CheckDataType.getDouble(anyString: useDetails.userRating as AnyObject)
        viewProfileDetail.layer.cornerRadius = viewProfileDetail.frame.size.height/2
        //        viewProfileBackground.layer.cornerRadius = viewProfileBackground.frame.size.height/2
        // border
        viewProfileDetail.layer.borderWidth = 10
        viewProfileDetail.layer.borderColor = UIColor.init(red: 252/255.0, green: 252/255.0, blue: 252/255.0, alpha: 1.0).cgColor
        // Do any additional setup after loading the view.
        if self.strFriendID == AppTheme.sharedInstance.currentUserData[0].UserID{
            useDetails.isratedAlready = true
        }
    }
    
    
    // MARK:
    // MARK: - Navigation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRatings.count+1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            if useDetails.isratedAlready == false{
                let ratecell = tableView.dequeueReusableCell(withIdentifier: "rateUserCell", for: indexPath) as! rateUserCell
                ratecell.lblUsername.text = AppTheme.sharedInstance.currentUserData[0].Name
                ratecell.selectionStyle = .none
                ratecell.sendRating = { comment, rating  in
                    if comment == "" {
                        Manager.sharedInstance.showAlert(self, message: "alert.rate.addComment".localized)
                    }else{
                        self.postRating(comment: comment, rating: rating)
                    }
                }
                return ratecell
            }else{
                let alreadyRated = tableView.dequeueReusableCell(withIdentifier: "alreadyRated", for: indexPath) as! alreadyRatedCell
                alreadyRated.selectionStyle = .none
                if self.strFriendID == AppTheme.sharedInstance.currentUserData[0].UserID{
                    alreadyRated.lblTitle.text = ""
                }else{
                    alreadyRated.lblTitle.text = "lbl.title.alreadyRated".localized
                }
                return alreadyRated
            }
        }else{
            let userratingcell = tableView.dequeueReusableCell(withIdentifier: "UserRatingCell", for: indexPath) as! UserRatingCell
            let userRatingDetails = arrRatings[indexPath.row-1]
            userratingcell.lblUsername.text = userRatingDetails.userName
            userratingcell.lblReview.text = userRatingDetails.review
            userratingcell.viewRating.rating = RV_CheckDataType.getDouble(anyString: userRatingDetails.rating as AnyObject)
            userratingcell.selectionStyle = .none
            return userratingcell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            if useDetails.isratedAlready == false{
                return 300
            }else if self.strFriendID == AppTheme.sharedInstance.currentUserData[0].UserID {
                return 0
            }else{
                return 80
            }
        }
        return tblRatings.estimatedRowHeight
    }
    @objc func getAllUserRating(){
        let when = DispatchTime.now() + 0.0
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"

        WebAPI.httpJSONRequest(viewController: self, url: APPURL.getAllUserRating, params: ["friend_id": strFriendID as AnyObject, "lang": lang as AnyObject], SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    if responseData["responseData"]["rating_reviews"].arrayValue.count > 0{
                        for json in responseData["responseData"]["rating_reviews"].arrayValue {
                            let ratings = UserRatingDetails()
                            ratings.parser(ratingDetails: json)
                            self.arrRatings.append(ratings)
                        }
                        self.tblRatings.reloadData()
                    }
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
        
    }
    @objc func postRating(comment : String, rating : String){
        let when = DispatchTime.now() + 0.0
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"

        WebAPI.httpJSONRequest(viewController: self, url: APPURL.submitRatngForUser, params: ["friend_id": strFriendID as AnyObject, "rating" : rating as AnyObject, "review" : comment as AnyObject, "lang": lang as AnyObject], SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.useDetails.isratedAlready = true
                    self.tblRatings.reloadData()
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
        
    }
}
class UserRatingCell: UITableViewCell {
    @IBOutlet var lblUsername : UILabel!
    @IBOutlet var lblReview : UILabel!
    @IBOutlet var viewRating : CosmosView!
    override func awakeFromNib() {
        super.awakeFromNib()
     
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    
}

class rateUserCell: UITableViewCell {
    @IBOutlet var lblUsername : UILabel!
    @IBOutlet var txtViewRating : UITextView!
    @IBOutlet var viewRating : CosmosView!
    @IBOutlet var btnRate : UIButton!
    var sendRating:((String, String)->Void)! = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    @IBAction func btnRateClicked( _ sender : UIButton){
        self.sendRating("\(String(describing: txtViewRating.text!))","\(RV_CheckDataType.getString(anyString: viewRating.rating as AnyObject))")
    }
}

class alreadyRatedCell: UITableViewCell {
    @IBOutlet var lblTitle : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

import SwiftyJSON
class UserRatingDetails: NSObject {
    
    var id = String()
    var userId = String()
    var userName = String()
    var rating = String()
    var review = String()
    
    override init() {
        super.init()
    }
   
    func parser(ratingDetails : JSON) {
        self.id = ratingDetails["id"].stringValue
        self.userId =  ratingDetails["user_id"].stringValue
        self.userName =  ratingDetails["username"].stringValue
        self.rating = ratingDetails["rating"].stringValue
        self.review = ratingDetails["review"].stringValue
    }
}






