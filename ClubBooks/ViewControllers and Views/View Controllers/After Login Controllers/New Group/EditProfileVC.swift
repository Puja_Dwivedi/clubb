//
//  SignUpVC.swift
//  ClubBooks
//
//  Created by cis on 21/11/17.
//  Copyright © 2017 CIS. All rights reserved.
//

import UIKit
import MBProgressHUD
class EditProfileVC: BaseViewController, UITextFieldDelegate {
    @IBOutlet var txtFldEmailAddress : UITextField!
    @IBOutlet var txtFldName : UITextField!
    @IBOutlet var txtFldPhone : UITextField!
    @IBOutlet var txtFldSchool : UITextField!
    @IBOutlet var txtFldBook : UITextField!
    @IBOutlet var arrtextField : [UITextField]!
    @IBOutlet var lblAccountTitle : UILabel!
    @IBOutlet var lblAccountType : UILabel!
    @IBOutlet var accountToggle: UISwitch!
    
    var selectedAccountType = 0  // 0 is public and 1 is private
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for textField in arrtextField
        {
            textField.superview?.addShadowAround(layer: (textField.superview?.layer)!)
        }
        txtFldEmailAddress.text = AppTheme.sharedInstance.currentUserData[0].email
        txtFldName.text = AppTheme.sharedInstance.currentUserData[0].UserName
        txtFldPhone.text = AppTheme.sharedInstance.currentUserData[0].Mobile
        txtFldSchool.text = AppTheme.sharedInstance.currentUserData[0].SchoolName
        txtFldBook.text = AppTheme.sharedInstance.currentUserData[0].FavBook
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "btn.title.editprofile".localized
        self.lblAccountTitle.text = "lbl.accounttype".localized
        self.setAccountType(type: Int(AppTheme.sharedInstance.currentUserData[0].AccountType) ?? 0)
        self.showLogoWithBackButon()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.showShadow()
        
    }
    
    func setAccountType(type: Int) {
        if type == 0 {
            self.selectedAccountType = 0
            self.lblAccountType.text = "lbl.private".localized
            self.accountToggle.isOn = false
        } else {
            self.selectedAccountType = 1
            self.lblAccountType.text = "lbl.public".localized
            self.accountToggle.isOn = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Button Actions
    @IBAction func btnBack(sender : UIButton)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnToggleAccount(_ sender: UISwitch) {
        if accountToggle.isOn {
            self.lblAccountType.text = "lbl.public".localized
            self.selectedAccountType = 1
        } else {
            self.lblAccountType.text = "lbl.private".localized
            self.selectedAccountType = 0
        }
        print(self.selectedAccountType)
    }
    
    @IBAction func btnSignupClicked( _ sender : UIButton){
        var strError = ""
        
        if RV_Validations.isEmptyField(testStr: txtFldEmailAddress.text!)
        {
            strError = ERROR.LoginSignUpError.EmailAddressRequired
        }
        else  if RV_Validations.isEmptyField(testStr: txtFldName.text!)
        {
            strError = ERROR.LoginSignUpError.NameRequired
        }
        else if !RV_Validations.isValidEmail(testStr: txtFldEmailAddress.text!)
        {
            strError = ERROR.LoginSignUpError.InvalidEmailId
        }else if RV_Validations.isEmptyField(testStr: txtFldPhone.text!) {
            
            strError = ERROR.LoginSignUpError.MobileNoRequired
        }else if (txtFldPhone.text?.count)! < 10 {
            
            strError = ERROR.LoginSignUpError.MobileNoStrength
        }
        else if RV_Validations.isEmptyField(testStr: txtFldSchool.text!) {
            
            strError = ERROR.LoginSignUpError.SchoolName
        }
        else if RV_Validations.isEmptyField(testStr: txtFldBook.text!) {
            
            strError = ERROR.LoginSignUpError.FavBookNameRequired
        }
        if strError != "" {
            Manager.sharedInstance.showAlert(self, message: strError)
            return
        }
        else{
            self.registerUser()
        }
        
    }
    //MARK: UITextField Delegates
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtFldName {
            let maxLength = 20
            let currentString = (textField.text ?? "") as NSString
            let newString = currentString.replacingCharacters(in: range, with: string)
            return newString.count <= maxLength
        }else{
            return true
        }
    }
    
    //MARK: WebService Functions
    
    func registerUser(){
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"

        let params: [String : Any] = [
            "username" : RV_CheckDataType.getString(anyString: txtFldName.text as AnyObject),
            "mobile" : RV_CheckDataType.getString(anyString: txtFldPhone.text as AnyObject),
            "school_name" : RV_CheckDataType.getString(anyString: txtFldSchool.text as AnyObject),
            "fav_book" : RV_CheckDataType.getString(anyString: txtFldBook.text as AnyObject),
            "account_type" : self.selectedAccountType,
            "lang": lang as AnyObject]
        
        WebAPI.httpJSONRequest(viewController: self, url: APPURL.updateProfile, params: params as [String : AnyObject], SuccessHandler: { (jsonResponse) in
            let Status = jsonResponse.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                
                if AppTheme.sharedInstance.currentUserData.count > 0
                {
                    AppTheme.sharedInstance.currentUserData[0] = UserDetails.init(userDict: jsonResponse)
                }
                else
                {
                    AppTheme.sharedInstance.currentUserData.append(UserDetails.init(userDict: jsonResponse))
                }
                AppTheme.sharedInstance.saveUserDataToDefault()
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: jsonResponse.dictionary!["msg"]?.stringValue as AnyObject) )
                
            }else{
                
                if jsonResponse.dictionary!["msgCode"]! == 404 {
                    let dict  =  jsonResponse.dictionary!["responseData"]
                    let alert = UIAlertController(title: Constant.ALERT_TITLE, message: ManageLocalization.getLocalaizedString(key: "alert.resendVerificationemail"), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "alert.ok".localized, style: .cancel, handler: nil))
                    alert.addAction(UIAlertAction.init(title: "alert.resendEmail".localized, style: .default, handler: { (action) in
                        self.resendEmail(email_Token: dict!["email_token"].stringValue)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }else {
                    Manager.sharedInstance.showAlert(self, message: jsonResponse.dictionary!["msg"]!.stringValue)
                }
            }
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
    
    func resendEmail(email_Token : String){
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"

        let params: [String : Any] = [
            "email" : RV_CheckDataType.getString(anyString: txtFldEmailAddress.text as AnyObject),
            "lang": lang as AnyObject,
            "email_token" : email_Token]
        
        WebAPI.httpJSONRequest(viewController: self, url: APPURL.resendVerificationEmail, params: params as [String : AnyObject], SuccessHandler: { (jsonResponse) in
            // change 2 to desired number of seconds
            
            let Status = jsonResponse.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                for textfield in self.arrtextField {
                    textfield.text = ""
                }
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: jsonResponse.dictionary!["msg"]?.stringValue as AnyObject) )
                
            }else{
                
                Manager.sharedInstance.showAlert(self, message: jsonResponse.dictionary!["msg"]!.stringValue)
            }
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
    
    
}

