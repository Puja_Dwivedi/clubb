//
//  WalletVC.swift
//  ClubBooks
//
//  Created by cis on 16/04/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class WalletVC: BaseViewController {
    @IBOutlet var lblUserName : UILabel!
    @IBOutlet var lblAmount : UILabel!
    @IBOutlet var btnAddMoney : UIButton!
    @IBOutlet var btnPay : UIButton!
    var isFromDetailScreen : Bool!
    var strPostIDtoPay = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        showLogoWithBackButon()
        self.title = "nav.Wallet".localized
        self.lblUserName.text = "lbl.title.welcome".localized + AppTheme.sharedInstance.currentUserData[0].UserName
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getCurrentWalletBalance()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnAddMoneyClicked(_ Sender : UIButton){
        let walletAddMoneyVC : WalletAddMoney = storyBoard.instantiateViewController(withIdentifier: "WalletAddMoney") as! WalletAddMoney
        self.navigationController?.pushViewController(walletAddMoneyVC, animated: true)
    }
    @IBAction func btnPayClicked(_ Sender : UIButton){
        if isFromDetailScreen {
            let alert = UIAlertController(title: Constant.ALERT_TITLE, message: "alert.payforSeller".localized , preferredStyle: .alert)
            alert.addAction(UIAlertAction.init(title: "btn.Title.PaySeller".localized, style: .default, handler: { (response) in
                self.payForPost()
            }))
            alert.addAction(UIAlertAction.init(title: "alert.cancel".localized, style: .destructive, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            let payRequestVC : PayRequestVC = storyBoard.instantiateViewController(withIdentifier: "PayRequestVC") as! PayRequestVC
            payRequestVC.isFromFriendsProfileScreen = false
            self.navigationController?.pushViewController(payRequestVC, animated: true)
        }
    }
    func setupNavigationBar()
    {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
    }
    func getCurrentWalletBalance(){
        let when = DispatchTime.now() + 0.0
        WebAPI.httpJSONRequest(viewController: self, url: APPURL.getCurrentWalletBalance, params: [:], SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    if responseData.dictionary!["responseData"]?.dictionaryValue != nil{
                        let doubleValues = RV_CheckDataType.getDouble(anyString: responseData.dictionary!["responseData"]?.dictionary!["wallet_balance"]?.stringValue as AnyObject)
                        self.lblAmount.text = String(format : "$ %.2f", doubleValues)
                    }
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
    func payForPost(){
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"

        WebAPI.httpJSONRequest(viewController: self, url: APPURL.payForPost, params: ["post_id": strPostIDtoPay as AnyObject, "lang": lang as AnyObject], SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.0) {
                    let addMoneyCompleted = self.storyboard?.instantiateViewController(withIdentifier: "PaymentCompleted") as! PaymentCompleted
                    addMoneyCompleted.isFromPayForPost = true
                    self.navigationController?.pushViewController(addMoneyCompleted, animated: true)
                }
            }else{
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject))
            }
        }) { (error) in
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
}
