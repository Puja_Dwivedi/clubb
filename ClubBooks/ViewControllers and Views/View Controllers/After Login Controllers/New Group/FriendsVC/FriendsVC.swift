//
//  FriendsVC.swift
//  ClubBooks
//
//  Created by cis on 24/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON
enum FriendListType : Int
{
    case buddies = 0
    case followers
    case following
    case friendRequest
}

class FriendsVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    var listType : FriendListType = FriendListType.init(rawValue: 0)!
    var arrayFriend = [FriendProfileDetails]()
    var intPageNumber : Int!
    @IBOutlet var lblNoRequest : UILabel!
    @IBOutlet var tblFriendList : UITableView!
    @IBOutlet var segFriend : UISegmentedControl!
    @IBOutlet var heightSegmentView : NSLayoutConstraint!
    @IBOutlet var heightSearchBar : NSLayoutConstraint!
    @IBOutlet var searchBar : UISearchBar!
    // If isFromConnectScreen, hide the segment control
    var isFromConnectScreen : Bool!
    let paginationLimit = 20
    var userID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNoRequest.text = ""
        lblNoRequest.isHidden = true
        //--
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = UIColor.white
        self.heightSearchBar.constant = 51.0
        searchBar.showsCancelButton = false
        self.view.layoutSubviews()
        //--
        showLogoWithBackButon()
       self.showSettingButton()
        if listType == .buddies || listType == .friendRequest{
            self.setTitleWithLocalizedString(title: "nav.Users")
        }else if listType == .following{
            self.setTitleWithLocalizedString(title: "lbl.title.followings")
        }else if listType == .followers{
            self.setTitleWithLocalizedString(title: "lbl.title.followers")
        }
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        self.navigationController?.navigationBar.tintColor = UIColor.gray
        
        let nibBook = UINib.init(nibName: "FriendCell", bundle: nil)
        self.tblFriendList.register(nibBook, forCellReuseIdentifier: "FriendCell")
        
        tblFriendList.tableFooterView = UIView()
        segFriend.setTitle(ManageLocalization.getLocalaizedString(key: "lbl.title.followers"), forSegmentAt: 0)
        segFriend.setTitle(ManageLocalization.getLocalaizedString(key: "lbl.buddiesRequst"), forSegmentAt: 1)
        intPageNumber = 1
        self.getAllFriendList()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        segFriend.selectedSegmentIndex = 0
        if isFromConnectScreen || listType == .buddies || listType == .following{
            segFriend.isHidden = true
            heightSegmentView.constant = 0
        }else{
            if userID != AppTheme.sharedInstance.currentUserData[0].UserID {
                segFriend.isHidden = true
                heightSegmentView.constant = 0
            }else{
                segFriend.isHidden = false
                heightSegmentView.constant = 30
            }
        }
        self.navigationController?.isNavigationBarHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - UIbutton Clicks
   
    @IBAction func SegmentedControllerfriends(_ sender: UISegmentedControl) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        arrayFriend.removeAll()
        intPageNumber = 1
        self.tblFriendList.reloadData()
        switch sender.selectedSegmentIndex {
        case 0:
            self.heightSearchBar.constant = 51.0
            self.view.layoutSubviews()
            arrayFriend.removeAll()
            listType = .followers
            getAllFriendList()
        case 1:
            self.heightSearchBar.constant = 0
            self.view.layoutSubviews()
            arrayFriend.removeAll()
            listType = .friendRequest
            getAllFriendList()
        default:
            return
        }
    }
    @IBAction func btnAcceptFriendRequestClicked( _ sender : UIButton){
        print("Accept %@", sender.tag)
        self.respondFriendRequest(index: sender.tag, status: "Accept")
    }
    @IBAction func btnRejectFriendRequestClicked( _ sender : UIButton){
        print("Reject% @", sender.tag)
        self.respondFriendRequest(index: sender.tag, status: " ")
    }
    func checkArray(){
        
        if segFriend.selectedSegmentIndex == 0 {
            if arrayFriend.count == 0 {
                lblNoRequest.text = "lbl.noFriends".localized
                lblNoRequest.isHidden = false
            }else{
                lblNoRequest.text = ""
                lblNoRequest.isHidden = true
                arrayFriend = arrayFriend.sorted{$0.name.compare($1.name) == ComparisonResult.orderedAscending}
            }
            
        }else{
            if arrayFriend.count == 0 {
                lblNoRequest.text = "lbl.noRequest".localized
                lblNoRequest.isHidden = false
            }else{
                lblNoRequest.text = ""
                lblNoRequest.isHidden = true
                arrayFriend = arrayFriend.sorted{$0.name.compare($1.name) == ComparisonResult.orderedAscending}
            }
        }
        
    }
    // MARK: - tableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrayFriend.count == 0{
            return 0
        }else{
            self.tblFriendList.restore()
            return arrayFriend.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendCell", for: indexPath as IndexPath) as! FriendCell
        let friend = arrayFriend[indexPath.row]
        if !isFromConnectScreen{
            cell.lblUserName.text = friend.userName
        }else{
            cell.lblUserName.text = friend.name
        }
        
        cell.imgUserPic.sd_setImage(with: URL.init(string: friend.userImg), placeholderImage: #imageLiteral(resourceName: "btn_loginbackground"), options: [], completed: nil)
        cell.selectionStyle = .none
        if listType == .buddies || listType == .followers || listType == .following{
            cell.btnAccept.isHidden = true
            cell.btnReject.isHidden = true
        }else{
            cell.btnAccept.isHidden = false
            cell.btnReject.isHidden = false
            cell.btnAccept.tag = indexPath.row
            cell.btnReject.tag = indexPath.row
            cell.btnAccept.addTarget(self, action: #selector(btnAcceptFriendRequestClicked(_:)), for: .touchUpInside)
            cell.btnReject.addTarget(self, action: #selector(btnRejectFriendRequestClicked(_:)), for: .touchUpInside)
        }
        
        if indexPath.row == arrayFriend.count - 1 && arrayFriend.count > 18{
            intPageNumber = intPageNumber+1
            getAllFriendList()
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if listType == .buddies || listType == .followers || listType == .following{
            return 60.0
        }else{
            return 102.0
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if isFromConnectScreen{
            let friend = self.arrayFriend[indexPath.row]
            let vc = Class_StageTwo.init(nibName: "Class_StageTwo", bundle: Bundle.main)
            let chatID = HSRealTimeMessagingLIbrary.function_CreatChatID(UInt64(friend.userId) ?? 0, UInt64(AppTheme.sharedInstance.currentUserData[0].UserID) ?? 0)
            vc.chatDetail = HSChatUser.init(isChatID: chatID, isID: UInt64(friend.userId) ?? 0, isName: friend.name)
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            var isViewExist = false
            for controller in self.navigationController!.viewControllers as Array {
                if controller is FriendProfileVC{
                    let friendsProfile = controller as! FriendProfileVC
                    isViewExist = true
                    friendsProfile.strFriendID = self.arrayFriend[indexPath.row].userId
                    friendsProfile.strFriendName = self.arrayFriend[indexPath.row].name
                    self.navigationController!.popToViewController(friendsProfile, animated: false)
                    break
                }
                else
                {
                    isViewExist = false
                    
                }
                //if controller.isKind(of: DashboardVC.self)
            }
            if !isViewExist
            {
                let friendProfileVC : FriendProfileVC = storyBoard.instantiateViewController(withIdentifier: "FriendProfileVC") as! FriendProfileVC
                friendProfileVC.strFriendID = self.arrayFriend[indexPath.row].userId
                friendProfileVC.strFriendName = self.arrayFriend[indexPath.row].name
                self.navigationController?.pushViewController(friendProfileVC, animated: true)
            }
        }
    }
    
    //MARK:
    //MARK: WebService Implementation
    
    func getAllFriendList(searchText : String = ""){
        let when = DispatchTime.now() + 0.0
        
        var params: [String : Any] = ["search_by_name": searchText, "page" : intPageNumber, "user_id" : userID, "pagination_limit" : paginationLimit]
        var strURL = ""
        if isFromConnectScreen{
            // get All Friends
            // User is comming from Connect screen
            strURL = APPURL.getAllUser
            params = ["search_by_name": searchText, "user_id" : AppTheme.sharedInstance.currentUserData[0].UserID, "page" : intPageNumber, "pagination_limit" : paginationLimit]
        }
        else{
            
            if listType == .buddies{
                strURL = APPURL.getFriendList
            }else if listType == .followers{
                strURL = APPURL.getFollower
            }else if listType == .following{
                strURL = APPURL.getFollowing
            }else {
                strURL = APPURL.getFriendRequest
            }
        }
        WebAPI.httpJSONRequest(viewController: self, url: strURL, params: params as [String : AnyObject], SuccessHandler: { (jsonResponse) in
            let Status = jsonResponse.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    if self.isFromConnectScreen{
                        self.parseAllUser(responseData: jsonResponse)
                    }else{
                        if self.listType == .following{
                            self.parseFollowingwith(responseData: jsonResponse)
                        }else if self.listType == .followers{
                            self.parseFollowersWith(responseData: jsonResponse)
                        }else if self.listType == .buddies{
                            self.parseBuddiesWith(responseData: jsonResponse)
                        }else{
                            self.parseBuddieRequestsWith(responseData: jsonResponse)
                        }
                    }
                }
            }else{
                self.checkArray()
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: jsonResponse.dictionaryValue["msg"]?.stringValue as AnyObject))
            }
        }) { (error) in
            self.checkArray()
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
    func respondFriendRequest(index : Int, status : String){
        let lang = AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as! String
        let params: [String : Any] = ["follower_id": arrayFriend[index].userId,"lang": lang]
        var strURL = ""
        if status == "Accept"{
            strURL = APPURL.acceptFriendRequest
        }else {
            strURL = APPURL.rejectFriendRequest
        }
        
        let when = DispatchTime.now() + 0.0
        WebAPI.httpJSONRequest(viewController: self, url: strURL, params: params as [String : AnyObject], SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.checkArray()
                    
                    Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
                    self.arrayFriend.remove(at: index)
                    self.tblFriendList.reloadData()
                }
            }else{
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
        }) { (error) in
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
    func parseFollowingwith(responseData : JSON){
        if responseData["responseData"]["following"].arrayValue.count > 0{
            for json in responseData["responseData"]["following"].arrayValue {
                let friend = FriendProfileDetails()
                friend.parser(profileData: json)
                self.arrayFriend.append(friend)
            }
        }else{
            self.tblFriendList.setEmptyView(title: "Followings", message: "You don't have any followings right now")
        }
        self.tblFriendList.reloadData()
        self.checkArray()
        self.tblFriendList.reloadData()
    }
    
    func parseFollowersWith(responseData : JSON){
        if responseData["responseData"]["follower"].arrayValue.count > 0{
            for json in responseData["responseData"]["follower"].arrayValue {
                let friend = FriendProfileDetails()
                friend.parser(profileData: json)
                self.arrayFriend.append(friend)
            }
        }else{
            self.tblFriendList.setEmptyView(title: "Followers", message: "You don't have any followers right now")
        }
        self.tblFriendList.reloadData()
        self.checkArray()
        self.tblFriendList.reloadData()
    }
    
    func parseBuddieRequestsWith(responseData : JSON){
        if responseData["responseData"].arrayValue.count > 0{
            for json in responseData["responseData"].arrayValue {
                let friend = FriendProfileDetails()
                friend.parser(profileData: json)
                self.arrayFriend.append(friend)
            }
        }else{
            self.tblFriendList.setEmptyView(title: "Friend Request", message: "You don't have any follow request right now")
        }
        self.tblFriendList.reloadData()
        self.checkArray()
        self.tblFriendList.reloadData()
    }
    
    func parseBuddiesWith(responseData : JSON){
        if responseData["responseData"]["buddies"].arrayValue.count > 0{
            for json in responseData["responseData"]["buddies"].arrayValue {
                let friend = FriendProfileDetails()
                friend.parser(profileData: json)
                self.arrayFriend.append(friend)
            }
        }else{
            self.tblFriendList.setEmptyView(title: "Buddies #", message: "You don't have any buddies right now")
        }
        self.tblFriendList.reloadData()
        self.checkArray()
        self.tblFriendList.reloadData()
    }
    func parseAllUser(responseData : JSON){
        if responseData["responseData"].arrayValue.count > 0{
            for json in responseData["responseData"].arrayValue {
                let friend = FriendProfileDetails()
                friend.parser(profileData: json)
                self.arrayFriend.append(friend)
            }
        }else{
            self.tblFriendList.setEmptyView(title: "Followers", message: "You don't have any followers right now")
        }
        self.tblFriendList.reloadData()
        self.checkArray()
        self.tblFriendList.reloadData()
    }
}

extension FriendsVC : UISearchBarDelegate{
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        print("searchBarShouldBeginEditing")
        return true
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidBeginEditing")
        searchBar.showsCancelButton = true
    }
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        print("searchBarShouldEndEditing")
        return true
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidEndEditing")
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
        arrayFriend.removeAll()
        self.tblFriendList.reloadData()
        intPageNumber = 1
        self.getAllFriendList()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let text = searchBar.text?.replacingOccurrences(of: " ", with: "")
        arrayFriend.removeAll()
        intPageNumber = 1
        self.tblFriendList.reloadData()
        if text!.count > 0{
            self.getAllFriendList(searchText: text!)
        }
    }
}
