//
//  NotificationsCell.swift
//  ClubBooks
//
//  Created by cis on 20/12/22.
//  Copyright © 2022 CIS. All rights reserved.
//

import UIKit

class NotificationsCell: UITableViewCell {
    @IBOutlet weak var lblTitle : UILabel?
    @IBOutlet weak var lblSubTitle : UILabel?
    @IBOutlet weak var imgView : UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
