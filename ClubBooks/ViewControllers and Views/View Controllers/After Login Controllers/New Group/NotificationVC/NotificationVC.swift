//
//  NotificationVC.swift
//  ClubBooks
//
//  Created by cis on 24/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class NotificationVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tblNotification : UITableView!
    var arrNotifcaitons : [Notifications] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        //--
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = UIColor.white
        //--
        showSettingButton()
        showLogoWithBackButon()
        self.setTitleWithLocalizedString(title: "nav.Notifications")
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        
        let nibNotifications = UINib.init(nibName: "NotificationsCell", bundle: nil)
        self.tblNotification.register(nibNotifications, forCellReuseIdentifier: "NotificationsCell")
        
        self.getNotifications()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - tableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrNotifcaitons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationsCell", for: indexPath as IndexPath) as! NotificationsCell
        let notification = arrNotifcaitons[indexPath.row]
        cell.lblTitle!.text = notification.title
        cell.lblSubTitle!.text = notification.subTitle
        cell.imgView?.sd_setImage(with: URL.init(string: notification.image), placeholderImage: #imageLiteral(resourceName: "btn.navigationlogo"), options: [], completed: nil)
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       
    }

}

extension NotificationVC{
    func getNotifications(){
        let when = DispatchTime.now() + 0.0
        WebAPI.httpJSONRequest(viewController: self, url: APPURL.getNotifications, params: [:], SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    
                    if responseData["responseData"].arrayValue.count > 0{
                        for json in responseData["responseData"].arrayValue {
                            let notifciation = Notifications()
                            notifciation.parser(json)
                            self.arrNotifcaitons.append(notifciation)
                        }
                        self.tblNotification.reloadData()
                    }
                    
//                    if self.intPagenumber == 1 && self.arrayPost.count > 0{
//                        DispatchQueue.main.async {
//                            let indexPath = NSIndexPath.init(row: 0, section: 0)
//                            self.tblDetails.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
//                        }
//                    }
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
        
    }
}
