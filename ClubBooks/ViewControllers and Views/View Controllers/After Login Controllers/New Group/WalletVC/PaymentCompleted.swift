//
//  PaymentCompleted.swift
//  ClubBooks
//
//  Created by cis on 02/09/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class PaymentCompleted: BaseViewController {
    var isFromPayForPost = Bool()
    @IBOutlet var btnMakeAnotherPayment : UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showBackButtonWith(Image: #imageLiteral(resourceName: "btn.back"))
        if isFromPayForPost{
            btnMakeAnotherPayment.isHidden = true
        }else{
            btnMakeAnotherPayment.isHidden = false
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnMakeAnotherPayment( _ sender : UIButton){
        var isViewExist = false
        for controller in self.navigationController!.viewControllers as Array {
            if controller is WalletAddMoney{
                let walletAddMoney = controller as! WalletAddMoney
                isViewExist = true
                self.navigationController!.popToViewController(walletAddMoney, animated: false)
                break
            }
            else
            {
                isViewExist = false
                
            }
        }
        if !isViewExist
        {
            let walletaddMoney : WalletAddMoney = storyBoard.instantiateViewController(withIdentifier: "WalletAddMoney") as! WalletAddMoney
            self.navigationController?.pushViewController(walletaddMoney, animated: true)
        }
    }
    @IBAction func btnDoneClicked( _ sender : UIButton){
        if !isFromPayForPost{
            var isViewExist = false
            for controller in self.navigationController!.viewControllers as Array {
                if controller is WalletVC{
                    let walletVC = controller as! WalletVC
                    isViewExist = true
                    walletVC.isFromDetailScreen = false
                    walletVC.strPostIDtoPay = ""
                    self.navigationController!.popToViewController(walletVC, animated: false)
                    break
                }
                else
                {
                    isViewExist = false
                    
                }
            }
            if !isViewExist
            {
                let walletVC : WalletVC = storyBoard.instantiateViewController(withIdentifier: "WalletVC") as! WalletVC
                walletVC.isFromDetailScreen = false
                walletVC.strPostIDtoPay = ""
                self.navigationController?.pushViewController(walletVC, animated: true)
            }
        }else{
            var isViewExist = false
            for controller in self.navigationController!.viewControllers as Array {
                if controller is LYTabBar{
                    appDelegate.window?.rootViewController = controller
                    break
                }
                else
                {
                    isViewExist = false
                    
                }
            }
            if !isViewExist
            {
                let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "LYTabBar") as! LYTabBar
                appDelegate.window?.rootViewController = homeVC
            }
        }
    }
}
