//
//  SearchFriendVC.swift
//  ClubBooks
//
//  Created by cis on 16/08/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

struct FriendData {
    let name: String
    let id: UInt64
}

protocol searchFriendVCDekegate : class{
    func didSelectUser(friend : FriendProfileDetails)
}
class SearchFriendVC: BaseViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
   
    let searchBar = UISearchBar()
    @IBOutlet var tblFriendList : UITableView!
    var arrayFriend = [FriendProfileDetails]()
    var searchfrienddelegate : searchFriendVCDekegate?
    var tableData:[HSChatUser] = []
    var frndData = [FriendData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let nibBook = UINib.init(nibName: "FriendCell", bundle: nil)
//        self.tblFriendList.register(nibBook, forCellReuseIdentifier: "FriendCell")
        self.tblFriendList.register(UINib.init(nibName: Cell_StageOne.identifier, bundle: nil), forCellReuseIdentifier: Cell_StageOne.identifier)
        tblFriendList.tableFooterView = UIView()
        self.showBackButtononPresentedViewControllerWith(Image: #imageLiteral(resourceName: "btn.back"))
    }
    override func viewDidAppear(_ animated: Bool) {
        animateSearchBar()
    }
    
    func animateSearchBar(){
        searchBar.alpha = 1
        searchBar.sizeToFit()
        self.navigationItem.rightBarButtonItem = nil
        searchBar.placeholder = ""
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as! UITextField
        textFieldInsideSearchBar.textColor = UIColor.black
        textFieldInsideSearchBar.backgroundColor = UIColor.init(red: 225/255.0, green: 225/255.0, blue: 225/255.0, alpha: 1.0)
        textFieldInsideSearchBar.placeHolderColor = UIColor.init(red: 200/255.0, green: 200/255.0, blue: 200/255.0, alpha: 1.0)
        textFieldInsideSearchBar.placeholder = ManageLocalization.getLocalaizedString(key: "txtfldplaceholder.search")
        self.navigationController?.navigationBar.topItem?.titleView = searchBar
        
        let animation = CAKeyframeAnimation()
        animation.keyPath = "position.x"
        animation.values = [self.view.superview?.frame.size.width as Any, 0]
        animation.keyTimes = [0.5, 1]
        animation.duration = 0.5
        animation.isAdditive = true
        
        self.navigationController?.navigationBar.topItem?.titleView?.layer.add(animation, forKey: "move")
        self.searchBar.becomeFirstResponder()
    }
    
    //MARK:
    //MARK: Searchbar Delegates
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        arrayFriend.removeAll()
        tblFriendList.reloadData()
        if searchBar.text == ""{
            return
        }else{
//            searchFriendWithName(strName: searchBar.text!)
            searchFriend(name: searchBar.text!)
        }
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchFriend(name: String) {
        var arrData = [FriendData]()
        for frnd in tableData {
            let id = UInt64(frnd.id)
            frndData.append(FriendData(name: frnd.name, id: id))
        }
        
        for i in 0..<frndData.count {
            if frndData[i].name.lowercased().contains(name.lowercased()) {
                arrData.append(frndData[i])
            }

        }
        self.frndData = arrData
        self.tblFriendList.reloadData()
        
       
    }
    
    //MARK:
    //MARK: TableView Delegates and DaraSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return frndData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_StageOne", for: indexPath as IndexPath) as! Cell_StageOne
        let friend = frndData[indexPath.row]
        cell.lbl_Name.text = friend.name
        HSRealTimeMessagingLIbrary.function_GetOnlineStatus(self.frndData[indexPath.row].id) { (isOnline) in
            if isOnline {
                cell.btn_Status.backgroundColor = UIColor.green
            } else {
                cell.btn_Status.backgroundColor = UIColor.lightGray
            }
        }
        let backView = UIView.init(frame: cell.contentView.frame); backView.backgroundColor = .clear
        cell.selectedBackgroundView = backView
//        cell.imgUserPic.sd_setImage(with: URL.init(string: friend.userImg), placeholderImage: #imageLiteral(resourceName: "btn_loginbackground"), options: [], completed: nil)
//        cell.selectionStyle = .none
//        cell.btnAccept.isHidden = true
//        cell.btnReject.isHidden = true
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let profile = FriendProfileDetails()
        profile.userId = String(self.frndData[indexPath.row].id)
        profile.name = self.frndData[indexPath.row].name
        self.searchfrienddelegate?.didSelectUser(friend: profile)
        self.dismiss(animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 60.0
    }
    
    //MARK:
    //MARK: Web Service Methods
    func searchFriendWithName(strName : String){
        let when = DispatchTime.now() + 0.0
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"
        let params: [String : Any] = ["search_by_name": strName, "lang": lang]
        WebAPI.httpJSONRequest(viewController: self, url: APPURL.getFriendList, params: params as [String : AnyObject], SuccessHandler: { (jsonResponse) in
            let Status = jsonResponse.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    if jsonResponse["responseData"].arrayValue.count > 0{
                        for json in jsonResponse["responseData"].arrayValue {
                            let friend = FriendProfileDetails()
                            friend.parser(profileData: json)
                            self.arrayFriend.append(friend)
                        }
                        self.tblFriendList.reloadData()
                    }
                    self.tblFriendList.reloadData()
                }
            }else{
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: jsonResponse.dictionaryValue["msg"]?.stringValue as AnyObject))
            }
        }) { (error) in
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
}
