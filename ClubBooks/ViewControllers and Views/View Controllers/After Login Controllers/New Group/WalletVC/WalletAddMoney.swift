//
//  WalletAddMoney.swift
//  ClubBooks
//
//  Created by cis on 30/04/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit
import TTGTagCollectionView
import StoreKit
import MBProgressHUD
import PassKit
class WalletAddMoney: BaseViewController, TTGTextTagCollectionViewDelegate, PKPaymentAuthorizationViewControllerDelegate {
    @IBOutlet var viewPriceTags : UIView!
    @IBOutlet var lblSelectedPrice : UILabel!
    var tagview = TTGTextTagCollectionView()
    var previousIndex : UInt!
    var products =  ["$ 10", "$ 15", "$ 20", "$ 30", "$ 40", "$ 50", "$ 60", "$ 70", "$ 80", "$ 90", "$ 100", "$ 110", "$ 120", "$ 130", "$ 140", "$ 150", "$ 160", "$ 170", "$ 180", "$ 190", "$ 200", "$ 210", "$ 220", "$ 230", "$ 240"]   // [SKProduct]()
    var selectedProduct = String() // SKProduct()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showLogoWithBackButon()
        self.title = "btn.title.addMoney".localized
        setUpTagView()
      
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        previousIndex = 999
//        reloadProducts()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setUpTagView(){
        if tagview != nil {
            tagview.removeFromSuperview()
        }
        tagview = TTGTextTagCollectionView.init(frame: viewPriceTags.frame)
        tagview.defaultConfig.textFont = UIFont.init(name: "Muli", size: 15.0)
        tagview.defaultConfig.backgroundColor = UIColor.lightGray
        tagview.defaultConfig.selectedBackgroundColor = UIColor.init(red: 43/255.0, green: 163/255.0, blue: 196/255.0, alpha: 1.0)
        tagview.delegate = self
        tagview.defaultConfig.shadowOpacity = 0.0
        viewPriceTags.addSubview(tagview)
        for product in products{
            tagview.addTag(RV_CheckDataType.getString(anyString: product as AnyObject))
        }
    }
    
    @IBAction func btnAddMoneyClicked(_ Sender : UIButton){
        if lblSelectedPrice.text == ""{
            AppTheme.sharedInstance.showAlert(message: "alert.wallet.selectMoneyToAdd".localized, viewcontroller: self)
            return
        }
        MBProgressHUD.showAdded(to: self.view, animated: true)
        applePayButtonTapped()
//        RageProducts.store.buyProduct(selectedProduct)
        
//        addMoneyToWallet()
    }
    
    @objc private func applePayButtonTapped() {
        // Cards that should be accepted
        let paymentNetworks:[PKPaymentNetwork] = [.amex,.masterCard,.visa]
        
        if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
            let request = PKPaymentRequest()
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            request.merchantIdentifier = appdelegate.merchantID
            request.countryCode = "US"
            request.currencyCode = "USD"
            request.supportedNetworks = paymentNetworks
            request.requiredShippingContactFields = [.name, .postalAddress]
            // This is based on using Stripe
            request.merchantCapabilities = .capability3DS
            
            var summaryItems = [PKPaymentSummaryItem]()
            let strAmount = self.lblSelectedPrice.text?.dropFirst(2)
            summaryItems.append(PKPaymentSummaryItem(label: "ClubBooks", amount :  NSDecimalNumber(string: String(format : "%@", RV_CheckDataType.getString(anyString: strAmount as AnyObject)))))
             request.paymentSummaryItems = summaryItems
            let authorizationViewController = PKPaymentAuthorizationViewController(paymentRequest: request)
            
            if let viewController = authorizationViewController {
                viewController.delegate = self
                present(viewController, animated: true, completion: nil)
            }
        }else{
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
       
    }
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        // Dismiss the Apple Pay UI
         MBProgressHUD.hide(for: self.view, animated: true)
        dismiss(animated: true, completion: nil)
    }
    
//    func reloadProducts() {
//        products = []
//
//        tagview.reload()
//
//        RageProducts.store.requestProducts{success, products in
//            if success {
//                self.products = products!
//                _ = self.products.sort {
//                    $0.price.compare($1.price) == ComparisonResult.orderedAscending
//                }
//                self.setUpTagView()
//
//                self.tagview.reload()
//            }
//        }
//    }
    //MARK:
    // MARK: - TagView Delegates and DataSource
    func textTagCollectionView(_ textTagCollectionView: TTGTextTagCollectionView!, didTapTag tagText: String!, at index: UInt, selected: Bool, tagConfig config: TTGTextTagConfig!) {
        if previousIndex != index{
            tagview.setTagAt(previousIndex, selected: false)
        }else{
            tagview.setTagAt(index, selected: true)
        }
        previousIndex = index
        lblSelectedPrice.text = textTagCollectionView.getTagAt(index)
        selectedProduct = products[RV_CheckDataType.getInteger(anyString: index as AnyObject)]
        tagview.reload()
    }
    //MARK:
    //MARK: Web Service Implementation
    func addMoneyToWalletUsingStripeTokem(strToken : String){
        let strAmount = self.lblSelectedPrice.text?.dropFirst(2)
        print(strAmount!)
        let when = DispatchTime.now() + 0.0
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"

        WebAPI.httpJSONRequest(viewController: self, url: APPURL.addMoneyToWalletUsingStripeToken, params: ["stripe_token" : strToken as AnyObject, "amount": strAmount as AnyObject, "is_card_saved" : "0" as AnyObject, "lang": lang as AnyObject], SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    let addMoneyCompleted = self.storyboard?.instantiateViewController(withIdentifier: "PaymentCompleted") as! PaymentCompleted
                    addMoneyCompleted.isFromPayForPost = false
                    self.navigationController?.pushViewController(addMoneyCompleted, animated: true)
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
            }else{
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
}
