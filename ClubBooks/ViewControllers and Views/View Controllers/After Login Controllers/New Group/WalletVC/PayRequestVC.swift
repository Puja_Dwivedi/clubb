//
//  PayRequestVC.swift
//  ClubBooks
//
//  Created by cis on 28/06/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

enum PayRequestType : Int
{
    case PayMoney = 0
    case RequestMoney
}

class PayRequestVC: BaseViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, searchFriendVCDekegate {
    var payRequestType : PayRequestType = PayRequestType.init(rawValue: 0)!
    @IBOutlet var segRequestMoney : UISegmentedControl!
    @IBOutlet var txtFldUserName : UITextField!
    @IBOutlet var txtFldAmount : UITextField!
    @IBOutlet var tblMoneyRequest : UITableView!
    @IBOutlet var scrollView : UIScrollView!
    var arrRequest = [PayRequest]()
    var isFromFriendsProfileScreen : Bool!
    var strUserName : String!
    var strUserID : String!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        payRequestType = .PayMoney
        segRequestMoney.setEnabled(true, forSegmentAt: 0)
        tblMoneyRequest.isHidden = true
        scrollView.isHidden = false
        if isFromFriendsProfileScreen{
            //make user interaction disabled for user
            txtFldUserName.isUserInteractionEnabled = false
            txtFldUserName.text = strUserName
        }else{
            txtFldUserName.isUserInteractionEnabled = true
            txtFldUserName.text = ""
        }
        self.showBackButtonWith(Image: #imageLiteral(resourceName: "btn.back"))
        segRequestMoney.setTitle(ManageLocalization.getLocalaizedString(key: "btn.PayorRequest"), forSegmentAt: 0)
        segRequestMoney.setTitle(ManageLocalization.getLocalaizedString(key: "segPaymentRequest"), forSegmentAt: 1)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = UIColor.white
        self.setTitleWithLocalizedString(title: "btn.PayorRequest")
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        self.navigationController?.navigationBar.tintColor = UIColor.lightGray
        let nibBook = UINib.init(nibName: "FriendCell", bundle: nil)
        self.tblMoneyRequest.register(nibBook, forCellReuseIdentifier: "FriendCell")
        tblMoneyRequest.tableFooterView = UIView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    } 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
    }
    //MARK: - UIbutton Clicks
    @IBAction func SegmentedControllerPayRequest(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            scrollView.isHidden = false
            tblMoneyRequest.isHidden = true
            payRequestType = .PayMoney
        case 1:
             scrollView.isHidden = true
             arrRequest.removeAll()
             self.getPendingPaymentRequest()
            tblMoneyRequest.isHidden = false
            payRequestType = .RequestMoney
        default:
            return
        }
    }
    @IBAction func btnSendMoneyToFreind( _ sender : UIButton){
        sendMoneyToFriend()
    }
    @IBAction func btnRequestMoneyFromFriend ( _ Sender : UIButton){
        if (txtFldAmount.text?.count)! > 2{
            requestMoneyFromFriend()
        }else{
            AppTheme.sharedInstance.showAlert(message: "alert.wallet.insertAmount".localized, viewcontroller: self)
        }
    }
    @IBAction func btnSelectFriend( _ sender : UIButton){
       
    }
    
    //MARK:
    //MARK: Search Friend Delegate
    func didSelectUser(friend: FriendProfileDetails) {
        txtFldUserName.text = friend.userName
        strUserID = friend.userId
    }
    
    //MARK: UITextfield Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtFldAmount{
            if textField.text?.count == 0{
                textField.text = "$ "
            }
        }
        else if textField == txtFldUserName{
            let searchvc = self.storyboard?.instantiateViewController(withIdentifier: "SearchFriendVC") as! SearchFriendVC
            searchvc.modalTransitionStyle = .flipHorizontal
            searchvc.searchfrienddelegate = self
            let navController = UINavigationController(rootViewController: searchvc)
            self.present(navController, animated: true, completion: nil)
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if range.location < 2{
            return false
        }
        return true
    }
    
     //MARK: UITableview Delegates and Databsource
    // MARK: - tableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrRequest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FriendCell", for: indexPath as IndexPath) as! FriendCell
        let request = arrRequest[indexPath.row]
         print(request.amount)
        cell.lblUserName.text = request.username
        cell.lblDetail.text = "$ " + request.amount
        cell.btnAccept.setTitle("lbl.title.payMoney".localized, for: .normal)
        cell.btnReject.setTitle("lbl.title.deleteRequest".localized, for: .normal)
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 102.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK:
    //MARK: Callback Methods from SearchfriendVC
    
    //MARK:
    //MARK: Web Service Call
    func requestMoneyFromFriend(){
        let when = DispatchTime.now() + 0.0
        let amount = txtFldAmount.text?.replacingOccurrences(of: "$ ", with: "")
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"

        WebAPI.httpJSONRequest(viewController: self, url: APPURL.sendMoneyReuestToFriend, params: ["requested_to": strUserID  as AnyObject, "amount" : amount as AnyObject, "lang": lang as AnyObject], SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.txtFldAmount.text = ""
                    self.txtFldAmount.resignFirstResponder()
                     Manager.sharedInstance.showAlert(self, message: "alert.sentrequestMoney".localized)
                }
            }else{

                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
        }) { (error) in

            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
    
    
    func sendMoneyToFriend(){
        let when = DispatchTime.now() + 0.0
        let amount = txtFldAmount.text?.replacingOccurrences(of: "$ ", with: "")
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"

        WebAPI.httpJSONRequest(viewController: self, url: APPURL.payMoneyToFriend, params: ["receiver_id": strUserID  as AnyObject, "amount" : amount as AnyObject, "message" : "" as AnyObject, "lang": lang as AnyObject], SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.txtFldAmount.text = ""
                    self.txtFldAmount.resignFirstResponder()
                    Manager.sharedInstance.showAlert(self, message: "alert.amountSent".localized)
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
    func getPendingPaymentRequest(){
        let when = DispatchTime.now() + 0.0
        WebAPI.httpJSONRequest(viewController: self, url: APPURL.getPendingMoneyRequest, params: [:], SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    if responseData["responseData"].arrayValue.count > 0{
                        for json in responseData["responseData"].arrayValue {
                            let payrequest = PayRequest()
                            payrequest.parser(requestData: json)
                            self.arrRequest.append(payrequest)
                        }
                        self.tblMoneyRequest.reloadData()
                    }
                    
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
    
}
