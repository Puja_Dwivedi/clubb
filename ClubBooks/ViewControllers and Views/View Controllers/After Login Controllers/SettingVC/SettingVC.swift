//
//  SettingVC.swift
//  ClubBooks
//
//  Created by cis on 27/12/18.
//  Copyright © 2018 CIS. All rights reserved.
//

import UIKit
import MBProgressHUD
class SettingVC: BaseViewController {
    @IBOutlet var viewProfileInformation : UIView!
    @IBOutlet var viewNotification : UIView!
    @IBOutlet var segLanguage : UISegmentedControl!
    @IBOutlet weak var btnDeleteYourAccount: UIButton!
    
    //--- Rating Option ---
    override func viewDidLoad() {
        super.viewDidLoad()
        showLogoWithBackButon()
        
        self.title = ManageLocalization.getLocalaizedString(key: "nav.Setting")
        self.navigationController?.navigationBar.showShadow()
        
        self.viewProfileInformation.addShadow(location: .top, color: UIColor.init(red: 230/255.0, green: 230/255.0, blue: 230/255.0, alpha: 1.0), opacity: 0.5, radius: 3.0)
        self.viewNotification.addShadow(location: .top, color: UIColor.init(red: 230/255.0, green: 230/255.0, blue: 230/255.0, alpha: 1.0), opacity: 0.5, radius: 3.0)
        btnDeleteYourAccount.setTitle("btn.title.deleteaccount".localized, for: .normal)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        ManageLocalization.deviceLang == "en" ? (AppDelegate.shared.langId = LangId.english) : (AppDelegate.shared.langId = LangId.spanish)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if ManageLocalization.deviceLang == "en"{
            segLanguage.selectedSegmentIndex = 0
        }else{
            segLanguage.selectedSegmentIndex = 1
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnEditProFileClicked( _ Sender : UIButton){
        let editProfile = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        self.navigationController?.pushViewController(editProfile, animated: true)
        
    }
    @IBAction func btnRatingFeedbackClicked( _ sender : UIButton){
        let feedbackVC = self.storyboard?.instantiateViewController(withIdentifier: "FeedbackVC") as! FeedbackVC
        let navController = UINavigationController(rootViewController: feedbackVC)
        feedbackVC.view.backgroundColor = UIColor.white
        feedbackVC.modalTransitionStyle = .crossDissolve
        feedbackVC.modalPresentationStyle = .overFullScreen
        feedbackVC.isNavigationbarHidden = false
        self.present(navController, animated: true, completion: nil)
    }
    
    @IBAction func btnChangePasswordClicked( _ sender : UIButton){
        let ChangePassword : ChangePasswordVC = storyBoard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        self.navigationController?.pushViewController(ChangePassword, animated: true)
    }
    
    @IBAction func btnWalletClicked( _ sender : UIButton){
        var postURL = "\(APPURL.Domains.Dev)wallet"
        AppTheme.sharedInstance.moveToSafari(viewController: self, postURL : &postURL)
    }
    @IBAction func btnPrivacyClicked( _ sender : UIButton){
        let url = "https://clubbooks.com/legal"
        if let url = URL(string: url) {
            UIApplication.shared.open(url)
        }
    }
    @IBAction func btnDeleteAccountClicked( _ sender : UIButton)
    {
        alertWithTextField(title: "btn.title.deleteaccount".localized, message: "alert.title.deleteaccount".localized, placeholder: "textfield.type.delete".localized, cancelTitle: "alert.cancel".localized, okTitle: "Delete".localized) { result in
            if result == "DELETE" {
                self.deleteAccount()
            }else{
                AppTheme.sharedInstance.showAlert(message: "alert.title.deleteaccount".localized, viewcontroller: self)
            }
        }
    }

    @IBAction func SegmentedControllerLanguage(_ sender: UISegmentedControl) {
       var selectedLanguage = ""
        switch sender.selectedSegmentIndex {
        case 0:
          selectedLanguage = "en"
        case 1:
           selectedLanguage = "es"
        default:
            return
        }
        if ManageLocalization.deviceLang != selectedLanguage{
            let alert : UIAlertController = UIAlertController(title: "ClubBooks", message: "alert.title.changeLanguage".localized, preferredStyle: .alert)
            
            let YesAction = UIAlertAction(title: "alert.ok".localized, style: .default, handler: { (action) in
                ManageLocalization.deviceLang = selectedLanguage
                AppTheme.sharedInstance.ClubBooksUserDefaults.setValue(selectedLanguage, forKey: KEY.UserDefaults.selectedLanguage)
                AppTheme.sharedInstance.ClubBooksUserDefaults.synchronize()
                let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "LYTabBar") as! LYTabBar
                appDelegate.window?.rootViewController = homeVC
            })
            let CancelAction = UIAlertAction.init(title: "alert.cancel".localized, style: .destructive) { (action) in
                if selectedLanguage == "en"{
                    selectedLanguage = "es"
                    self.segLanguage.selectedSegmentIndex = 1
                }else{
                    selectedLanguage = "en"
                    self.segLanguage.selectedSegmentIndex = 0
                }
            }
            alert.addAction(YesAction)
            alert.addAction(CancelAction)
            self.present(alert, animated: true, completion: {
            })
        }
    }
    ///---
    @IBAction func btnNotifications( _ Sender : UIButton){
        let objTarget = storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(objTarget, animated: true)
    }
    @IBAction func btnSignoutClicked ( _ sender : UIButton) {
        let alert : UIAlertController = UIAlertController(title: "ClubBooks", message: "alert.logoutTitle".localized, preferredStyle: .alert)
        
        let YesAction = UIAlertAction(title: "alert.ok".localized, style: .default, handler: { (action) in
            
            let params: [String : Any] = [:]
            WebAPI.httpJSONRequest(viewController: self, url: APPURL.Logout, params: params as [String : AnyObject], SuccessHandler: { (jsonResponse) in
                let Status = jsonResponse.dictionary!["status"]?.boolValue
                AppTheme.sharedInstance.currentUserData.removeAll()
                AppTheme.sharedInstance.saveUserDataToDefault()
                if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                    let IntroViewController = self.storyboard?.instantiateViewController(withIdentifier: "IntroViewController") as! IntroViewController
                    self.navigationController?.pushViewController(IntroViewController, animated: false)
                }else{
                    let IntroViewController = self.storyboard?.instantiateViewController(withIdentifier: "IntroViewController") as! IntroViewController
                    self.navigationController?.pushViewController(IntroViewController, animated: false)
                }
            }) { (error) in
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
            }
        })
        let CancelAction = UIAlertAction.init(title: "alert.cancel".localized, style: .destructive) { (action) in
            
        }
        alert.addAction(YesAction)
        alert.addAction(CancelAction)
        self.present(alert, animated: true, completion: {
        })
        
    }
   
    func deleteAccount(){
        let alert : UIAlertController = UIAlertController(title: "ClubBooks", message: "alert.delete.confirmation".localized, preferredStyle: .alert)

        let YesAction = UIAlertAction(title: "alert.ok".localized, style: .default, handler: { (action) in
        WebAPI.httpJSONRequest(viewController: self, url: APPURL.deleteAccount, params: [:] as [String : AnyObject], SuccessHandler: { (jsonResponse) in
            let Status = jsonResponse.dictionary!["status"]?.boolValue
            AppTheme.sharedInstance.currentUserData.removeAll()
            AppTheme.sharedInstance.saveUserDataToDefault()
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                let IntroViewController = self.storyboard?.instantiateViewController(withIdentifier: "IntroViewController") as! IntroViewController
                self.navigationController?.pushViewController(IntroViewController, animated: false)
            }else{
                let IntroViewController = self.storyboard?.instantiateViewController(withIdentifier: "IntroViewController") as! IntroViewController
                self.navigationController?.pushViewController(IntroViewController, animated: false)

            }
        }) { (error) in

            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
        })
        let CancelAction = UIAlertAction.init(title: "alert.cancel".localized, style: .destructive) { (action) in

        }
        alert.addAction(YesAction)
        alert.addAction(CancelAction)
        self.present(alert, animated: true, completion: {
        })
    }
}
