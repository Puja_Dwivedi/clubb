//
//  FeedbackVC.swift
//  ClubBooks
//
//  Created by cis on 24/04/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit
import MBProgressHUD
class FeedbackVC: BaseViewController {
    //--- Rating Option ---
    @IBOutlet var btnClose : UIButton!
    @IBOutlet var ratingView : CosmosView!
    @IBOutlet var txtViewComment : UITextView!
    @IBOutlet var lblUserName : UILabel!
    var isNavigationbarHidden = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showBackButtononPresentedViewControllerWith(Image: #imageLiteral(resourceName: "btn.back"))
        self.title = "nav.rateReview".localized
        lblUserName.text = AppTheme.sharedInstance.currentUserData[0].UserName
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        if isNavigationbarHidden {
            btnClose.isHidden = false
        }else{
            btnClose.isHidden = true
        }
        // Do any additional setup after loading the view.
    }
    @objc func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnCloseClicked( sender : UIButton){
        
    }
    @IBAction func btnSubmitRating(sender : UIButton)
    {
        self.view.endEditing(true)
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"

        let params: [String : Any] = ["rating" : RV_CheckDataType.getString(anyString: ratingView.rating as AnyObject), "lang": lang, 
                                      "comment" : txtViewComment.text]
        WebAPI.httpJSONRequest(viewController: self, url: APPURL.postSubmitRating, params: params as [String : AnyObject], SuccessHandler: { (jsonResponse) in
            let Status = jsonResponse.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                AppTheme.sharedInstance.currentUserData[0].appRaing = RV_CheckDataType.getString(anyString: self.ratingView.rating as AnyObject)
                AppTheme.sharedInstance.saveUserDataToDefault()
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: jsonResponse.dictionaryValue["msg"]?.stringValue as AnyObject))
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: jsonResponse.dictionaryValue["msg"]?.stringValue as AnyObject))
            }
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
        
    }
}
