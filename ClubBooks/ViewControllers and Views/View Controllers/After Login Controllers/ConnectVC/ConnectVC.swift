//
//  ConnectVC.swift
//  ClubBooks
//
//  Created by cis on 24/12/18.
//  Copyright © 2018 CIS. All rights reserved.
//

import UIKit
import CoreLocation
class ConnectVC: BaseViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, searchFriendVCDekegate {
    @IBOutlet var tblUser : UITableView!
    @IBOutlet var collectionView : UICollectionView!
    @IBOutlet var viewMessage : UIView!
    @IBOutlet var viewTblBg : UIView!
    @IBOutlet var segOnOffline : UISegmentedControl!
    @IBOutlet var lblMessages : UILabel!
    @IBOutlet var lblOnline : UILabel!
    @IBOutlet var lblCount : UILabel!
    private var tableData:[HSChatUser] = []
    private var arrNearUsers : [HSUser] = []
    private var arrNearByUserLocations : [CLLocation] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "nav.Connect".localized.uppercased()
        segOnOffline.setTitle(ManageLocalization.getLocalaizedString(key: "btn.Title.offline"), forSegmentAt: 0)
        segOnOffline.setTitle(ManageLocalization.getLocalaizedString(key: "btn.Title.online"), forSegmentAt: 1)
        showlogoLeft()
        
        viewMessage.addShadowAround(layer: viewMessage.layer)
        viewTblBg.addShadowAround(layer: viewTblBg.layer)
        segOnOffline.addShadowAround(layer: segOnOffline.layer)
        viewMessage.layer.borderWidth = 0.3
        viewMessage.layer.borderColor = UIColor.lightGray.cgColor
        viewTblBg.layer.borderWidth = 0.3
        viewTblBg.layer.borderColor = UIColor.lightGray.cgColor
        segOnOffline.layer.borderWidth = 0.3
        segOnOffline.layer.borderColor = UIColor.lightGray.cgColor
        segOnOffline.layer.cornerRadius = 3.0
        segOnOffline.clipsToBounds = true
        self.addRightBarButtonItems()
        self.tblUser.register(UINib.init(nibName: Cell_StageOne.identifier, bundle: nil), forCellReuseIdentifier: Cell_StageOne.identifier)
        
        HSRealTimeMessagingLIbrary.function_GetOnlineStatus(HSRealTimeMessagingLIbrary.HSCurrentUserID()) { (isOnline) in
            if isOnline {self.segOnOffline.selectedSegmentIndex = 1} else {self.segOnOffline.selectedSegmentIndex = 0}
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.function_LoadData()
        tblUser.reloadData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.function_PushController(data:)), name: Notification.Name("firebaseChatNotifications"), object: nil)
        self.function_LoadNearByUsers()
        
    }
    func addRightBarButtonItems()
    {
        let btnSetting = UIButton.init(type: .custom)
        btnSetting.setImage(UIImage(named: "icon.verticalDots"), for: .normal)
        btnSetting.addTarget(self, action: #selector(btnOpenSettingClicked), for: .touchUpInside)
        let btnCreateChat = UIButton.init(type: .custom)
        btnCreateChat.setImage(UIImage(named: "icon_createChat"), for: .normal)
        btnCreateChat.addTarget(self, action: #selector(btnOpenFriendLisr(_:)), for: .touchUpInside)
        let btnSearch = UIButton.init(type: .custom)
        btnSearch.setImage(#imageLiteral(resourceName: "tab.unselected.search"), for: .normal)
        btnSearch.addTarget(self, action: #selector(btnSearchUserClicked(_:)), for: .touchUpInside)
        let stackview = UIStackView.init(arrangedSubviews: [btnSearch, btnCreateChat, btnSetting])
        stackview.distribution = .fillEqually
        stackview.axis = .horizontal
        stackview.alignment = .center
        stackview.spacing = 10
        let rightBarButton = UIBarButtonItem(customView: stackview)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    @IBAction func btnSearchUserClicked( _ sender : UIButton)
    {
        let searchvc = self.storyboard?.instantiateViewController(withIdentifier: "SearchFriendVC") as! SearchFriendVC
        searchvc.modalTransitionStyle = .flipHorizontal
        searchvc.searchfrienddelegate = self
        searchvc.tableData = self.tableData
        let navController = UINavigationController(rootViewController: searchvc)
        self.present(navController, animated: true, completion: nil)
    }
    
    @IBAction func btnOpenFriendLisr( _ sender : UIButton){
        let friendVC : FriendsVC = storyBoard.instantiateViewController(withIdentifier: "FriendsVC") as! FriendsVC
        friendVC.isFromConnectScreen = true
        friendVC.userID = AppTheme.sharedInstance.currentUserData[0].UserID
        friendVC.listType = .buddies
        friendVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(friendVC, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "firebaseChatNotifications"), object: nil)
    }
    //MARK: - From Push notifications
    @objc func function_PushController(data: Notification) {
//        let chatID = RV_CheckDataType.getString(anyString: data.userInfo!["chatID"] as AnyObject)
            let userID = UInt64(data.userInfo!["user_ID"] as! String)!
//        let userName = RV_CheckDataType.getString(anyString: data.userInfo!["username"] as AnyObject)
        let chatID = HSRealTimeMessagingLIbrary.function_CreatChatID(userID, UInt64(AppTheme.sharedInstance.currentUserData[0].UserID) ?? 0)
        
        let aps = RV_CheckDataType.getDictionary(anyDict: data.userInfo?["aps"] as AnyObject)
        if aps.count > 0
        {
            if let alert = aps.value(forKey: "alert") as? NSDictionary
            {
                let title = RV_CheckDataType.getString(anyString: alert.value(forKey: "title") as AnyObject)
                let vc = Class_StageTwo.init(nibName: "Class_StageTwo", bundle: Bundle.main)
                //        vc.hidesBottomBarWhenPushed = true
                vc.chatDetail = HSChatUser.init(isChatID: chatID, isID: userID, isName: title )
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    //MARK: - UIbutton Clicks
    @IBAction func SegmentedControllerOnlineOfline(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            HSRealTimeMessagingLIbrary.function_UpdateOnlineStatus(HSRealTimeMessagingLIbrary.HSCurrentUserID(), false)
        case 1:
            HSRealTimeMessagingLIbrary.function_UpdateOnlineStatus(HSRealTimeMessagingLIbrary.HSCurrentUserID(), true)
        default:
            return
        }
    }
    //MARK:
    //MARK: Search User Delegates
    func didSelectUser(friend: FriendProfileDetails)
    {
        let vc = Class_StageTwo.init(nibName: "Class_StageTwo", bundle: Bundle.main)
        let chatID = HSRealTimeMessagingLIbrary.function_CreatChatID(UInt64(friend.userId) ?? 0, UInt64(AppTheme.sharedInstance.currentUserData[0].UserID) ?? 0)
        vc.chatDetail = HSChatUser.init(isChatID: chatID, isID: UInt64(friend.userId) ?? 0, isName: friend.name)
        //        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK:
    //MARK: UICollectionView Datasource and Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrNearUsers.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let connectUsers = collectionView.dequeueReusableCell(withReuseIdentifier: "ConnectNearByUsers", for: indexPath) as! ConnectNearByUsers
        connectUsers.setProfileCircle(gender: Int(arrNearUsers[indexPath.row].gender))
        connectUsers.lblName.text = arrNearUsers[indexPath.row].name
        let distance = arrNearByUserLocations[indexPath.row].distance(from: self.locationVC.locationManager.location ?? CLLocation.init(latitude: 0.0, longitude: 0.0))
        let meterDistance = Measurement.init(value: distance, unit: UnitLength.meters)
        let feetDistance = meterDistance.converted(to: UnitLength.feet)
        if AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) == nil || AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as! String == "en"{
            connectUsers.lblDistance.text = String.init(format: "%.2f Ft", feetDistance.value)
        }else{
            connectUsers.lblDistance.text = String.init(format: "%.2f M", meterDistance.value)
        }
        return connectUsers
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let friend = self.arrNearUsers[indexPath.row]
        let vc = Class_StageTwo.init(nibName: "Class_StageTwo", bundle: Bundle.main)
        //        vc.hidesBottomBarWhenPushed = true
        let chatID = HSRealTimeMessagingLIbrary.function_CreatChatID(friend.id, UInt64(AppTheme.sharedInstance.currentUserData[0].UserID) ?? 0)
        vc.chatDetail = HSChatUser.init(isChatID: chatID, isID: friend.id, isName: friend.name)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 100, height: 110)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    // MARK: - tableView
    //MARK: Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Cell_StageOne.identifier) as? Cell_StageOne else { return UITableViewCell.init(style: .default, reuseIdentifier: Cell_StageOne.identifier) }
        cell.lbl_Name.text = "\(self.tableData[indexPath.row].name)"
        HSRealTimeMessagingLIbrary.function_GetOnlineStatus(self.tableData[indexPath.row].id) { (isOnline) in
            if isOnline {
                cell.btn_Status.backgroundColor = UIColor.green
            } else {
                cell.btn_Status.backgroundColor = UIColor.lightGray
            }
        }
        let backView = UIView.init(frame: cell.contentView.frame); backView.backgroundColor = .clear
        cell.selectedBackgroundView = backView
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.width/7
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = Class_StageTwo.init(nibName: "Class_StageTwo", bundle: Bundle.main)
        //        vc.hidesBottomBarWhenPushed = true
        vc.chatDetail = self.tableData[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK: Functions
    func function_LoadData() {
        self.tableData.removeAll()
        HSRealTimeMessagingLIbrary.function_LoadChatUsers(isID: HSRealTimeMessagingLIbrary.HSCurrentUserID()) { (user) in
            self.tableData.append(user)
            self.tableData = self.tableData.sorted(by: {$0.time > $1.time})
            self.lblCount.text = "0"
            for datum in self.tableData {
                HSRealTimeMessagingLIbrary.function_GetOnlineStatus(datum.id) { (isOnline) in
                    if isOnline {
                        self.lblCount.text = String(Int(self.lblCount.text ?? "0")! + 1)
                    }else{
                        
                    }
                }
            }
            self.tblUser.reloadData()
            if self.tableData.count > 0 {
                self.tblUser.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .none, animated: true)
            }
        }
    }
    func function_LoadNearByUsers() {
        self.arrNearUsers.removeAll()
        self.arrNearByUserLocations.removeAll()
        HSRealTimeMessagingLIbrary.function_LoadNearByUsers(forID: HSRealTimeMessagingLIbrary.HSCurrentUserID()) { (user, userlocation) in
            self.arrNearUsers.append(user)
            self.arrNearByUserLocations.append(userlocation)
            self.collectionView.reloadData()
        }
        
    }
}
class ConnectNearByUsers: UICollectionViewCell {
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblDistance : UILabel!
    
    func setProfileCircle(gender: Int) {
        let blue =  UIColor.init(red: 40/255.0, green: 162/255.0, blue: 199/255.0, alpha: 1.0)
        let pink = UIColor.init(red: 255/255.0, green: 0/255.0, blue: 255/255.0, alpha: 1.0)
        let grey = UIColor.gray
        if gender == 0 {
            //Male
            imgView.backgroundColor = blue
            lblDistance.textColor = blue
        }else if gender == 1 {
            //Female
            imgView.backgroundColor = pink
            lblDistance.textColor = pink
        }else{
            //Other
            imgView.backgroundColor = grey
            lblDistance.textColor = grey
        }
    }
}

