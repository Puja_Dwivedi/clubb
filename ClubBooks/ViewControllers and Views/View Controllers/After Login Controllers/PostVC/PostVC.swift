//
//  PostVC.swift
//  ClubBooks
//
//  Created by cis on 24/12/18.
//  Copyright © 2018 CIS. All rights reserved.
//

import UIKit

class PostVC: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate, GetPickerValueDelagate  {
    
    

    @IBOutlet var collectionPost : UICollectionView!
    @IBOutlet var viewSearchBar : UIView!
    @IBOutlet var btnSelectPostType : UIButton!
    //MARK:-- View did load --
    override func viewDidLoad() {
        super.viewDidLoad()
        showlogoLeft()
        showSettingButton()
        self.tabBarController?.tabBar.addShadowAround(layer: (self.tabBarController?.tabBar.layer)!)
        viewSearchBar.addShadowAround(layer: viewSearchBar.layer)
        viewSearchBar.layer.borderWidth = 0.5
        viewSearchBar.layer.borderColor = UIColor.lightGray.cgColor
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.title = "nav.Post".localized.uppercased()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnChoosePostClicked(_ sender : UIButton){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Picker") as! Picker
        vc.delegate = self
        vc.index = sender.tag
        vc.selectedValue = "A"
        let arrOptions = ["lbl.title.booksLabel", "lbl.title.meetupLabel", "lbl.title.bookChatLabel", "lbl.title.topicLabel", "lbl.title.examLabel", "lbl.title.quizeLabel", "lbl.title.notesLabel"]
        vc.data = arrOptions
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: false, completion: nil)
    }

    //MARK:-- Collection view delegate methods --
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeHeaderCell", for: indexPath as IndexPath) as! HomeHeaderCell
        switch indexPath.row {
        case 0:
            cell.imgView.isHidden = true
            cell.lblHeading.isHidden = true
        case 1:
            cell.imgView.image = UIImage(named: "books")
            cell.lblHeading.text = ManageLocalization.getLocalaizedString(key: "lbl.title.booksLabel").uppercased()
        case 2:
            cell.imgView.isHidden = true
            cell.lblHeading.isHidden = true
        case 3:
            cell.imgView.image = UIImage(named: "class_notes")
            cell.lblHeading.text = ManageLocalization.getLocalaizedString(key: "lbl.title.notesLabel").uppercased()
            
        case 4:
            cell.imgView.image = UIImage(named: "exams")
            cell.lblHeading.text = ManageLocalization.getLocalaizedString(key: "lbl.title.examLabel").uppercased()
            
        case 5:
            cell.imgView.image = UIImage(named: "quizzes")
            cell.lblHeading.text = ManageLocalization.getLocalaizedString(key: "lbl.quizes").uppercased()
            
        case 6:
            cell.imgView.image = UIImage(named: "meetups")
            cell.lblHeading.text = ManageLocalization.getLocalaizedString(key: "lbl.title.meetupLabel").uppercased()
        case 7:
            cell.imgView.image = UIImage(named: "chats_on_nots")
            cell.lblHeading.text = ManageLocalization.getLocalaizedString(key: "lbl.title.bookChatLabel").uppercased()
        case 8:
            cell.imgView.image = UIImage(named: "chats_on_topics")
            cell.lblHeading.text = ManageLocalization.getLocalaizedString(key: "lbl.title.topicLabel").uppercased()
        default:
            cell.lblHeading.text = ""
        }
        
        return cell
    }
    //MARK:
    //MARK: Picker Delegates
    func getValue(_ value: String, index: Int, iflag: Bool) {
        btnSelectPostType.setTitle(ManageLocalization.getLocalaizedString(key: value), for: .normal)
        var objTarget = UIViewController()
        //var identifier = ""
        objTarget = storyboard?.instantiateViewController(withIdentifier: "PostCreatedVC") as! PostCreatedVC
        switch index {
        case 0:
            print("BOOKS")
        //objTarget = storyboard?.instantiateViewController(withIdentifier: "BookInitialVC") as! BookInitialVC
        case 1:
            print("BOOKS")
            objTarget = storyboard?.instantiateViewController(withIdentifier: "BookInitialVC") as! BookInitialVC
        case 2:
            print("BOOKS")
            objTarget = storyboard?.instantiateViewController(withIdentifier: "BookInitialVC") as! BookInitialVC
        case 3:
            print("CLASS NOTES")
            objTarget = storyboard?.instantiateViewController(withIdentifier: "ClassNotesVC") as! ClassNotesVC
        case 4:
            print("EXAMS")
            objTarget = storyboard?.instantiateViewController(withIdentifier: "ExamPostVC") as! ExamPostVC
        case 5:
            print("QUIZZES")
            objTarget = storyboard?.instantiateViewController(withIdentifier: "QuizzesVC") as! QuizzesVC
        case 6:
            print("MEETuPS")
            objTarget = storyboard?.instantiateViewController(withIdentifier: "CreateMeetupsVC") as! CreateMeetupsVC
        case 7:
            print("CHATS ON BOOKS")
            objTarget = storyboard?.instantiateViewController(withIdentifier: "ChatOnBookVC") as! ChatOnBookVC
        case 8:
            print("CHATS ON TOPICS")
            objTarget = storyboard?.instantiateViewController(withIdentifier: "ChatOnTopicVC") as! ChatOnTopicVC
        default:
            print("No controller")
        }
        
        self.navigationController?.pushViewController(objTarget, animated: true)
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        var objTarget = UIViewController()
        //var identifier = ""
        objTarget = storyboard?.instantiateViewController(withIdentifier: "PostCreatedVC") as! PostCreatedVC
        switch indexPath.row {
        case 0:
            return
        //objTarget = storyboard?.instantiateViewController(withIdentifier: "BookInitialVC") as! BookInitialVC
        case 1:
            print("BOOKS")
            objTarget = storyboard?.instantiateViewController(withIdentifier: "BookInitialVC") as! BookInitialVC
        case 2:
            return
        //objTarget = storyboard?.instantiateViewController(withIdentifier: "BookInitialVC") as! BookInitialVC
        case 3:
            print("CLASS NOTES")
            objTarget = storyboard?.instantiateViewController(withIdentifier: "ClassNotesVC") as! ClassNotesVC
        case 4:
            print("EXAMS")
            objTarget = storyboard?.instantiateViewController(withIdentifier: "ExamPostVC") as! ExamPostVC
        case 5:
            print("QUIZZES")
            objTarget = storyboard?.instantiateViewController(withIdentifier: "QuizzesVC") as! QuizzesVC
        case 6:
            print("MEETuPS")
            objTarget = storyboard?.instantiateViewController(withIdentifier: "CreateMeetupsVC") as! CreateMeetupsVC
        case 7:
            print("CHATS ON BOOKS")
            objTarget = storyboard?.instantiateViewController(withIdentifier: "ChatOnBookVC") as! ChatOnBookVC
        case 8:
            print("CHATS ON TOPICS")
            objTarget = storyboard?.instantiateViewController(withIdentifier: "ChatOnTopicVC") as! ChatOnTopicVC
        default:
            print("No controller")
        }
        self.navigationController?.pushViewController(objTarget, animated: true)
    }
}
extension PostVC : UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let nbCol = 3
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(nbCol - 1))
        let screenWidth = UIScreen.main.bounds.width
        let scaleFactor = (screenWidth / 3) - 18
        return CGSize(width: scaleFactor, height: scaleFactor+18)
    }
}


