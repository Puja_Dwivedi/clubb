//
//  CreateMeetupsVC.swift
//  ClubBooks
//
//  Created by cis on 19/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit
import MBProgressHUD

enum PickerType : Int {
    case Date = 0
    case Time = 1
}

class CreateMeetupsVC: BaseViewController, GetDateDelagate, UITextFieldDelegate {
    var pickerType : PickerType?
   
    @IBOutlet var allTextField : [UITextField]!
    @IBOutlet var txtViewNotes : UITextView!
    @IBOutlet var viewNotes : UIView!
    @IBOutlet var viewMain : UIView!
    
    @IBOutlet var txtFldTitle : UITextField!
    @IBOutlet var txtFldPlace : UITextField!
    @IBOutlet var txtFldTime : UITextField!
    @IBOutlet var txtFldDate : UITextField!
    var dicCreateMeetUp = [String : AnyObject]()
    @IBOutlet var btnAnonymousChat : CheckBoxButton!
    var strServerDate = "";

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTitleWithLocalizedString(title: "nav.CreateMeetups")
        showLogoWithBackButon()
        // Do any additional setup after loading the view.
        viewNotes.addShadowAround(layer: viewNotes.layer)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        for textField in allTextField
        {
            textField.addShadowAround(layer: textField.layer)
            textField.setLeftPaddingPoints(10.0)
            textField.text = ""
        }
    }
    func clearData(){
        for textField in allTextField
        {
            textField.addShadowAround(layer: textField.layer)
            textField.setLeftPaddingPoints(10.0)
            textField.text = ""
        }
        self.txtViewNotes.text = ""
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func openPicker(pickertype : PickerType){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DatePicker") as! DatePicker
        vc.delegate = self
        vc.selectedValue = ""
        vc.pickerType = pickertype
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: false, completion: nil)
    }
    
    //MARK:-
    //MARK:- UITextFieldDelegates
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtFldDate {
            pickerType = PickerType.Date
            openPicker(pickertype: pickerType!)
            return false
        }else if textField == txtFldTime {
            pickerType = PickerType.Time
            openPicker(pickertype: pickerType!)
            return false
        }
        return true
    }
    
    //MARK:-
    //MARK:- DatePickerDelegates
    func getDate(_ date: String, index: Int) {
        if pickerType == PickerType.Date{
            txtFldDate.text = AppTheme.sharedInstance.setMeetupDateForm(strDate: date)
            strServerDate = date
        }else if pickerType == PickerType.Time{
            txtFldTime.text = date
        }
    }
    
    @IBAction func btnCreate(sender : UIButton)
    {
        for textField in allTextField {
            if textField.text == "" {
                Manager.sharedInstance.showAlert(self, message: ManageLocalization.getLocalaizedString(key: "alert.allDetailsRequiredForPostingMeetups"))
                return
            }
        }
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"
        dicCreateMeetUp = ["title" : txtFldTitle.text!,
                          "meetup_place" : txtFldPlace.text!,
                           "meetup_time" : AppTheme.sharedInstance.get24HoursTimeFormat(strTime: txtFldTime.text!),
                          "meetup_date" : strServerDate,
                          "meetup_comment" : txtViewNotes.text!,
                           "lang": lang,
                          "post_cat_id" : "5"] as [String : AnyObject]
        if btnAnonymousChat.isSelected {
            dicCreateMeetUp["is_anonymous"] = "1" as AnyObject
        }else{
            dicCreateMeetUp["is_anonymous"] = "0" as AnyObject
        }
        let when = DispatchTime.now() + 0.0
        WebAPI.httpJSONRequest(viewController: self, url: APPURL.PostMeetUps, params: dicCreateMeetUp, SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    
                    let objTarget = self.storyboard?.instantiateViewController(withIdentifier: "PostCreatedVC") as! PostCreatedVC
                    objTarget.contrType = .Meetup
                    objTarget.createNewPost = {self.clearData()}
                    self.navigationController?.pushViewController(objTarget, animated: true)
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
            
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
}
