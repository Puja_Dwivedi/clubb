//
//  ClassNotesVC.swift
//  ClubBooks
//
//  Created by cis on 21/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit
import MBProgressHUD
class ClassNotesVC: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var allTextField : [UITextField]!
    @IBOutlet var collViewImages : UICollectionView!
    @IBOutlet var btnAnonymousChat : CheckBoxButton!
    @IBOutlet weak var txtFldPrice: UITextField!
    @IBOutlet weak var txtFldNumberOfPages: UITextField!
    @IBOutlet weak var txtFldCourse: UITextField!
    @IBOutlet weak var txtFldProfessor: UITextField!
    @IBOutlet weak var txtFldDepartment: UITextField!
    @IBOutlet weak var txtFldSchool: UITextField!
    @IBOutlet weak var txtFldTitle: UITextField!
    @IBOutlet weak var txtFldCity: UITextField!
    @IBOutlet var heightConstraintsCollView : NSLayoutConstraint!
    var imagePicker = UIImagePickerController()
    var arrQuizImages = [AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTitleWithLocalizedString(title: "btn.Title.postClassNotes")
        showLogoWithBackButon()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        let nibPost = UINib.init(nibName: "BookPostImageCell", bundle: nil)
        self.collViewImages.register(nibPost, forCellWithReuseIdentifier: "BookPostImageCell")
        self.clearData()
    }
    func clearData(){
        for textField in allTextField
        {
            textField.text = ""
            textField.addShadowAround(layer: textField.layer)
            textField.setLeftPaddingPoints(10.0)
        }
        arrQuizImages.removeAll()
        self.checkForImageCount()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnCreate(sender : UIButton)
    {
        for textField in allTextField {
            if textField.text == "" {
                Manager.sharedInstance.showAlert(self, message: ManageLocalization.getLocalaizedString(key: "alert.allDetailsRequiredForPostingNotes"))
                return
            }
        }
        if arrQuizImages.count == 0 {
            Manager.sharedInstance.showAlert(self, message: ManageLocalization.getLocalaizedString(key: "alert.selectNotesImage"))
            return
        }
        self.postNotes()
    }
    
    @objc func btnDeleteImage(sender : UIButton)
    {
        print(sender.tag)
        let alert : UIAlertController = UIAlertController(title: "ClubBooks", message: ManageLocalization.getLocalaizedString(key: "alert.deleteImage"), preferredStyle: .alert)
        
        let YesAction = UIAlertAction(title: "alert.yes".localized, style: .default, handler: { (action) in
            self.arrQuizImages.remove(at: sender.tag-1000)
            self.checkForImageCount()
        })
        let Cancel = UIAlertAction(title: "alert.no".localized, style: .destructive, handler: { (action) in
        })
        alert.addAction(YesAction)
        alert.addAction(Cancel)
        self.present(alert, animated: true, completion: {
        })
    }
    func checkForImageCount(){
        if arrQuizImages.count == 0 {
            self.heightConstraintsCollView.constant = 0
        }else{
            self.heightConstraintsCollView.constant = 112.0
        }
        self.view.layoutIfNeeded()
        self.collViewImages.reloadData()
    }

    
    //MARK:-- Collection view delegate methods --
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrQuizImages.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookPostImageCell", for: indexPath as IndexPath) as! BookPostImageCell
        if indexPath.row < arrQuizImages.count {
            cell.btnClose.tag = 1000 + indexPath.item
            cell.btnClose.isHidden = false
            cell.btnClose.addTarget(self, action: #selector(btnDeleteImage(sender:)), for: .touchUpInside)
            cell.imgView.image = arrQuizImages[indexPath.row] as? UIImage
            cell.btnAdd.isHidden = true
        }else{
            cell.btnAdd.isHidden = false
            cell.btnAdd.addTarget(self, action: #selector(btnImageClicked(_:)), for: .touchUpInside)
            cell.btnClose.isHidden = true
        }
        return cell
    }
    
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    
    @IBAction func btnImageClicked(_ sender : UIButton) {
        if arrQuizImages.count < 11{
            let actionSheet : UIAlertController = UIAlertController(title: "alert.chooseOption".localized, message: "", preferredStyle: .actionSheet)
            
            let saveActionButton = UIAlertAction(title: "alert.openCamera".localized, style: .default)
            { _ in
                
                if UIImagePickerController.isSourceTypeAvailable(.camera){
                    print("Button capture")
                    
                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = .camera;
                    self.imagePicker.allowsEditing = false
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
            }
            actionSheet.addAction(saveActionButton)
            
            let deleteActionButton = UIAlertAction(title: "alert.photoLibrary".localized, style: .default)
            { _ in
                
                if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                    print("Button capture")
                    
                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = .photoLibrary;
                    self.imagePicker.allowsEditing = false
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
                
            }
            actionSheet.addAction(deleteActionButton)
            
            let cancelActionButton = UIAlertAction(title: "alert.cancel".localized, style: .cancel) { _ in
                
            }
            actionSheet.addAction(cancelActionButton)
            
            self.present(actionSheet, animated: true, completion: nil)
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] {
            arrQuizImages.append(self.imageOrientation(image as! UIImage))
        }
        self.checkForImageCount()
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imageOrientation(_ src:UIImage)->UIImage {
        if src.imageOrientation == UIImage.Orientation.up {
            return src
        }
        var transform: CGAffineTransform = CGAffineTransform.identity
        switch src.imageOrientation {
        case UIImage.Orientation.down, UIImage.Orientation.downMirrored:
            transform = transform.translatedBy(x: src.size.width, y: src.size.height)
            transform = transform.rotated(by: CGFloat(M_PI))
            break
        case UIImage.Orientation.left, UIImage.Orientation.leftMirrored:
            transform = transform.translatedBy(x: src.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(M_PI_2))
            break
        case UIImage.Orientation.right, UIImage.Orientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: src.size.height)
            transform = transform.rotated(by: CGFloat(-M_PI_2))
            break
        case UIImage.Orientation.up, UIImage.Orientation.upMirrored:
            break
        }
        
        switch src.imageOrientation {
        case UIImage.Orientation.upMirrored, UIImage.Orientation.downMirrored:
            transform.translatedBy(x: src.size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImage.Orientation.leftMirrored, UIImage.Orientation.rightMirrored:
            transform.translatedBy(x: src.size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImage.Orientation.up, UIImage.Orientation.down, UIImage.Orientation.left, UIImage.Orientation.right:
            break
        }
        
        let ctx:CGContext = CGContext(data: nil, width: Int(src.size.width), height: Int(src.size.height), bitsPerComponent: (src.cgImage)!.bitsPerComponent, bytesPerRow: 0, space: (src.cgImage)!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch src.imageOrientation {
        case UIImage.Orientation.left, UIImage.Orientation.leftMirrored, UIImage.Orientation.right, UIImage.Orientation.rightMirrored:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.height, height: src.size.width))
            break
        default:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.width, height: src.size.height))
            break
        }
        
        let cgimg:CGImage = ctx.makeImage()!
        let img:UIImage = UIImage(cgImage: cgimg)
        
        return img
    }
    
    
    func postNotes(){
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"

        var dicpostQuiz = ["post_cat_id" : "2",
                           "title" : txtFldTitle.text!,
                           "city" : txtFldCity.text!,
                           "school" : txtFldSchool.text!,
                           "department" : txtFldDepartment.text!,
                           "professor" : txtFldProfessor.text!,
                           "course" : txtFldCourse.text!,
                           "no_of_pages" : txtFldNumberOfPages.text!,
                           "price" : txtFldPrice.text!,
                           "lang": lang,
                           "post_img[]" : arrQuizImages] as [String : AnyObject]
        if btnAnonymousChat.isSelected{
            dicpostQuiz["is_anonymous"] = "1" as AnyObject
        }else{
            dicpostQuiz["is_anonymous"] = "0" as AnyObject
        }
        let when = DispatchTime.now() + 0.0
        
        WebAPI.multipartHttpRequestWithImageArray(viewController: self, imageKey: "post_img[]", arrayImages: arrQuizImages as! [UIImage], url: APPURL.PostNotes, params: dicpostQuiz, SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    let objTarget = self.storyboard?.instantiateViewController(withIdentifier: "PostCreatedVC") as! PostCreatedVC
                    objTarget.contrType = .Notes
                    objTarget.createNewPost = {self.clearData()}
                    self.navigationController?.pushViewController(objTarget, animated: true)
                }
                
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
                
            }
            
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
}
