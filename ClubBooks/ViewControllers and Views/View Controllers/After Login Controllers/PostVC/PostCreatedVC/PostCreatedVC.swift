//
//  PostCreatedVC.swift
//  ClubBooks
//
//  Created by cis on 19/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

enum controllerType : Int
{
    case Book = 1
    case Meetup
    case BookChat
    case Exam
    case Quiz
    case Notes
    case TopicChat
}

class PostCreatedVC: BaseViewController {

   
    var contrType: controllerType = controllerType.init(rawValue: 1)!
    var createNewPost:(()->Void)! = nil

    @IBOutlet var lblPostTitle : UILabel!
    @IBOutlet var btnPostMore : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showlogoLeft()
        showSettingButton()
        
        switch contrType {
        case .Book:
            lblPostTitle.text = ManageLocalization.getLocalaizedString(key: "lbl.title.postedBook")
            btnPostMore.setTitle(ManageLocalization.getLocalaizedString(key: "btn.Title.postAnotherBook"), for: .normal)
        case .Meetup:
            lblPostTitle.text = ManageLocalization.getLocalaizedString(key: "lbl.title.createdMeetup")
            btnPostMore.setTitle(ManageLocalization.getLocalaizedString(key: "btn.Title.creatAnotherMeetup"), for: .normal)
        case .BookChat:
            lblPostTitle.text = ManageLocalization.getLocalaizedString(key: "lbl.title.createdBookChat")
            btnPostMore.setTitle(ManageLocalization.getLocalaizedString(key: "btn.Title.creatAnotherBookChat"), for: .normal)
        case .TopicChat:
            lblPostTitle.text = ManageLocalization.getLocalaizedString(key: "lbl.title.createdTopicChat")
            btnPostMore.setTitle(ManageLocalization.getLocalaizedString(key: "btn.Title.creatAnotherTopicChat"), for: .normal)
        case .Exam:
            lblPostTitle.text = ManageLocalization.getLocalaizedString(key: "lbl.title.postedExam")
            btnPostMore.setTitle(ManageLocalization.getLocalaizedString(key: "btn.Title.postMoreExam"), for: .normal)
        case .Quiz:
            lblPostTitle.text = ManageLocalization.getLocalaizedString(key: "lbl.title.postedQuiz")
            btnPostMore.setTitle(ManageLocalization.getLocalaizedString(key: "btn.Title.postMoreQuiz"), for: .normal)
        case .Notes:
            lblPostTitle.text = ManageLocalization.getLocalaizedString(key: "lbl.title.postedNotes")
            btnPostMore.setTitle(ManageLocalization.getLocalaizedString(key: "btn.Title.postMoreNotes"), for: .normal)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnCreateAnother(sender : UIButton)
    {
        if contrType == .Book{
            if let viewControllers = self.navigationController?.viewControllers
            {
                for controller in viewControllers
                {
                    if controller is BookInitialVC
                    {
                        let bookinitial = controller as! BookInitialVC
                        bookinitial.clearData()
                        self.navigationController?.popToViewController(controller, animated: true)
                    }
                }
            }
        }else{
            self.createNewPost()
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    @IBAction func btnDone(sender : UIButton)
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
}
