//
//  ChatOnBookVC.swift
//  ClubBooks
//
//  Created by cis on 21/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit
import MBProgressHUD
class ChatOnBookVC: BaseViewController {
    @IBOutlet var allTextField : [UITextField]!
    @IBOutlet var txtFldTitle : UITextField!
    @IBOutlet var txtFldAuthor : UITextField!
    @IBOutlet var btnCreateChat : UIButton!
    @IBOutlet var btnAnonymousChat : CheckBoxButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.setTitleWithLocalizedString(title: "nav.ChatOnBook")
        self.title = "\("nav.Post".localized) \("nav.ChatOnBook".localized)"
        showLogoWithBackButon()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        for textField in allTextField
        {
            textField.addShadowAround(layer: textField.layer)
            textField.setLeftPaddingPoints(10.0)
        }
        btnCreateChat.setTitle("btn.title.postBookChat".localized, for: .normal)
        clearData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func clearData(){
        self.txtFldTitle.text = ""
        self.txtFldAuthor.text = ""
        txtFldTitle.becomeFirstResponder()
    }
    @IBAction func btnCreate(sender : UIButton){
        for textField in allTextField {
            if textField.text == "" {
                Manager.sharedInstance.showAlert(self, message: ManageLocalization.getLocalaizedString(key: "alert.allDetailsRequiredForPostingChatsOnBooks"))
                return
            }
        }
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"
        var dicCreateMeetUp = ["post_cat_id" : "6",
                               "title" : txtFldTitle.text!,
                               "lang": lang,
                               "meetup_place" : txtFldAuthor.text!] as [String : AnyObject]
        if btnAnonymousChat.isSelected {
            dicCreateMeetUp["is_anonymous"] = "1" as AnyObject
        }else{
            dicCreateMeetUp["is_anonymous"] = "0" as AnyObject
        }
        
        
        let when = DispatchTime.now() + 0.0
        WebAPI.httpJSONRequest(viewController: self, url: APPURL.PostchatonBooks, params: dicCreateMeetUp, SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    let objTarget = self.storyboard?.instantiateViewController(withIdentifier: "PostCreatedVC") as! PostCreatedVC
                    objTarget.createNewPost = {self.clearData()}
                    objTarget.contrType = .BookChat
                    self.navigationController?.pushViewController(objTarget, animated: true)
                }
            }else{
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
        }){ (error) in
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
}
