//
//  ChatOnTopicVC.swift
//  ClubBooks
//
//  Created by cis on 21/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit
import MBProgressHUD
class ChatOnTopicVC: BaseViewController {
    @IBOutlet var viewNotes : UIView!
    @IBOutlet var txtViewNotes : UITextView!
    @IBOutlet var btnAnonymousChat : CheckBoxButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "\("nav.Post".localized) \("nav.ChatsOnTopics".localized)"
        showLogoWithBackButon()
        viewNotes.addShadowAround(layer: viewNotes.layer)

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        self.clearData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func clearData(){
        self.txtViewNotes.text = ""
    }

    @IBAction func btnCreate(sender : UIButton)
    {
        if txtViewNotes.text == "" {
            Manager.sharedInstance.showAlert(self, message: ManageLocalization.getLocalaizedString(key: "alert.allDetailsRequiredForPostingChatsOnTopic"))
            return
        }
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"

        var dicCreateMeetUp = ["post_cat_id" : "7",
                               "lang": lang,
                               "title" : txtViewNotes.text!] as [String : AnyObject]
        
        if btnAnonymousChat.isSelected {
            dicCreateMeetUp["is_anonymous"] = "1" as AnyObject
        }else{
            dicCreateMeetUp["is_anonymous"] = "0" as AnyObject
        }
        
        let when = DispatchTime.now() + 0.0
        WebAPI.httpJSONRequest(viewController: self, url: APPURL.PostchatonTopic, params: dicCreateMeetUp, SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    
                    let objTarget = self.storyboard?.instantiateViewController(withIdentifier: "PostCreatedVC") as! PostCreatedVC
                    objTarget.createNewPost = {self.clearData()}
                    objTarget.contrType = .TopicChat
                    self.navigationController?.pushViewController(objTarget, animated: true)
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
            
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
}
