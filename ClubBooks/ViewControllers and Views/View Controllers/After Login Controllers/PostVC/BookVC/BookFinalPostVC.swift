//
//  BookFinalPostVC.swift
//  ClubBooks
//
//  Created by cis on 22/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit
import MBProgressHUD
class BookFinalPostVC: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, GetPickerValueDelagate {
    @IBOutlet var allTextField : [UITextField]!
    @IBOutlet var txtFldCity : UITextField!
    @IBOutlet var txtFldSchool : UITextField!
    @IBOutlet var txtFldDepartment : UITextField!
    @IBOutlet var txtFldProfessor : UITextField!
    @IBOutlet var txtFldCourse : UITextField!
    @IBOutlet var txtFldCondition : UITextField!
    var intSelectedConditionIndex : Int = 0
    @IBOutlet var txtFldISBN : UITextField!
    @IBOutlet var txtFldPhone : UITextField!
    @IBOutlet var txtExchangeMeetup : UITextView!
    @IBOutlet var collViewBookImages : UICollectionView!
    @IBOutlet var viewTxtViewBackground : UIView!
    var arrBookImages = [AnyObject]()
    var dicFinalBookDetails = [String : AnyObject]()
    var imagePicker = UIImagePickerController()
    @IBOutlet var heightConstraintsCollView : NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTitleWithLocalizedString(title: "btn.Title.postBook")
        showLogoWithBackButon()
        
        viewTxtViewBackground.addShadowAround(layer: viewTxtViewBackground.layer)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        let nibBookImage = UINib.init(nibName: "BookPostImageCell", bundle: nil)
        self.collViewBookImages.register(nibBookImage, forCellWithReuseIdentifier: "BookPostImageCell")
        for textField in allTextField
        {
            textField.text = ""
            textField.addShadowAround(layer: textField.layer)
            textField.setLeftPaddingPoints(10.0)
            textField.text = ""
        }
        arrBookImages.removeAll()
        checkForImageCount()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnCreate(sender : UIButton)
    {
        for textField in allTextField {
            if textField.text == "" {
                Manager.sharedInstance.showAlert(self, message: ManageLocalization.getLocalaizedString(key: "alert.allDetailsRequiredForPostingBook"))
                return
            }
        }
        if arrBookImages.count == 0 {
            Manager.sharedInstance.showAlert(self, message: ManageLocalization.getLocalaizedString(key: "alert.selectBookImage"))
            return
        }
        self.postBook()
       
    }

    @objc func btnDeleteImage(sender : UIButton)
    {
        print(sender.tag)
        let alert : UIAlertController = UIAlertController(title: "ClubBooks", message: ManageLocalization.getLocalaizedString(key: "alert.deleteImage"), preferredStyle: .alert)
        
        let YesAction = UIAlertAction(title: "alert.yes".localized, style: .default, handler: { (action) in
            self.arrBookImages.remove(at: sender.tag-1000)
            self.checkForImageCount()
        })
        let Cancel = UIAlertAction(title: "alert.no".localized, style: .destructive, handler: { (action) in
        })
        alert.addAction(YesAction)
        alert.addAction(Cancel)
        self.present(alert, animated: true, completion: {
        })
    }
    
    func checkForImageCount(){
        if arrBookImages.count == 0 {
            self.heightConstraintsCollView.constant = 0
        }else{
            self.heightConstraintsCollView.constant = 112.0
        }
        self.view.layoutIfNeeded()
        self.collViewBookImages.reloadData()
    }
    
    //MARK:-- Collection view delegate methods --
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if arrBookImages.count < 10 {
            return arrBookImages.count
        }else{
            return arrBookImages.count
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookPostImageCell", for: indexPath as IndexPath) as! BookPostImageCell
        if indexPath.row < arrBookImages.count{
            cell.btnClose.tag = 1000 + indexPath.item
            cell.btnClose.isHidden = false
            cell.btnClose.addTarget(self, action: #selector(btnDeleteImage(sender:)), for: .touchUpInside)
            cell.imgView.image = arrBookImages[indexPath.row] as? UIImage
            cell.btnAdd.isHidden = true
        }
//        }else{
//            cell.btnAdd.isHidden = false
//            cell.btnClose.isHidden = true
//            cell.btnAdd.addTarget(self, action: #selector(btnImageClicked(_:)), for: .touchUpInside)
//        }
        cell.backgroundColor = .clear
        return cell
    }
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
    }
    // MARK: - UIimagePicker Delegates
    @IBAction func btnImageClicked(_ sender : UIButton) {
        if arrBookImages.count < 11 {
            let actionSheet : UIAlertController = UIAlertController(title: "alert.chooseOption".localized, message: "", preferredStyle: .actionSheet)
            
            let saveActionButton = UIAlertAction(title: "alert.openCamera".localized, style: .default)
            { _ in
                
                if UIImagePickerController.isSourceTypeAvailable(.camera){
                    print("Button capture")
                    
                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = .camera
                    self.imagePicker.allowsEditing = false
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
            }
            actionSheet.addAction(saveActionButton)
            
            let deleteActionButton = UIAlertAction(title: "alert.photoLibrary".localized, style: .default)
            { _ in
                
                if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                    print("Button capture")
                    
                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = .photoLibrary;
                    self.imagePicker.allowsEditing = false
                    
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
                
            }
            actionSheet.addAction(deleteActionButton)
            
            let cancelActionButton = UIAlertAction(title: "alert.cancel".localized, style: .cancel) { _ in
                
            }
            actionSheet.addAction(cancelActionButton)
            
            self.present(actionSheet, animated: true, completion: nil)
        }
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] {
            arrBookImages.append(self.imageOrientation(image as! UIImage))
        }
        checkForImageCount()
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imageOrientation(_ src:UIImage)->UIImage {
        if src.imageOrientation == UIImage.Orientation.up {
            guard let image = src.jpeg(.medium) else {
                return src
            }
            return image
        }
        var transform: CGAffineTransform = CGAffineTransform.identity
        switch src.imageOrientation {
        case UIImage.Orientation.down, UIImage.Orientation.downMirrored:
            transform = transform.translatedBy(x: src.size.width, y: src.size.height)
            transform = transform.rotated(by: CGFloat(M_PI))
            break
        case UIImage.Orientation.left, UIImage.Orientation.leftMirrored:
            transform = transform.translatedBy(x: src.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(M_PI_2))
            break
        case UIImage.Orientation.right, UIImage.Orientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: src.size.height)
            transform = transform.rotated(by: CGFloat(-M_PI_2))
            break
        case UIImage.Orientation.up, UIImage.Orientation.upMirrored:
            break
        }
        
        switch src.imageOrientation {
        case UIImage.Orientation.upMirrored, UIImage.Orientation.downMirrored:
            transform.translatedBy(x: src.size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImage.Orientation.leftMirrored, UIImage.Orientation.rightMirrored:
            transform.translatedBy(x: src.size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImage.Orientation.up, UIImage.Orientation.down, UIImage.Orientation.left, UIImage.Orientation.right:
            break
        }
        
        let ctx:CGContext = CGContext(data: nil, width: Int(src.size.width), height: Int(src.size.height), bitsPerComponent: (src.cgImage)!.bitsPerComponent, bytesPerRow: 0, space: (src.cgImage)!.colorSpace!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch src.imageOrientation {
        case UIImage.Orientation.left, UIImage.Orientation.leftMirrored, UIImage.Orientation.right, UIImage.Orientation.rightMirrored:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.height, height: src.size.width))
            break
        default:
            ctx.draw(src.cgImage!, in: CGRect(x: 0, y: 0, width: src.size.width, height: src.size.height))
            break
        }
        
        let cgimg:CGImage = ctx.makeImage()!
        let img:UIImage = UIImage(cgImage: cgimg)
        
        guard let image = img.jpeg(.medium) else {
            return img
        }
        return image
    }
    // MARK: - UITextField Delegates
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtFldCondition{
            self.openPicker()
            return false
        }else{
            return true
        }
    }
    func openPicker(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Picker") as! Picker
        vc.delegate = self
        vc.selectedValue = ""
        vc.data = ["lbl.new".localized, "lbl.good".localized, "lbl.fair".localized, "lbl.poor".localized]
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overFullScreen
        self.present(vc, animated: false, completion: nil)
    }
    func getValue(_ value: String, index: Int, iflag: Bool) {
        txtFldCondition.text = value
        intSelectedConditionIndex = index + 1
    }
    // MARK: - WebService Methods
    func postBook(){
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"

        dicFinalBookDetails["post_cat_id"] = "1" as AnyObject
        dicFinalBookDetails["city"] = txtFldCity.text as AnyObject
        dicFinalBookDetails["school"] = txtFldSchool.text as AnyObject
        dicFinalBookDetails["department"] = txtFldDepartment.text as AnyObject
        dicFinalBookDetails["professor"] = txtFldProfessor.text as AnyObject
        dicFinalBookDetails["course"] = txtFldCourse.text as AnyObject
        dicFinalBookDetails["post_condition"] = "\(intSelectedConditionIndex)" as AnyObject
        dicFinalBookDetails["isbn"] = txtFldISBN.text as AnyObject
//        dicFinalBookDetails["phone"] = txtFldPhone.text as AnyObject
        dicFinalBookDetails["meetup_place"] = txtExchangeMeetup.text as AnyObject
        dicFinalBookDetails["post_img[]"] = arrBookImages as AnyObject
        dicFinalBookDetails["lang"] = lang as AnyObject
       
        let when = DispatchTime.now() + 0.0
        WebAPI.multipartHttpRequestWithImageArray(viewController: self, imageKey: "post_img[]", arrayImages: arrBookImages as! [UIImage], url: APPURL.PostBook, params: dicFinalBookDetails, SuccessHandler: { (responseData) in
            print(responseData.dictionary)
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    let objTarget = self.storyboard?.instantiateViewController(withIdentifier: "PostCreatedVC") as! PostCreatedVC
                    objTarget.contrType = .Book
                    self.navigationController?.pushViewController(objTarget, animated: true)
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
                
            }
           
        }) { (error) in
            
           Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
}
