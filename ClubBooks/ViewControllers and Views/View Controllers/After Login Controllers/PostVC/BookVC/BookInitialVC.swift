//
//  BookInitialVC.swift
//  ClubBooks
//
//  Created by cis on 21/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class BookInitialVC: BaseViewController {
    @IBOutlet var allTextField : [UITextField]!
    @IBOutlet var txtFldTitle : UITextField!
    @IBOutlet var txtFldAuthor : UITextField!
    @IBOutlet var txtFldPrice : UITextField!
    @IBOutlet var txtFldEdition : UITextField!
    @IBOutlet var btnAnonymousBook : CheckBoxButton!
    var dicBookDetails = [String : AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clearData()
        showLogoWithBackButon()
        self.setTitleWithLocalizedString(title: "btn.Title.postBook")
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    func clearData(){
        for textField in allTextField
        {
            textField.addShadowAround(layer: textField.layer)
            textField.setLeftPaddingPoints(10.0)
            textField.text = ""
        }
        self.btnAnonymousBook.isSelected = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnNextStep(sender : UIButton)
    {
        for textField in allTextField {
            if textField.text == "" {
                Manager.sharedInstance.showAlert(self, message: ManageLocalization.getLocalaizedString(key: "alert.allDetailsRequiredForPostingBook"))
                return
            }
        }
        dicBookDetails = ["title" : txtFldTitle.text,
                          "author" : txtFldAuthor.text,
                          "price" : txtFldPrice.text,
                          "edition" : txtFldEdition.text] as! [String : AnyObject]
        if btnAnonymousBook.isSelected{
            dicBookDetails["is_anonymous"] = "1" as AnyObject
        }else{
            dicBookDetails["is_anonymous"] = "0" as AnyObject
        }
        let objTarget = storyboard?.instantiateViewController(withIdentifier: "BookFinalPostVC") as! BookFinalPostVC
        objTarget.dicFinalBookDetails = dicBookDetails
        self.navigationController?.pushViewController(objTarget, animated: true)
    }    
}
