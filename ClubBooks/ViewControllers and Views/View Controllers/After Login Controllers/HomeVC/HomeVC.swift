    //
//  HomeVC.swift
//  ClubBooks
//
//  Created by cis on 24/12/18.
//  Copyright © 2018 CIS. All rights reserved.
//

import UIKit
import MBProgressHUD
import UserNotifications
enum ListType : Int
{
    case All = 0
    case Book
    case Notes
    case Exam
    case Quiz
    case Meetup
    case ChatonBooks
    case ChatsonTopics
}
class HomeVC: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {
    
    var listType : ListType = ListType.init(rawValue: 0)!
    @IBOutlet var tblHeaderView : UIView!
    @IBOutlet var tblHome : UITableView!
    let arrHeaderText = ["lbl.books", "lbl.classnotes", "lbl.exams","lbl.quizes", "lbl.meetups", "lbl.title.bookChatLabel", "lbl.title.topicLabel",]
    var arrayPost = [PostDetails]()
    var intPagenumber : Int = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = nil
        listType = .All
        self.tabBarController?.tabBar.addShadowAround(layer: (self.tabBarController?.tabBar.layer)!)
        
        let nibBook = UINib.init(nibName: "HomeBookCell", bundle: nil)
        self.tblHome.register(nibBook, forCellReuseIdentifier: "HomeBookCell")
        let nibMeetUps = UINib.init(nibName: "HomeMeetUpsCell", bundle: nil)
        self.tblHome.register(nibMeetUps, forCellReuseIdentifier: "HomeMeetUpsCell")
        let nibChatsOnBooks = UINib.init(nibName: "ChatOnBooksCell", bundle: nil)
        self.tblHome.register(nibChatsOnBooks, forCellReuseIdentifier: "ChatOnBooksCell")
        let nibChatsOnTopic = UINib.init(nibName: "ChatsOnTopicCell", bundle: nil)
        self.tblHome.register(nibChatsOnTopic, forCellReuseIdentifier: "ChatsOnTopicCell")
        let nibExams = UINib.init(nibName: "HomeExamCell", bundle: nil)
        self.tblHome.register(nibExams, forCellReuseIdentifier: "HomeExamCell")
        let nibQuiz = UINib.init(nibName: "HomeQuizCell", bundle: nil)
        self.tblHome.register(nibQuiz, forCellReuseIdentifier: "HomeQuizCell")
        let nibClassNotes = UINib.init(nibName: "HomeClassNotesTableViewCell", bundle: nil)
        self.tblHome.register(nibClassNotes, forCellReuseIdentifier: "HomeClassNotesTableViewCell")
        self.getPostForCategory(URLType: APPURL.getAllPost)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = false
        self.showlogoLeft()
        self.showSettingButton()
        self.navigationItem.title =  "nav.Home".localized.uppercased()
        self.tblHome.reloadData()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:
    //MARK: UICollectionView Datasource and Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let homeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeHeaderCell", for: indexPath) as! HomeHeaderCell
        if indexPath.row == 0 {
            homeCell.lblHeading.text = ""
            homeCell.imgView.image = nil
            homeCell.lblAllHeight.constant = 14
            homeCell.imgView.backgroundColor = UIColor.init(red: 65/255.0, green: 196/255.0, blue: 221/255.0, alpha: 1.0)
            
        }else{
            homeCell.lblHeading.text = ManageLocalization.getLocalaizedString(key: arrHeaderText[indexPath.row-1])
            switch indexPath.row
            {
            case 1:
                homeCell.imgView.image = UIImage(named: "books")
            case 2:
                homeCell.imgView.image = UIImage(named: "class_notes")
            case 3:
                homeCell.imgView.image = UIImage(named: "exams")
            case 4:
                homeCell.imgView.image = UIImage(named: "quizzes")
            case 5:
                homeCell.imgView.image = UIImage(named: "meetups")
            case 6:
                homeCell.imgView.image = UIImage(named: "chatsbook")
            case 7:
                homeCell.imgView.image = UIImage(named: "chatstopic")
            default:
                break
            }
            homeCell.lblAllHeight.constant = 0
            homeCell.imgView.backgroundColor = UIColor.clear

            
        }
        if listType == ListType.init(rawValue: indexPath.row){
//            homeCell.backgroundColor = UIColor(red: 217.0/255.0, green: 243.0/255.0, blue: 248.0/255.0, alpha: 1.0)
            homeCell.backgroundColor = .clear
            homeCell.lblHeading.textColor = UIColor.init(red: 65/255.0, green: 196/255.0, blue: 221/255.0, alpha: 1.0)
        }else{
            homeCell.backgroundColor = UIColor.clear
            homeCell.lblHeading.textColor = UIColor.black
        }
        return homeCell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        listType = ListType.init(rawValue: indexPath.row)!
        arrayPost.removeAll()
        tblHome.reloadData()
        intPagenumber = 1
        collectionView.reloadData()
        self.getFeedType()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 80, height: 88)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //MARK:
    //MARK: UITableView Datasource and Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayPost.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let post = arrayPost[indexPath.row]
       
        if indexPath.row == arrayPost.count - 1{
            intPagenumber = intPagenumber+1
            getFeedType()
        }
        if post.postCat == "1" && (listType == .Book || listType == .All) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeBookCell", for: indexPath) as! HomeBookCell
            cell.ConfigureBookCell(bookDetails: post)
            cell.selectionStyle = .none
            return cell
        }else if post.postCat == "2" && (listType == .Notes || listType == .All){
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeClassNotesTableViewCell") as! HomeClassNotesTableViewCell
            cell.ConfigureNotesCell(notesDetails: post)
            cell.selectionStyle = .none
            return cell
        }else if post.postCat == "3" && (listType == .Exam || listType == .All) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeExamCell", for: indexPath) as! HomeExamCell
            cell.selectionStyle = .none
            cell.ConfigureBookCell(examDetails: post)
            return cell
        }else if post.postCat == "4" && (listType == .Quiz || listType == .All) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeQuizCell", for: indexPath) as! HomeQuizCell
            cell.ConfigureQuizCell(quizDetails: post)
            cell.selectionStyle = .none
            return cell
        }else if post.postCat == "5" && (listType == .Meetup || listType == .All) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeMeetUpsCell", for: indexPath) as! HomeMeetUpsCell
            cell.delegate = self
            cell.ConfigureMeetUpsCell(meetUpsDetails: post)
            cell.selectionStyle = .none
            return cell
        }else if post.postCat == "6" && (listType == .ChatonBooks || listType == .All) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatOnBooksCell", for: indexPath) as! ChatOnBooksCell
            cell.ConfigureBookChatCell(chatDetails: post)
            cell.selectionStyle = .none
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatsOnTopicCell", for: indexPath) as! ChatsOnTopicCell
            cell.ConfigureTopicChatCell(chatDetails: post)
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return tblHeaderView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if ManageLocalization.deviceLang == "es"{
            return 140
        }
        return 110
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let post = arrayPost[indexPath.row]
        print(post.postTitle)
        if post.postCat == "1" && (listType == .Book || listType == .All) {
            //Book Detail Screen
            let objTarget = storyboard?.instantiateViewController(withIdentifier: "BookDetailVC") as! BookDetailVC
            objTarget.strPostID = post.postID
            objTarget.hidesBottomBarWhenPushed = true
            objTarget.reserveBookWithPostID = { bookid, isReserved in
                let foundItems = self.arrayPost.filter {$0.postID == bookid}
                foundItems[0].isReserved = isReserved
                self.tblHome.reloadData()
            }
            self.navigationController?.pushViewController(objTarget, animated: true)
        }else if post.postCat == "2" && (listType == .Notes || listType == .All){
            //Notes Detail Screen
            let objTarget = storyboard?.instantiateViewController(withIdentifier: "PostDetailsVC") as! PostDetailsVC
            objTarget.strPostID = post.postID
            objTarget.postType = .PostClassNotes
            objTarget.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objTarget, animated: true)
        }else if post.postCat == "3" && (listType == .Exam || listType == .All) {
            //Exam Detail Screen
            let objTarget = storyboard?.instantiateViewController(withIdentifier: "PostDetailsVC") as! PostDetailsVC
            objTarget.strPostID = post.postID
            objTarget.postType = .PostExam
            objTarget.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objTarget, animated: true)
        }else if post.postCat == "4" && (listType == .Quiz || listType == .All) {
            //Quiz Detail Screen
            let objTarget = storyboard?.instantiateViewController(withIdentifier: "PostDetailsVC") as! PostDetailsVC
            objTarget.strPostID = post.postID
            objTarget.postType = .PostQuiz
            objTarget.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objTarget, animated: true)
        }else if post.postCat == "5" && (listType == .Meetup || listType == .All) {
            //MeetUps Detail Screen
            let objTarget = storyboard?.instantiateViewController(withIdentifier: "MeetupDetailVC") as! MeetupDetailVC
            objTarget.strPostID = post.postID
            objTarget.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objTarget, animated: true)
            
        }else if post.postCat == "6" && (listType == .ChatonBooks || listType == .All) {
            let objTarget = storyboard?.instantiateViewController(withIdentifier: "BooksChatVC") as! BooksChatVC
            objTarget.strPostID = post.postID
            objTarget.strPostCreator = post.postCreator
            objTarget.strPostCreatorId = post.userid
            objTarget.strPostTitle = post.postTitle
            objTarget.isChatOnbooks = true
            if post.postIsAnnonymous == "true" || post.postIsAnnonymous == "1"{
                objTarget.isAnonymous = true
            }else{
                objTarget.isAnonymous = false
            }
            objTarget.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objTarget, animated: true)
            
        }else {
            //ChatsOnTopic Detail Screen
            let objTarget = storyboard?.instantiateViewController(withIdentifier: "BooksChatVC") as! BooksChatVC
            objTarget.strPostID = post.postID
            objTarget.strPostCreator = post.postCreator
            objTarget.strPostCreatorId = post.userid
            objTarget.strPostTitle = post.postTitle
            objTarget.isChatOnbooks = false
            if post.postIsAnnonymous == "true" || post.postIsAnnonymous == "1"{
                objTarget.isAnonymous = true
            }else{
                objTarget.isAnonymous = false
            }
            objTarget.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(objTarget, animated: true)
        }
    }
    var lastContentOffset: CGFloat = 0
    // this delegate is called when the scrollView (i.e your UITableView) will start scrolling
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        self.lastContentOffset = scrollView.contentOffset.y
    }
    // while scrolling this delegate is being called so you may now check which direction your scrollView is being scrolled to
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if scrollView is UITableView {
//        if (self.lastContentOffset < scrollView.contentOffset.y) {
//            UIView.animate(withDuration: 0.60, animations: {
//                self.tblHome.contentInset.top = -1 * self.tblHeaderView.frame.size.height
//            })
//        } else if (self.lastContentOffset > scrollView.contentOffset.y) {
//            UIView.animate(withDuration: 0.60, animations: {
//                self.tblHome.contentInset.top = 0
//            })
//        } else {
//            UIView.animate(withDuration: 0.60, animations: {
//                self.tblHome.contentInset.top = -1 * self.tblHeaderView.frame.size.height
//            })
//        }
//        }
    }
    //MARK:
    //MARK: GetAllPost API
    func getFeedType(){
        if listType == .All{
            getPostForCategory(URLType: APPURL.getAllPost)
        }else if listType == .Book{
           getPostForCategory(URLType: APPURL.getAllBooks)
        }else if listType == .ChatonBooks{
            getPostForCategory(URLType: APPURL.getChatsOnBook)
        }else if listType == .ChatsonTopics{
            getPostForCategory(URLType: APPURL.getChatsOnTopic)
        }else if listType == .Exam{
            getPostForCategory(URLType: APPURL.getAllExams)
        }else if listType == .Meetup{
            getPostForCategory(URLType: APPURL.getAllMeetups)
        }else if listType == .Notes{
            getPostForCategory(URLType: APPURL.getAllNotes)
        }else if listType == .Quiz{
            getPostForCategory(URLType: APPURL.getAllQuizz)
        }
    }
    func getPostForCategory(URLType : String){
        let when = DispatchTime.now() + 0.0
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"
        WebAPI.httpJSONRequest(viewController: self, url: URLType, params: ["page": intPagenumber as AnyObject, "lang": lang as AnyObject], SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                     
                    if responseData["responseData"].arrayValue.count > 0{
                    for json in responseData["responseData"].arrayValue {
                        let postFeed = PostDetails()
                        postFeed.parser(json)
                        self.arrayPost.append(postFeed)
                        }
                        self.tblHome.reloadData()
                        let numberofappLogins = RV_CheckDataType.getInteger(anyString: AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: "numberOfLogins") as AnyObject)
                        if numberofappLogins > 3 && AppTheme.sharedInstance.currentUserData[0].appRaing == "" && AppTheme.sharedInstance.isPresentRatngViewForThisSession == false{
                        let feedbackVC = self.storyboard?.instantiateViewController(withIdentifier: "FeedbackVC") as! FeedbackVC
                        feedbackVC.modalTransitionStyle = .crossDissolve
                        feedbackVC.modalPresentationStyle = .overFullScreen
                        feedbackVC.isNavigationbarHidden = true
                        self.present(feedbackVC, animated: true, completion: nil)
                        AppTheme.sharedInstance.isPresentRatngViewForThisSession = true
                        }
                    }
                    
                    if self.intPagenumber == 1 && self.arrayPost.count > 0 {
                    DispatchQueue.main.async {
                            let indexPath = NSIndexPath.init(row: 0, section: 0)
                            self.tblHome.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
                        }
                    }
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }

    }
}

extension HomeVC: HomeMeetUpsCellDelegate {
    func openUserProfile(userID: String, username: String) {
        let friendProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "FriendProfileVC") as! FriendProfileVC
        friendProfileVC.strFriendID = userID
        friendProfileVC.strFriendName = username
        navigationController?.pushViewController(friendProfileVC, animated: true)
    }
}
