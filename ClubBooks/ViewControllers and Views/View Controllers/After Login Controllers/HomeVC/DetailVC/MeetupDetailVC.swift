//
//  MeetupDetailVC.swift
//  ClubBooks
//
//  Created by cis on 16/11/22.
//  Copyright © 2022 CIS. All rights reserved.
//

import UIKit
import SwiftyJSON
class MeetupDetailVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var tblMeetups : UITableView!
    var strPostID : String!
    var selectedPost = MeetupDetails()

    override func viewDidLoad() {
        super.viewDidLoad()
        showSettingButton()
        showLogoWithBackButon()
        self.setTitleWithLocalizedString(title: "nav.MeetUpDetails")
        
        let nibBook = UINib.init(nibName: "MeetupDetailsCell", bundle: nil)
        self.tblMeetups.register(nibBook, forCellReuseIdentifier: "MeetupDetailsCell")
        let nibSocialSharing = UINib.init(nibName: "SocialSharingCell", bundle: nil)
        self.tblMeetups.register(nibSocialSharing, forCellReuseIdentifier: "SocialSharingCell")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getSelectedPostDetails()
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.postURL = ""
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnProfileClicked( _ sender : UIButton){
       
            //Post user id is not same as the logged in user.
            //Navigate to friend's profile screen
        var isViewExist = false
        for controller in self.navigationController!.viewControllers as Array {
            if controller is FriendProfileVC{
                isViewExist = true
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
            else
            {
                isViewExist = false
                
            }
            //if controller.isKind(of: DashboardVC.self)
        }
        if !isViewExist
        {
            let friendProfileVC : FriendProfileVC = storyBoard.instantiateViewController(withIdentifier: "FriendProfileVC") as! FriendProfileVC
            friendProfileVC.strFriendID = self.selectedPost.userID
            friendProfileVC.strFriendName = self.selectedPost.meetupCreator
            self.navigationController?.pushViewController(friendProfileVC, animated: true)
        }
    }
    // MARK: - tableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if selectedPost.userID == ""{
            return 0
        }else{
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MeetupDetailsCell", for: indexPath) as! MeetupDetailsCell
            cell.delegate = self
            cell.configureCell(PostDetails: selectedPost)
            cell.selectionStyle = .none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SocialSharingCell", for: indexPath) as! SocialSharingCell
            self.postURL = selectedPost.PostUrl
            cell.btnFaceBook.addTarget(self, action: #selector(btnFacebookShareClicked(_:)), for: .touchUpInside)
            cell.btnMessanger.addTarget(self, action: #selector(btnMessangerShareClicked(_:)), for: .touchUpInside)
            cell.btnEmail.addTarget(self, action: #selector(btnEmailClicked(_:)), for: .touchUpInside)
            cell.btnTwitter.addTarget(self, action: #selector(btnTwitterClicked(_:)), for: .touchUpInside)
            cell.btnInstaGram.addTarget(self, action: #selector(btnTwitterClicked(_:)), for: .touchUpInside)
            cell.btnTikTok.addTarget(self, action: #selector(btnTiktokClicked(_:)), for: .touchUpInside)
            cell.btnSnapChat.addTarget(self, action: #selector(btnSnapChatClicked(_:)), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1{
            return 90.0
        }else{
            return tblMeetups.estimatedRowHeight
        }
    }
    // MARK: -
    // MARK: - Web Service Call
    func getSelectedPostDetails(){
        let when = DispatchTime.now() + 0.0
        
        
          let strUrlString = APPURL.getMeetupDetails
        
        WebAPI.httpJSONRequest(viewController: self, url: strUrlString, params: ["post_id": strPostID as AnyObject], SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    
                    print(responseData.arrayValue)
                    let json = responseData["responseData"].arrayValue
                    if json.count > 0{
                        let jsonDic = json[0] as! JSON
                        self.selectedPost.parser(jsonDic)
                    }
                    self.tblMeetups.reloadData()
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
                
            }
            
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
        
    }

}

extension MeetupDetailVC: MeetupDetailsCellDelegate {
    func openUserProfile(userID: String, username: String) {
        let friendProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "FriendProfileVC") as! FriendProfileVC
        friendProfileVC.strFriendID = userID
        friendProfileVC.strFriendName = username
        navigationController?.pushViewController(friendProfileVC, animated: true)
    }
}
