//
//  BookDetailVC.swift
//  ClubBooks
//
//  Created by cis on 28/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit
import MBProgressHUD
class BookDetailVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tblBookDetail : UITableView!
    var strPostID : String!
    var BookPost = SelectedPostDetail()
    var myTimer = Timer()
    
    //Refresh the home screen for reserve book
    var reserveBookWithPostID: ((String, String)->Void)! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showSettingButton()
        showLogoWithBackButon()
        self.setTitleWithLocalizedString(title: "nav.BookDetail")
        let nibBook = UINib.init(nibName: "BookDetailCell", bundle: nil)
        self.tblBookDetail.register(nibBook, forCellReuseIdentifier: "BookDetailCell")
        let nibSeller = UINib.init(nibName: "SellerInfoCell", bundle: nil)
        self.tblBookDetail.register(nibSeller, forCellReuseIdentifier: "SellerInfoCell")
        self.tblBookDetail.register(UINib.init(nibName: "SocialSharingCell", bundle: nil), forCellReuseIdentifier: "SocialSharingCell")
        self.tblBookDetail.register(UINib.init(nibName: "ReserveBookOptionCell", bundle: nil), forCellReuseIdentifier: "ReserveBookOptionCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getBookDetails()
        self.navigationController?.navigationBar.isHidden = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.postURL = ""
        self.myTimer.invalidate()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func reloadTableview(){
        let time1 = AppTheme.sharedInstance.convertStringToDate(from: BookPost.currentDateTime)
        let date = time1.addingTimeInterval(1)
        BookPost.currentDateTime = AppTheme.sharedInstance.convertDateToString(from: date)
        let reserveTimeForBook = AppTheme.sharedInstance.convertStringToDate(from: BookPost.reserveBookEndTime)
        if date.compare(reserveTimeForBook as Date) == ComparisonResult.orderedDescending
        {
           // release book as the reserve book end time comes.
           releaseBook(strURL: APPURL.releaseBookAfter48Hours)
           
        } else if date.compare(reserveTimeForBook as Date) == ComparisonResult.orderedAscending
        {
            self.tblBookDetail.reloadData()
            
        }else
        {
           //release book as the reserve book end time comes.
            releaseBook(strURL: APPURL.releaseBookAfter48Hours)
        }
        
    }

    @IBAction func btnProfileClicked( _ sender : UIButton){
            //Navigate to friend's profile screen
        var isViewExist = false
        for controller in self.navigationController!.viewControllers as Array {
            if controller is FriendProfileVC{
                isViewExist = true
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
            else
            {
                isViewExist = false
                
            }
            //if controller.isKind(of: DashboardVC.self)
        }
        if !isViewExist
        {
            if BookPost.Userid != AppTheme.sharedInstance.currentUserData[0].UserID{
                let friendProfileVC : FriendProfileVC = storyBoard.instantiateViewController(withIdentifier: "FriendProfileVC") as! FriendProfileVC
                friendProfileVC.strFriendID = self.BookPost.Userid
                friendProfileVC.strFriendName = self.BookPost.UserName
                self.navigationController?.pushViewController(friendProfileVC, animated: true)
            }else{
                let profile = storyBoard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                profile.isPreviousViewController = true
                self.navigationController?.pushViewController(profile, animated: true)
            }
        }
    }
    @IBAction func btnReserveBookClicked( _ sender : UIButton){
        reserveBook()
    }
    @IBAction func btnCancelBookReservation( _ sender : UIButton){
        releaseBook(strURL: APPURL.releaseBook)
    }
    @IBAction func btnPayForBook( _ Sender : UIButton){
        var postURL = "\(APPURL.Domains.Dev)book-detail/\(BookPost.PostID)"
        AppTheme.sharedInstance.moveToSafari(viewController: self, postURL : &postURL)

    }
    // MARK: - tableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if BookPost.Userid == ""{
            return 0
        }else{
            if RV_CheckDataType.getBoolValue(obj: self.BookPost.isReserved as AnyObject){
                if BookPost.reserveBookUserID == AppTheme.sharedInstance.currentUserData[0].UserID{
                    // If book is reserverd for loggedin user
                    // Show book details
                    // Show seller info with timer and rating
                    // show social sharing option
                    return 3
                }else{
                    // if book is reserverd by someone else
                    // Show book details
                    // Show social sharing option
                    return 2
                }
            }else{
                // If book is not reserverd
                // Show book details
                // Option to reserver the book
                // show social sharing option
                if BookPost.Userid == AppTheme.sharedInstance.currentUserData[0].UserID{
                    return 2
                }else{
                    return 3
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if BookPost.isReserved == "1"{
            if BookPost.reserveBookUserID != AppTheme.sharedInstance.currentUserData[0].UserID && indexPath.row == 1{
                return 90.0
            }
        }
        if indexPath.row == 2{
            return 90.0
        }else{
            if BookPost.Userid == AppTheme.sharedInstance.currentUserData[0].UserID && indexPath.row == 1{
                return 90.0
            }
            return tblBookDetail.estimatedRowHeight
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == 0{
            return configureBookDetailsCell(indexPath: indexPath)
        }else if indexPath.row == 1{
            if RV_CheckDataType.getBoolValue(obj: self.BookPost.isReserved as AnyObject){
                if BookPost.reserveBookUserID == AppTheme.sharedInstance.currentUserData[0].UserID{
                    return configureSellerInfoCell(indexPath: indexPath)
                }else{
                    return configureSocialSharingCell(indexPath: indexPath)
                }
            }else{
                if BookPost.Userid == AppTheme.sharedInstance.currentUserData[0].UserID{
                    return configureSocialSharingCell(indexPath: indexPath)
                }else{
                    return configureReserveBookOptionCell(indexPath: indexPath)
                }
            }
        }else{
            return configureSocialSharingCell(indexPath: indexPath)
        }
    }
    
    func configureBookDetailsCell(indexPath : IndexPath) -> UITableViewCell{
        let cell = tblBookDetail.dequeueReusableCell(withIdentifier: "BookDetailCell", for: indexPath) as! BookDetailCell
        cell.selectionStyle = .none
        cell.configureBasic(BookDetails: BookPost)
        return cell
    }
    func configureReserveBookOptionCell(indexPath : IndexPath) -> UITableViewCell {
        let cell = self.tblBookDetail.dequeueReusableCell(withIdentifier: "ReserveBookOptionCell", for: indexPath) as! ReserveBookOptionCell
        cell.btnReserveBook.addTarget(self, action: #selector(btnReserveBookClicked(_:)), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    func configureSellerInfoCell(indexPath : IndexPath) -> UITableViewCell{
        let cell = tblBookDetail.dequeueReusableCell(withIdentifier: "SellerInfoCell", for: indexPath) as! SellerInfoCell
        cell.configureSellerInfo(Seller: BookPost, isForBookDetails: true)
        cell.btnName.addTarget(self, action: #selector(btnProfileClicked(_:)), for: .touchUpInside)
        cell.selectionStyle = .none
        cell.viewReserveBookOptionsHeight.constant = 45
        let time1 = AppTheme.sharedInstance.convertStringToDate(from: BookPost.currentDateTime)
        let time2 = AppTheme.sharedInstance.convertStringToDate(from: BookPost.reserveBookEndTime)
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        cell.btnPaySeller.setTitle(formatter.string(from: time1 as Date, to: time2 as Date), for: .normal)
        cell.btnPaySeller.removeTarget(self, action: #selector(btnReserveBookClicked(_:)), for: .touchUpInside)
        cell.btnPayforPost.setTitle("btn.Title.buyBook".localized, for: .normal)
        cell.btnPayforPost.addTarget(self, action: #selector(btnPayForBook(_:)), for: .touchUpInside)
        cell.btnPaySeller.isHidden = false
        cell.btnCancel.addTarget(self, action: #selector(btnCancelBookReservation(_:)), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    func configureSocialSharingCell(indexPath : IndexPath) -> UITableViewCell{
        let cell = self.tblBookDetail.dequeueReusableCell(withIdentifier: "SocialSharingCell", for: indexPath) as! SocialSharingCell
        self.postURL = BookPost.postURL
        cell.btnFaceBook.addTarget(self, action: #selector(btnFacebookShareClicked(_:)), for: .touchUpInside)
        cell.btnMessanger.addTarget(self, action: #selector(btnMessangerShareClicked(_:)), for: .touchUpInside)
        cell.btnEmail.addTarget(self, action: #selector(btnEmailClicked(_:)), for: .touchUpInside)
        cell.btnTwitter.addTarget(self, action: #selector(btnTwitterClicked(_:)), for: .touchUpInside)
        cell.btnInstaGram.addTarget(self, action: #selector(btnTwitterClicked(_:)), for: .touchUpInside)
        cell.btnTikTok.addTarget(self, action: #selector(btnTiktokClicked(_:)), for: .touchUpInside)
        cell.btnSnapChat.addTarget(self, action: #selector(btnSnapChatClicked(_:)), for: .touchUpInside)
        cell.btnWhatsApp.addTarget(self, action: #selector(btnTwitterClicked(_:)), for: .touchUpInside)
        cell.btnMessage.addTarget(self, action: #selector(btnTwitterClicked(_:)), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
    //MARK
    //MARK : Web Service Call
    func getBookDetails(){
        let when = DispatchTime.now() + 0.0
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"
        WebAPI.httpJSONRequest(viewController: self, url: APPURL.getBookDetails, params: ["post_id": strPostID as AnyObject, "lang": lang as AnyObject], SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    let json = responseData["responseData"].dictionaryValue
                    self.BookPost.parser(json)
                    if RV_CheckDataType.getBoolValue(obj: self.BookPost.isReserved as AnyObject) && self.BookPost.reserveBookUserID == AppTheme.sharedInstance.currentUserData[0].UserID{
                        self.myTimer = Timer(timeInterval: 1.0, target: self, selector: #selector(self.reloadTableview), userInfo: nil, repeats: true)
                        RunLoop.main.add(self.myTimer, forMode: RunLoop.Mode.default)
                       self.tblBookDetail.reloadData()
                    }
                     self.tblBookDetail.reloadData()
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject))
            }
            
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
        
    }
    func reserveBook(){
        self.myTimer.invalidate()
        let when = DispatchTime.now() + 0.0
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"
        MBProgressHUD.showAdded(to: self.view, animated: true)
        WebAPI.httpJSONRequest(viewController: self, url: APPURL.reserveBook, params: ["post_id": strPostID as AnyObject, "lang": lang as AnyObject], SuccessHandler: { (responseData) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    let json = responseData["responseData"].dictionaryValue
                    self.BookPost.reserveBookEndTime = (json["reserve_end_date"]?.stringValue)!
                    self.BookPost.currentDateTime = (json["current_date_and_time"]?.stringValue)!
                    self.BookPost.isReserved = "1"
                    self.BookPost.reserveBookUserID = AppTheme.sharedInstance.currentUserData[0].UserID
                    self.myTimer = Timer(timeInterval: 1.0, target: self, selector: #selector(self.reloadTableview), userInfo: nil, repeats: true)
                    self.reserveBookWithPostID(self.BookPost.PostID, "1")
                    RunLoop.main.add(self.myTimer, forMode: RunLoop.Mode.default)
                }
            }else{
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject))
            }
            
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
        
    }
    func releaseBook(strURL : String){
        self.myTimer.invalidate()
        let when = DispatchTime.now() + 0.0
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"
        MBProgressHUD.showAdded(to: self.view, animated: true)
        WebAPI.httpJSONRequest(viewController: self, url: strURL, params: ["post_id": strPostID as AnyObject, "lang": lang as AnyObject], SuccessHandler: { (responseData) in
            MBProgressHUD.hide(for: self.view, animated: true)
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    
                    self.BookPost.isReserved = "0"
                    self.reserveBookWithPostID(self.BookPost.PostID, "0")
                    self.tblBookDetail.reloadData()
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject))
            }
            
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
        
    }
}
