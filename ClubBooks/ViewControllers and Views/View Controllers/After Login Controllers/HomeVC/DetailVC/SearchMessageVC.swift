//
//  SearchMessageVC.swift
//  ClubBooks
//
//  Created by cis on 18/09/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class SearchMessageVC: BaseViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var tblChat : UITableView!
    @IBOutlet var lblUserName : UILabel!
    @IBOutlet var lblSchoolName : UILabel!
    @IBOutlet var searchView : UIView!
    var arrayChat = [PostChatDetail]()
    var strPostID : String!
    var strPostCreator : String!
    var strPostTitle : String!
    var isChatOnbooks : Bool!
    var intPageNumber : Int! = 1
    let searchBar = UISearchBar()
     private let refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        tblChat.tableFooterView = UIView()
        self.showBackButtonWith(Image: #imageLiteral(resourceName: "btn.back"))
        if #available(iOS 10.0, *) {
            tblChat.refreshControl = refreshControl
        } else {
            tblChat.addSubview(refreshControl)
        }
        lblUserName.text = strPostCreator
        lblSchoolName.text = strPostTitle
        let nibReceivedCell = UINib.init(nibName: "ChatReceivedCell", bundle: nil)
        self.tblChat.register(nibReceivedCell, forCellReuseIdentifier: "ChatReceivedCell")
        let nibSenderCell = UINib.init(nibName: "ChatSenderCell", bundle: nil)
        self.tblChat.register(nibSenderCell, forCellReuseIdentifier: "ChatSenderCell")
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        animateSearchBar()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         self.navigationController?.navigationBar.isHidden = true
    }
    @objc private func refreshData(_ sender: Any) {
        // Fetch Weather Data
        intPageNumber = intPageNumber + 1
        getChatHistoryfor(Page: RV_CheckDataType.getString(anyString: intPageNumber as AnyObject))
    }
    func animateSearchBar(){
        searchBar.alpha = 1
        searchBar.sizeToFit()
        self.navigationItem.rightBarButtonItem = nil
        searchBar.placeholder = ""
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as! UITextField
        textFieldInsideSearchBar.textColor = UIColor.black
        textFieldInsideSearchBar.backgroundColor = UIColor.init(red: 225/255.0, green: 225/255.0, blue: 225/255.0, alpha: 1.0)
        textFieldInsideSearchBar.placeHolderColor = UIColor.init(red: 200/255.0, green: 200/255.0, blue: 200/255.0, alpha: 1.0)
        textFieldInsideSearchBar.placeholder = ManageLocalization.getLocalaizedString(key: "txtfldplaceholder.search")
        searchView.addSubview(searchBar)
        
        let animation = CAKeyframeAnimation()
        animation.keyPath = "position.x"
        animation.values = [self.view.superview?.frame.size.width as Any, 0]
        animation.keyTimes = [0.5, 1]
        animation.duration = 0.5
        animation.isAdditive = true
        
        searchView.addSubview(searchBar)
        self.searchBar.becomeFirstResponder()
    }
    //MARK:
    //MARK: Searchbar Delegates
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        arrayChat.removeAll()
        tblChat.reloadData()
        if searchBar.text == ""{
            return
        }else{
            getChatHistoryfor(Page: RV_CheckDataType.getString(anyString: intPageNumber as AnyObject))
        }
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    // MARK: -
    // MARK: - UITableViewDelegates and UITableviewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayChat.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let postchatdetails = arrayChat[indexPath.row]
        if  !postchatdetails.isMyMessage {
            let chatReceivedCell = tblChat.dequeueReusableCell(withIdentifier: "ChatReceivedCell", for: indexPath) as! ChatReceivedCell
            chatReceivedCell.message.text = postchatdetails.chatMessage
            chatReceivedCell.profilePic.sd_setImage(with: URL.init(string: postchatdetails.userImage), placeholderImage: #imageLiteral(resourceName: "btn_loginbackground"), options: [], completed: nil)
            return chatReceivedCell
            
        }else{
            let chatSenderdCell = tblChat.dequeueReusableCell(withIdentifier: "ChatSenderCell", for: indexPath) as! ChatSenderCell
            chatSenderdCell.lblUsername.text = AppTheme.sharedInstance.currentUserData[0].UserName
            chatSenderdCell.message.text = postchatdetails.chatMessage
            return chatSenderdCell
        }
    }
    
    func scrollToBottom(){
        if arrayChat.count > 0 {
            DispatchQueue.main.async {
                let indexPath = IndexPath(row: self.arrayChat.count-1, section: 0)
                self.tblChat.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    // MARK: -
    // MARK: - Web Service Implementation
    func getChatHistoryfor(Page : String){
        var requestURL = ""
        if isChatOnbooks{
            requestURL = APPURL.getBookChatHistory
        }else{
            requestURL = APPURL.getTopicChatHistory
        }
        let when = DispatchTime.now() + 0.0
        WebAPI.httpJSONRequest(viewController: self, url: requestURL, params: ["post_id": self.strPostID as AnyObject , "lang" : "es" as AnyObject, "page" : Page as AnyObject, "search" : searchBar.text as AnyObject], SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    for json in responseData["responseData"].arrayValue {
                        let postFeed = PostChatDetail()
                        postFeed.parser(json)
                        self.arrayChat.insert(postFeed, at: 0)
                    }
                    self.refreshControl.endRefreshing()
                    self.tblChat.reloadData()
                    if self.intPageNumber == 1{
                        self.scrollToBottom()
                    }
                }
            }else{
                self.refreshControl.endRefreshing()
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
            
        }) { (error) in
            self.refreshControl.endRefreshing()
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
        
    }
}
