//
//  BooksChatVC.swift
//  ClubBooks
//
//  Created by cis on 29/03/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit
import MBProgressHUD
class BooksChatVC: BaseViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet var tblChat : UITableView!
    @IBOutlet var txtViewMessage : HSExpandableTextView!
    @IBOutlet var lblUserName : UILabel!
    @IBOutlet var lblSchoolName : UILabel!
    @IBOutlet var btnScrollToBottom : UIButton!
    var arrayChat = [PostChatDetail]()
    var strPostID : String!
    var strPostCreator : String!
    var strPostCreatorId : String!
    var strPostTitle : String!
    var isChatOnbooks : Bool!
    var intPageNumber : Int! = 1
    var isAnonymous : Bool = false
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showLogoWithBackButon()
        let nibReceivedCell = UINib.init(nibName: "ChatReceivedCell", bundle: nil)
        self.tblChat.register(nibReceivedCell, forCellReuseIdentifier: "ChatReceivedCell")
        let nibSenderCell = UINib.init(nibName: "ChatSenderCell", bundle: nil)
        self.tblChat.register(nibSenderCell, forCellReuseIdentifier: "ChatSenderCell")
        self.tblChat.register(UINib.init(nibName: "ChatReceivedImageCell", bundle: nil), forCellReuseIdentifier: "ChatReceivedImageCell")
        self.tblChat.register(UINib.init(nibName: "ChatSenderImageCell", bundle: nil), forCellReuseIdentifier: "ChatSenderImageCell")
        
        getChatHistoryfor(Page: RV_CheckDataType.getString(anyString: intPageNumber as AnyObject))
        txtViewBlocksMethods()
        sendMediaClicked()
        
        lblUserName.text = strPostTitle
        if !isAnonymous{
            lblSchoolName.text = strPostCreator
        }else{
            lblSchoolName.text = ""
        }
        
        if #available(iOS 10.0, *) {
            tblChat.refreshControl = refreshControl
        } else {
            tblChat.addSubview(refreshControl)
        }
        
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        addRightBarButtonItems()
        addTapGestureToSchoolName()
        // Do any additional setup after loading the view.
    }
    
    func addTapGestureToSchoolName(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        lblSchoolName.isUserInteractionEnabled = true
        lblSchoolName.addGestureRecognizer(tap)
    }
    
    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        //Do somethin
        let friendProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "FriendProfileVC") as! FriendProfileVC
        friendProfileVC.strFriendID = strPostCreatorId
        friendProfileVC.strFriendName = strPostCreator
        navigationController?.pushViewController(friendProfileVC, animated: true)
    }
    
    func addRightBarButtonItems()
    {
        let btnSetting = UIButton.init(type: .custom)
        btnSetting.setImage(UIImage(named: "icon.verticalDots"), for: .normal)
        btnSetting.addTarget(self, action: #selector(btnOpenSettingClicked), for: .touchUpInside)
        let btnSearch = UIButton.init(type: .custom)
        btnSearch.setImage(#imageLiteral(resourceName: "tab.unselected.search"), for: .normal)
        btnSearch.addTarget(self, action: #selector(btnSearchBarButtonClicked(sender:)), for: .touchUpInside)
        let stackview = UIStackView.init(arrangedSubviews: [btnSearch, btnSetting])
        stackview.distribution = .fillEqually
        stackview.axis = .horizontal
        stackview.alignment = .center
        stackview.spacing = 15
        let rightBarButton = UIBarButtonItem(customView: stackview)
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    @IBAction func btnSearchBarButtonClicked( sender : UIButton){
        let objTarget = storyboard?.instantiateViewController(withIdentifier: "SearchMessageVC") as! SearchMessageVC
        objTarget.strPostID = strPostID
        objTarget.strPostCreator = strPostCreator
        objTarget.strPostTitle = strPostTitle
        objTarget.isChatOnbooks = isChatOnbooks
        self.navigationController?.pushViewController(objTarget, animated: true)
    }
    @objc private func refreshData(_ sender: Any) {
        // Fetch Weather Data
        intPageNumber = 1
        arrayChat.removeAll()
        self.tblChat.reloadData()
        getChatHistoryfor(Page: RV_CheckDataType.getString(anyString: intPageNumber as AnyObject))
    }
    
    @IBAction func btnScrollToBottom( _ sender : UIButton){
        if arrayChat.count > 0{
            let indexPath = IndexPath(item: arrayChat.count - 1, section: 0)
            tblChat.scrollToRow(at: indexPath, at: UITableView.ScrollPosition.bottom, animated: true)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidDisappear(true)
//        txtViewMessage.btnMediaWidthConstraints.constant = 0.0
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
    }
    
    // MARK: -
    // MARK: - HSTextView Blocks
    func txtViewBlocksMethods(){
       
        txtViewMessage.sendMessage = { message in
            self.sendMessageForChat(Message: message)
        }
    }

    func sendMediaClicked(){
        self.txtViewMessage.sendMedia = {
            let alert = UIAlertController.init(title: "ClubBooks", message: "alert.shareImageVideo".localized, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction.init(title: "alert.image".localized, style: .default, handler: { (action) in
                guard UIImagePickerController.isCameraDeviceAvailable(.front) else {return}
                let picker = UIImagePickerController.init()
                picker.sourceType = .camera
                picker.allowsEditing = true
                picker.delegate = self
                self.navigationController?.present(picker, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction.init(title: "alert.choooseimage".localized, style: .default, handler: { (action) in
                let picker = UIImagePickerController.init()
                picker.sourceType = .photoLibrary
                picker.allowsEditing = true
                picker.delegate = self
                self.navigationController?.present(picker, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction.init(title: "alert.cancel".localized, style: .cancel, handler: { (action) in
            }))
            self.navigationController?.present(alert, animated: true, completion: nil)
        }
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    // MARK: -
    // MARK: - UIImagePickerControllerDelegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            picker.dismiss(animated: true) {
                if let isImage = info[.originalImage]  {
                self.txtViewMessage.activityIndicator.isHidden = false
                    self.sendImageMessage(img: isImage as! UIImage)
                    
            } else if let isURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
                self.txtViewMessage.activityIndicator.isHidden = false
            }
        }
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
    // MARK: -
    // MARK: - UITableViewDelegates and UITableviewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayChat.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let postchatdetails = arrayChat[indexPath.row]
        if  !postchatdetails.isMyMessage {
            if  postchatdetails.chatImage == ""{
                let chatReceivedCell = tblChat.dequeueReusableCell(withIdentifier: "ChatReceivedCell", for: indexPath) as! ChatReceivedCell
                chatReceivedCell.message.text = postchatdetails.chatMessage
                chatReceivedCell.lblUserName.text = postchatdetails.SenderName
                chatReceivedCell.userID = postchatdetails.SenderID
                chatReceivedCell.delegate = self
                chatReceivedCell.lblMessageDate.text = "\(AppTheme.sharedInstance.getDateFromString(strDate: postchatdetails.MessageTime)) \(AppTheme.sharedInstance.getTimeFromString(strTime : postchatdetails.MessageTime))"
                return chatReceivedCell
            }else{
                let chatImageCell = tblChat.dequeueReusableCell(withIdentifier: "ChatReceivedImageCell", for: indexPath) as! ChatReceivedImageCell
                chatImageCell.lblUserName.text = postchatdetails.SenderName
                chatImageCell.lblMessageDate.text = "\(AppTheme.sharedInstance.getDateFromString(strDate: postchatdetails.MessageTime)) \(AppTheme.sharedInstance.getTimeFromString(strTime : postchatdetails.MessageTime))"
                chatImageCell.imgChatMessage.sd_setImage(with: URL.init(string: postchatdetails.chatImage))
                return chatImageCell
            }
            
        }else{
            if  postchatdetails.chatImage == ""{
                let chatSenderdCell = tblChat.dequeueReusableCell(withIdentifier: "ChatSenderCell", for: indexPath) as! ChatSenderCell
                chatSenderdCell.delegate = self
                chatSenderdCell.userID = AppTheme.sharedInstance.currentUserData[0].UserID
                chatSenderdCell.lblUsername.text = AppTheme.sharedInstance.currentUserData[0].UserName
                chatSenderdCell.message.text = postchatdetails.chatMessage
                chatSenderdCell.lblMessageDate.text = "\(AppTheme.sharedInstance.getDateFromString(strDate: postchatdetails.MessageTime)) \(AppTheme.sharedInstance.getTimeFromString(strTime : postchatdetails.MessageTime))"
                return chatSenderdCell
            }else{
                let chatImageCell = tblChat.dequeueReusableCell(withIdentifier: "ChatSenderImageCell", for: indexPath) as! ChatSenderImageCell
                chatImageCell.imgChatMessage?.sd_setImage(with: URL.init(string: postchatdetails.chatImage))
                chatImageCell.lblMessageDate.text = "\(AppTheme.sharedInstance.getDateFromString(strDate: postchatdetails.MessageTime)) \(AppTheme.sharedInstance.getTimeFromString(strTime : postchatdetails.MessageTime))"
                return chatImageCell
            }
            
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard arrayChat[indexPath.row] != nil else {return}
        let postDetails = arrayChat[indexPath.row]
        if postDetails.chatImage != ""{
            let vc = HSImageViewer.init(nibName: "HSImageViewer", bundle: Bundle.main)
            vc.imgURL = "\(postDetails.chatImage)"
            vc.returnErro = { error in
                vc.dismiss(animated: true, completion: {
                    let alert = UIAlertController.init(title: Constant.ALERT_TITLE, message: "\(error)", preferredStyle: .alert)
                    alert.addAction(UIAlertAction.init(title: "Cancel", style: .default, handler: nil))
                    self.navigationController?.present(alert, animated: true, completion: nil)
                })
            }
            self.navigationController?.present(vc, animated: true, completion: nil)
        }
        
    }
    func scrollToBottom(){  
        if arrayChat.count > 0 {
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.arrayChat.count-1, section: 0)
            self.tblChat.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    // MARK: -
    // MARK: - Web Service Implementation
    func getChatHistoryfor(Page : String){
        var requestURL = ""
        if isChatOnbooks{
           requestURL = APPURL.getBookChatHistory
        }else{
            requestURL = APPURL.getTopicChatHistory
        }
        let when = DispatchTime.now() + 0.0
        WebAPI.httpJSONRequest(viewController: self, url: requestURL, params: ["post_id": self.strPostID as AnyObject , "lang" : "es" as AnyObject, "page" : Page as AnyObject], SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    
                    print(responseData.arrayValue)
                    for json in responseData["responseData"].arrayValue {
                        let postFeed = PostChatDetail()
                        postFeed.parser(json)
                        self.arrayChat.append(postFeed)
                    }
                    self.refreshControl.endRefreshing()
                    self.tblChat.reloadData()
                    if self.intPageNumber == 1{
                        self.scrollToBottom()
                    }
                }
            }else{
                self.refreshControl.endRefreshing()
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
            
        }) { (error) in
            self.refreshControl.endRefreshing()
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
    func sendImageMessage(img : UIImage){
        var requestURL = ""
        if isChatOnbooks{
            requestURL = APPURL.postMessageOnBook
        }else{
            requestURL = APPURL.postMessageOnTopic
        }
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"
        let when = DispatchTime.now() + 0.0
        WebAPI.multipartHttpRequestWithImageArray(viewController: self, imageKey: "chat_image", arrayImages: [img], url: requestURL, params: ["chat_id": self.strPostID as AnyObject , "chat_image" : img as AnyObject, "chat_message" : "abc" as AnyObject, "lang": lang as AnyObject]) { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    
                    print(responseData.arrayValue)
                    let postFeed = PostChatDetail()
                    postFeed.parser(responseData["responseData"])
                    self.arrayChat.append(postFeed)
                    self.tblChat.reloadData()
                    self.scrollToBottom()
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
            
        } Errorhandler: { error in
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }

    }
    func sendMessageForChat(Message : String){
        var requestURL = ""
        if isChatOnbooks{
            requestURL = APPURL.postMessageOnBook
        }else{
            requestURL = APPURL.postMessageOnTopic
        }
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"
        let when = DispatchTime.now() + 0.0
        WebAPI.httpJSONRequest(viewController: self, url: requestURL, params: ["chat_id": self.strPostID as AnyObject , "chat_message" : Message as AnyObject, "lang": lang as AnyObject], SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    
                    print(responseData.arrayValue)
                    let postFeed = PostChatDetail()
                    postFeed.parser(responseData["responseData"])
                    self.arrayChat.append(postFeed)
                    self.tblChat.reloadData()
                    self.scrollToBottom()
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
            }
            
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
        
    }

}

extension BooksChatVC: ChatReceivedCellDelegate, ChatSenderCellDelegate{
    func openUserProfile(userID: String, username: String) {
        let nextVC = self.storyboard?.instantiateViewController(withIdentifier: "FriendProfileVC") as! FriendProfileVC
        nextVC.strFriendID = userID
        nextVC.strFriendName = username
        navigationController?.pushViewController(nextVC, animated: true)
    }
}
