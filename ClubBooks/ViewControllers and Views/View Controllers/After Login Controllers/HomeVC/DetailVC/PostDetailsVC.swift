//
//  ExamDetails.swift
//  ClubBooks
//
//  Created by cis on 26/03/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit
import MBProgressHUD
enum PostType : Int
{
    case PostExam = 0
    case PostQuiz
    case PostClassNotes
}
class PostDetailsVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var tblExamDetail : UITableView!
    var strPostID : String!
    var Post = SelectedPostDetail()
    var postType: PostType = PostType.init(rawValue: 0)!
    override func viewDidLoad() {
        super.viewDidLoad()
        showSettingButton()
        showLogoWithBackButon()
        if postType == .PostExam{
            self.setTitleWithLocalizedString(title: "nav.ExamDetail")
        }else if postType == .PostQuiz{
             self.setTitleWithLocalizedString(title: "nav.QuizDetail")
        }else{
             self.setTitleWithLocalizedString(title: "nav.NotesDetail")
        }
        let nibBook = UINib.init(nibName: "PostDetailCell", bundle: nil)
        self.tblExamDetail.register(nibBook, forCellReuseIdentifier: "PostDetailCell")
        let nibSeller = UINib.init(nibName: "SellerInfoCell", bundle: nil)
        self.tblExamDetail.register(nibSeller, forCellReuseIdentifier: "SellerInfoCell")
        let nibSocialSharing = UINib.init(nibName: "SocialSharingCell", bundle: nil)
        self.tblExamDetail.register(nibSocialSharing, forCellReuseIdentifier: "SocialSharingCell")
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         getSelectedPostDetails()
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.postURL = ""
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnProfileClicked( _ sender : UIButton){
       
            //Post user id is not same as the logged in user.
            //Navigate to friend's profile screen
        var isViewExist = false
        for controller in self.navigationController!.viewControllers as Array {
            if controller is FriendProfileVC{
                isViewExist = true
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
            else
            {
                isViewExist = false
                
            }
            //if controller.isKind(of: DashboardVC.self)
        }
        if !isViewExist
        {
            if self.Post.Userid != AppTheme.sharedInstance.currentUserData[0].UserID{
                let friendProfileVC : FriendProfileVC = storyBoard.instantiateViewController(withIdentifier: "FriendProfileVC") as! FriendProfileVC
                friendProfileVC.strFriendID = self.Post.Userid
                friendProfileVC.strFriendName = self.Post.UserName
                self.navigationController?.pushViewController(friendProfileVC, animated: true)
            }else{
                let profile = storyBoard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
                profile.isPreviousViewController = true
                self.navigationController?.pushViewController(profile, animated: true)
            }
        }
    }
    @IBAction func btnPayForPost( _ Sender : UIButton){
        var postURL = ""
        if postType == .PostExam{
            postURL = "\(APPURL.Domains.Dev)exam-detail/\(Post.PostID)"
        }else if postType == .PostQuiz{
            postURL = "\(APPURL.Domains.Dev)quiz-detail/\(Post.PostID)"
        }else{
            postURL = "\(APPURL.Domains.Dev)classnote-detail/\(Post.PostID)"
        }
        
        AppTheme.sharedInstance.moveToSafari(viewController: self, postURL : &postURL)
    }
    // MARK: - tableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if Post.Userid == ""{
            return 0
        }else{
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostDetailCell", for: indexPath) as! PostDetailCell
            if postType == .PostExam{
                cell.cellType = .PostExam
            }else if postType == .PostQuiz{
                cell.cellType = .PostQuiz
            }else{
                cell.cellType = .PostClassNotes
            }
            cell.configureCell(PostDetails: Post)
            cell.selectionStyle = .none
            
            return cell
        }
        else if indexPath.row == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SellerInfoCell", for: indexPath) as! SellerInfoCell
            cell.selectionStyle = .none
            if (Post.BookIsAnnonymous == "true" || Post.BookIsAnnonymous == "1"){
                cell.btnName.removeTarget(self, action: #selector(btnProfileClicked(_:)), for: .touchUpInside)
            }else{
                cell.btnName.addTarget(self, action: #selector(btnProfileClicked(_:)), for: .touchUpInside)
            }
            cell.configureSellerInfo(Seller: Post, isForBookDetails: false)
            cell.viewReserveBookOptionsHeight.constant = 0
            var buyButtonTitle = ""
            if self.postType == .PostClassNotes{
                buyButtonTitle = "btn.Title.buyNotes".localized
            }else if self.postType == .PostExam {
                buyButtonTitle = "btn.Title.buyExam".localized
            }else if self.postType == .PostQuiz{
                buyButtonTitle = "btn.Title.buyQuiz".localized
            }
            cell.btnPaySeller.setTitle(buyButtonTitle, for: .normal)
            cell.btnPaySeller.addTarget(self, action: #selector(btnPayForPost(_:)), for: .touchUpInside)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SocialSharingCell", for: indexPath) as! SocialSharingCell
            self.postURL = Post.postURL
            cell.btnFaceBook.addTarget(self, action: #selector(btnFacebookShareClicked(_:)), for: .touchUpInside)
            cell.btnMessanger.addTarget(self, action: #selector(btnMessangerShareClicked(_:)), for: .touchUpInside)
            cell.btnEmail.addTarget(self, action: #selector(btnEmailClicked(_:)), for: .touchUpInside)
            cell.btnTwitter.addTarget(self, action: #selector(btnTwitterClicked(_:)), for: .touchUpInside)
            cell.btnInstaGram.addTarget(self, action: #selector(btnTwitterClicked(_:)), for: .touchUpInside)
            cell.btnTikTok.addTarget(self, action: #selector(btnTiktokClicked(_:)), for: .touchUpInside)
            cell.btnSnapChat.addTarget(self, action: #selector(btnSnapChatClicked(_:)), for: .touchUpInside)
            cell.btnWhatsApp.addTarget(self, action: #selector(btnTwitterClicked(_:)), for: .touchUpInside)
            cell.btnMessage.addTarget(self, action: #selector(btnTwitterClicked(_:)), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2{
            return 90.0
        }else{
            return tblExamDetail.estimatedRowHeight
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    // MARK: -
    // MARK: - Web Service Call
    func getSelectedPostDetails(){
        let when = DispatchTime.now() + 0.0
        
        var strUrlString : String = String()
        if postType == .PostExam{
            strUrlString = APPURL.getExamDetails
        }else if postType == .PostQuiz {
            strUrlString = APPURL.getQuizDetails
        }else{
            strUrlString = APPURL.getNotesDetails
        }
        WebAPI.httpJSONRequest(viewController: self, url: strUrlString, params: ["post_id": strPostID as AnyObject], SuccessHandler: { (responseData) in
            let Status = responseData.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    
                    print(responseData.arrayValue)
                    let json = responseData["responseData"].dictionaryValue
                    self.Post.parser(json)
                    self.tblExamDetail.reloadData()
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: responseData.dictionary!["msg"]?.stringValue as AnyObject) )
                
            }
            
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
        
    }

}
