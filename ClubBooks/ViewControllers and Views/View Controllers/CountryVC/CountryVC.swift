//
//  CountryVC.swift
//  MyFirstApp
//
//  Created by cis on 29/01/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

var selectedCountryName  = ""

class CountryVC: UIViewController {
    
    //MARK:-
    //MARK: - IBOutlet
    
    @IBOutlet weak var tblview: UITableView!
    
    
    //MARK:-
    //MARK: - variables
    
    var arrCountry = [(name : String, code : String)]()
    var arrCountryList = [CountryCodeModelClass]()
    var countryVM =  CountryViewModel()
    
    //MARK:-
    //MARK: - App flow
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        //arrCountryList.append(CountryCodeModelClass(countryFlag: "🇺🇸", countryCode: "+1", countryName: "United States", countryNameWithCode: "US", max_phoneLength: 10))
      //  arrCountryList.append(CountryCodeModelClass(countryFlag: "🇫🇷", countryCode: "+33", countryName: "France", countryNameWithCode: "FR", max_phoneLength: 9))
       // arrCountryList.append(CountryCodeModelClass(countryFlag: "🇮🇳", countryCode: "+91", countryName: "India", countryNameWithCode: "IN", max_phoneLength: 10))
        
        //  let counryCode = Constants.userDefault.object(forKey: Variables.country_code) as! String
        //let index =  self.arrCountryList.firstIndex(where: {$0.countryCode == counryCode})
         arrCountry.append((name: "US", code: "+1"))
         arrCountry.append((name: "Non-US", code: "--"))
    }
    
    
    //MARK:-
    //MARK: - Action Methods:
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-
    //MARK: - Methods:
    
    func initView(){
        tblview.register(UINib(nibName: "CountryCell", bundle: nil), forCellReuseIdentifier: "CountryCell")
        tblview.rowHeight = UITableView.automaticDimension;
        tblview.estimatedRowHeight = 20.0; // set to whatever your "average" cell height is
    }
}


// MARK:-
// MARK: - Extension CountryVC TableView

extension CountryVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCountry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCell", for: indexPath) as! CountryCell
        cell.lblTitle.text =  arrCountry[indexPath.row].name
        
        if arrCountry[indexPath.row].name.lowercased() == selectedCountryName.lowercased(){
            cell.imgSelect.isHidden = false
        }else{
            cell.imgSelect.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
          
        if arrCountry[indexPath.row].name == "US"{
            updateCountry(countryName: arrCountry[indexPath.row].name, date_format: 1, code: arrCountry[indexPath.row].code)
        }else{
            updateCountry(countryName: arrCountry[indexPath.row].name, date_format: 0, code: arrCountry[indexPath.row].code)
        }
    }
}


//MARK:-
//MARK: - API :

extension CountryVC {
    
    func updateCountry(countryName : String , date_format : Int, code : String)
    {
//        Loader.sharedInstance.showLoader(msg: "Loading...")
//
//        countryVM.updateCountryApi(countryName: countryName, date_format: date_format) { (status, message, dictResult) in
//            Loader.sharedInstance.stopLoader()
//
//            if status == "success"
//            {
//                selectedCountryName = countryName
//                 Constants.userDefault.set(date_format, forKey: Variables.date_format)
//                self.tblview.reloadData()
//            }else{
//                SnackBar.sharedInstance.show(message: message, showMsgAt: .bottom)
//            }
//        }
    }
}

