//
//  CountryCell.swift
//  MyFirstApp
//
//  Created by cis on 29/01/21.
//  Copyright © 2021 cis. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgSelect: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
