
//
//  ViewController.swift
//  ClubBooks
//
//  Created by cis on 21/11/17.
//  Copyright © 2017 CIS. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var isNotificationLaunch = String()
    var notifcaitonObject = [String : Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
//        self.perform(#selector(loadController), with: self, afterDelay: 1.0)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getAccessToken()
    }
    @objc func loadController(){
        AppTheme.sharedInstance.getUserDataFromDefault()
        if isNotificationLaunch == "1" {
            self.navigationController?.navigationItem.setHidesBackButton(true, animated: true)
            if AppTheme.sharedInstance.currentUserData.count > 0 {
                if AppTheme.sharedInstance.currentUserData[0].UserID != "" {
                    var numberofappLogins = RV_CheckDataType.getInteger(anyString: AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: "numberOfLogins") as AnyObject)
                    if numberofappLogins == 0{
                        AppTheme.sharedInstance.ClubBooksUserDefaults.set(1, forKey: "numberOfLogins")
                    }else{
                        numberofappLogins = numberofappLogins + 1
                        AppTheme.sharedInstance.ClubBooksUserDefaults.set(numberofappLogins, forKey: "numberOfLogins")
                    }
                    AppTheme.sharedInstance.ClubBooksUserDefaults.synchronize()
                    let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "LYTabBar") as! LYTabBar
                    homeVC.selectedIndex = 3
                     NotificationCenter.default.post(name: Notification.Name("firebaseChatNotifications"), object: nil, userInfo: notifcaitonObject)
                    appDelegate.window?.rootViewController = homeVC
                }
            }else{
                let IntroVC : IntroViewController = storyBoard.instantiateViewController(withIdentifier: "IntroViewController") as! IntroViewController
                self.navigationController?.pushViewController(IntroVC, animated: false)
            }
        }else{
            self.navigationController?.navigationItem.setHidesBackButton(true, animated: true)
            if AppTheme.sharedInstance.currentUserData.count > 0 {
                if AppTheme.sharedInstance.currentUserData[0].UserID != "" {
                    var numberofappLogins = RV_CheckDataType.getInteger(anyString: AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: "numberOfLogins") as AnyObject)
                    if numberofappLogins == 0{
                        AppTheme.sharedInstance.ClubBooksUserDefaults.set(1, forKey: "numberOfLogins")
                    }else{
                        numberofappLogins = numberofappLogins + 1
                        AppTheme.sharedInstance.ClubBooksUserDefaults.set(numberofappLogins, forKey: "numberOfLogins")
                    }
                    AppTheme.sharedInstance.ClubBooksUserDefaults.synchronize()
                    let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "LYTabBar") as! LYTabBar
                    appDelegate.window?.rootViewController = homeVC
                }else{
                    let IntroVC : IntroViewController = storyBoard.instantiateViewController(withIdentifier: "IntroViewController") as! IntroViewController
                    self.navigationController?.pushViewController(IntroVC, animated: false)
                }
            }else{
                let IntroVC : IntroViewController = storyBoard.instantiateViewController(withIdentifier: "IntroViewController") as! IntroViewController
                self.navigationController?.pushViewController(IntroVC, animated: false)
            }
        }
    }
    func getAccessToken(){
        WebAPI.httpSessionRequest(viewController: self, url: APPURL.getAccessToken, params: [:], SuccessHandler: { (response) in
            let responseDict = response.dictionary
            UserAccessToken.sharedInstance.message = (responseDict?["responseData"]?.stringValue)!
            print(UserAccessToken.sharedInstance.message)
            DispatchQueue.main.async {
                self.perform(#selector(self.loadController), with: self, afterDelay: 0.5)
            }
        }) { (error) in

        }
    }
}




