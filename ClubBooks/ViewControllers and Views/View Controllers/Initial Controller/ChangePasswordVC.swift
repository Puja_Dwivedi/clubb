//
//  ChangePasswordVC.swift
//  BlubBooks
//
//  Created by cis on 21/11/17.
//  Copyright © 2017 CIS. All rights reserved.
//

import UIKit
import MBProgressHUD
class ChangePasswordVC: BaseViewController {
    @IBOutlet var txtFldCurrentPassword : UITextField!
    @IBOutlet var txtFldNewPassword : UITextField!
    @IBOutlet var txtFldConfirmPassword : UITextField!
    @IBOutlet var allTextField : [UITextField]!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.showLogoWithBackButon()
        txtFldCurrentPassword.text = ""
        txtFldNewPassword.text = ""
        txtFldConfirmPassword.text = ""
        self.title = "title.forgetpassword".localized
        for textField in allTextField
        {
            textField.addShadowAround(layer: textField.layer)
            textField.setLeftPaddingPoints(10.0)
        }
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBack(sender : UIButton)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSaveClicked( _ sender : UIButton){
        if txtFldCurrentPassword.text == "" {
            Manager.sharedInstance.showAlert(self, message: ManageLocalization.getLocalaizedString(key: "alert.requiredCurrentPassword"))
            return
        }else if txtFldNewPassword.text == "" {
            Manager.sharedInstance.showAlert(self, message: ManageLocalization.getLocalaizedString(key: "alert.newPasswordShouldNotBeBlack"))
            return
        }
        else if txtFldNewPassword.text != txtFldConfirmPassword.text {
            Manager.sharedInstance.showAlert(self, message: ManageLocalization.getLocalaizedString(key: "alert.sampasswordRequired"))
            return
        }
        resetPassword()
    }
    func resetPassword(){
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"
        let params: [String : Any] = [
            "old_password" : RV_CheckDataType.getString(anyString: txtFldCurrentPassword?.text as AnyObject),
            "new_password" : RV_CheckDataType.getString(anyString: txtFldNewPassword?.text as AnyObject),
            "lang": lang as AnyObject]
        
        WebAPI.httpJSONRequest(viewController: self, url: APPURL.ChangePassword, params: params as [String : AnyObject], SuccessHandler: { (jsonResponse) in
            // change 2 to desired number of seconds
            let when = DispatchTime.now() + 0.0
            
            let Status = jsonResponse.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: when) {
                    
                    let alert = UIAlertController(title: Constant.ALERT_TITLE, message:  ManageLocalization.getLocalaizedString(key: "alert.passwordChangedSucessfully"), preferredStyle: .alert)
                    alert.addAction(UIAlertAction.init(title: ManageLocalization.getLocalaizedString(key: "btn.title.signout"), style: .cancel, handler: { (action) in
                        let appdelegate = UIApplication.shared.delegate as! AppDelegate
                        appdelegate.logoutUser()
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }else{
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: jsonResponse.dictionary!["msg"]?.stringValue as AnyObject) )
            }
        }) { (error) in
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
