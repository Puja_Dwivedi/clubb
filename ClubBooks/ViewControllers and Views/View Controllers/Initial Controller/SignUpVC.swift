//
//  SignUpVC.swift
//  ClubBooks
//
//  Created by cis on 21/11/17.
//  Copyright © 2017 CIS. All rights reserved.
//

import UIKit
import MBProgressHUD

enum GenderSelected: Int {
    case male = 0
    case female = 1
    case other = 2
}

class SignUpVC: BaseViewController, UITextFieldDelegate {
    @IBOutlet var txtFldEmailAddress : UITextField!
    @IBOutlet var txtFldName : UITextField!
    @IBOutlet var txtFldUserName : UITextField!
    @IBOutlet var txtFldPassword : UITextField!
    @IBOutlet var txtFldRePassword : UITextField!
    @IBOutlet var txtFldPhone : UITextField!
    @IBOutlet var txtFldSchool : UITextField!
    @IBOutlet var txtFldBook : UITextField!
    @IBOutlet var arrtextField : [UITextField]!
    @IBOutlet var btnLogin : UIButton!
    @IBOutlet var btnMale : UIButton!
    @IBOutlet weak var btnAcceptTerms: UIButton!
    @IBOutlet var btnFemale : UIButton!
    @IBOutlet weak var btnTermsContions: UIButton!
    @IBOutlet var btnGenderOthers : UIButton!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var viewCountryCode: UIView!
    
    var arrCountryList = [CountryCodeModelClass]()
    var selectedCountryCodeInfo:  CountryCodeModelClass =  CountryCodeModelClass(countryFlag: "", countryCode: "", countryName: "", countryNameWithCode: "", max_phoneLength: 0)
    var strSelectedGender : String! = ""
    var genderSelected: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadCountryJsonList()
        for textField in arrtextField
        {
            textField.superview?.addShadowAround(layer: (textField.superview?.layer)!)
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.showShadow()
        let attributedString1 = NSMutableAttributedString(string:"")
        var attrs1 = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: (btnLogin?.titleLabel?.font.pointSize)!),
            NSAttributedString.Key.foregroundColor : UIColor.gray
            ] as [NSAttributedString.Key : Any]
        let buttonTitleStr1 = NSMutableAttributedString(string: ManageLocalization.getLocalaizedString(key: "btn.Title.AlreadyHaveAccount"), attributes:attrs1)
        attrs1 = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: (btnLogin?.titleLabel?.font.pointSize)!+1),
            NSAttributedString.Key.foregroundColor : UIColor.init(red: 79/255.0, green: 195/255.0, blue: 223/255.0, alpha: 1.0),
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue
            ] as [NSAttributedString.Key : Any]
        let buttonTitleStr2 = NSMutableAttributedString(string: ManageLocalization.getLocalaizedString(key: "btn.Title.ClickHere"), attributes:attrs1)
        attributedString1.setAttributedString(buttonTitleStr1)
        attributedString1.append(buttonTitleStr2)
        btnLogin?.setAttributedTitle(attributedString1, for: .normal)
        btnAcceptTerms.setTitle(ManageLocalization.getLocalaizedString(key: "btn.acceptTerms").localized, for: .normal)
        btnTermsContions.setTitle(ManageLocalization.getLocalaizedString(key: "btn.termsconditions").localized, for: .normal)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnCountryCodeTouchUpInside(_ sender: UIButton) {
        self.view.endEditing(true)
        CountryCodeView.sharedInstance.Show(arr: arrCountryList) { [weak self](searchModel) in
            print("selected :\n countryName :  \(searchModel.countryName)")
            self?.selectedCountryCodeInfo = searchModel
            
            UIView.animate(withDuration: 0.5) {
                self?.lblCountryCode.text = "\(searchModel.countryFlag) \(searchModel.countryCode)"
                self?.txtFldPhone.becomeFirstResponder()
                self?.view.layoutIfNeeded()
            }
        }
    }
    
    
    //MARK: Button Actions
    @IBAction func btnBack(sender : UIButton)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnTermsConditionTouchUpinside(_ sender: Any) {
        let url = "https://clubbooks.com/legal"
        if let url = URL(string: url) {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func btnAcceptTouchUpInside(_ sender: Any) {
        if btnAcceptTerms.currentImage == UIImage(named: "icon_checked"){
            btnAcceptTerms.setImage(UIImage(named: "icon_uncheckbox"), for: .normal)
        }else{
            btnAcceptTerms.setImage(UIImage(named: "icon_checked"), for: .normal)
        }
    }
    
    @IBAction func btnMaleClicked( _ sender : UIButton){
        strSelectedGender = "Male"
        genderSelected = GenderSelected.male.rawValue
        btnMale.setImage(#imageLiteral(resourceName: "btn_GenderChecked"), for: .normal)
        btnFemale.setImage(#imageLiteral(resourceName: "btn_GenderUnChecked"), for: .normal)
        btnGenderOthers.setImage(#imageLiteral(resourceName: "btn_GenderUnChecked"), for: .normal)
    }
   
    @IBAction func btnFemaleClicked(_ Sender : UIButton){
        strSelectedGender = "Female"
        genderSelected = GenderSelected.female.rawValue
        btnFemale.setImage(#imageLiteral(resourceName: "btn_GenderChecked"), for: .normal)
        btnMale.setImage(#imageLiteral(resourceName: "btn_GenderUnChecked"), for: .normal)
        btnGenderOthers.setImage(#imageLiteral(resourceName: "btn_GenderUnChecked"), for: .normal)
    }
    @IBAction func btnGenderOtherClicked( _ sender : UIButton){
        strSelectedGender = "Other"
        genderSelected = GenderSelected.other.rawValue
        btnGenderOthers.setImage(#imageLiteral(resourceName: "btn_GenderChecked"), for: .normal)
        btnMale.setImage(#imageLiteral(resourceName: "btn_GenderUnChecked"), for: .normal)
        btnFemale.setImage(#imageLiteral(resourceName: "btn_GenderUnChecked"), for: .normal)
    }
    @IBAction func btnSignupClicked( _ sender : UIButton){
        var strError = ""
        let phone = "\(selectedCountryCodeInfo.countryCode)\(txtFldPhone.text ?? "")"
       
        if RV_Validations.isEmptyField(testStr: txtFldEmailAddress.text!)
        {
            strError = ERROR.LoginSignUpError.EmailAddressRequired
        }
        else  if RV_Validations.isEmptyField(testStr: txtFldUserName.text!)
        {
            strError = ERROR.LoginSignUpError.UsernameRequired
        }
        else  if RV_Validations.isEmptyField(testStr: txtFldName.text!)
        {
            strError = ERROR.LoginSignUpError.NameRequired
        }
        else if !RV_Validations.isValidEmail(testStr: txtFldEmailAddress.text!)
        {
            strError = ERROR.LoginSignUpError.InvalidEmailId
        }
       
        else if RV_Validations.isEmptyField(testStr: txtFldPassword.text!)
        {
            strError = ERROR.LoginSignUpError.PasswordRequired
            
        }else if txtFldPassword.text != txtFldRePassword.text {
            
            strError = ERROR.LoginSignUpError.PasswordShouldbeSame
            
        }else if !RV_Validations.passwordStrength(passString: txtFldPassword.text!){
      
            strError = ERROR.LoginSignUpError.PasswordStrengh
            
        }else if RV_Validations.isEmptyField(testStr: txtFldPhone.text!) {
            
            strError = ERROR.LoginSignUpError.MobileNoRequired
        }else if (phone.count) < 10 {
            
            strError = ERROR.LoginSignUpError.MobileNoStrength
        }
        else if RV_Validations.isEmptyField(testStr: txtFldSchool.text!) {
            
            strError = ERROR.LoginSignUpError.SchoolName
        }
        else if RV_Validations.isEmptyField(testStr: txtFldBook.text!) {
            
            strError = ERROR.LoginSignUpError.FavBookNameRequired
        }else if strSelectedGender == "" {
            strError = ERROR.LoginSignUpError.SelectGender
        }
        if strError != "" {
            Manager.sharedInstance.showAlert(self, message: strError)
            return
        }
        else{
            self.registerUser()
        }
        
    }
    //MARK: UITextField Delegates
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtFldUserName {
            let maxLength = 20
            let currentString = (textField.text ?? "") as NSString
            let newString = currentString.replacingCharacters(in: range, with: string)
            return newString.count <= maxLength
        }else{
            return true
        }
    }
    
    //MARK: WebService Functions
   
    func registerUser(){
        let phone = "\(selectedCountryCodeInfo.countryCode)\(txtFldPhone.text ?? "")"
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"
        let params: [String : Any] = [
            "password" : RV_CheckDataType.getString(anyString: txtFldPassword?.text as AnyObject),
            "username" : RV_CheckDataType.getString(anyString: txtFldUserName.text as AnyObject),
            "name" : RV_CheckDataType.getString(anyString: txtFldName.text as AnyObject),
            "email" : RV_CheckDataType.getString(anyString: txtFldEmailAddress.text as AnyObject),
            "mobile" : phone,
            "school_name" : RV_CheckDataType.getString(anyString: txtFldSchool.text as AnyObject),
            "fav_book" : RV_CheckDataType.getString(anyString: txtFldBook.text as AnyObject),
            "apnstoken" : AppTheme.sharedInstance.strDeviceToken,
            "gender" : strSelectedGender,
            "lang": lang as AnyObject,
            "device_type" : "I",
            "ip_address" : ""]
        //RV_CheckDataType.getString(anyString: txtFldPhone.text as AnyObject)
        
        WebAPI.httpJSONRequest(viewController: self, url: APPURL.UserRegistration, params: params as [String : AnyObject], SuccessHandler: { (jsonResponse) in
            let Status = jsonResponse.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
                    let verificationCodeVC = self.storyboard?.instantiateViewController(withIdentifier: "VerificationCodeVC") as! VerificationCodeVC
                    verificationCodeVC.strMobileNumber = RV_CheckDataType.getString(anyString: self.txtFldPhone.text as AnyObject)
                    verificationCodeVC.genderSelected = self.genderSelected
                    self.navigationController?.pushViewController(verificationCodeVC, animated: false)
                }
            }else{
                
                if jsonResponse.dictionary!["msgCode"]! == 404 {
                    let dict  =  jsonResponse.dictionary!["responseData"]
                    let alert = UIAlertController(title: Constant.ALERT_TITLE, message: ManageLocalization.getLocalaizedString(key: "alert.resendVerificationemail"), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "alert.ok".localized, style: .cancel, handler: nil))
                    alert.addAction(UIAlertAction.init(title: "alert.resendEmail".localized, style: .default, handler: { (action) in
                        self.resendEmail(email_Token: dict!["email_token"].stringValue)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }else {
                    Manager.sharedInstance.showAlert(self, message: jsonResponse.dictionary!["msg"]!.stringValue)
                }
            }
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
    
    func resendEmail(email_Token : String){
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"
        let params: [String : Any] = [
            "email" : RV_CheckDataType.getString(anyString: txtFldEmailAddress.text as AnyObject),
            "lang": lang as AnyObject,
            "email_token" : email_Token]
        
        WebAPI.httpJSONRequest(viewController: self, url: APPURL.resendVerificationEmail, params: params as [String : AnyObject], SuccessHandler: { (jsonResponse) in
            // change 2 to desired number of seconds
            
            let Status = jsonResponse.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                for textfield in self.arrtextField {
                    textfield.text = ""
                }
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: jsonResponse.dictionary!["msg"]?.stringValue as AnyObject) )
                
            }else{
                
                    Manager.sharedInstance.showAlert(self, message: jsonResponse.dictionary!["msg"]!.stringValue)
            }
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
    

}

extension SignUpVC {
    private func loadCountryJsonList() {
        guard let path = Bundle.main.path(forResource: "CountryCode", ofType:"json") else {
            debugPrint( "path not found")
            return
        }
        do{
            let data = try Data(contentsOf: URL(fileURLWithPath: path))
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            if let object = json as? [String : Any]
            {
                print(object)
            }
            else if let Anyjson = json as? [Any]
            {
                for (_,dict) in Anyjson.enumerated(){
                    
                    let dictItem = dict as! NSDictionary
                    
                    let countryFlag = "\(dictItem.object(forKey: "flag") ?? "")"
                    let countryCode = "\(dictItem.object(forKey: "dial_code") ?? "")"
                    let countryName = "\(dictItem.object(forKey: "name") ?? "")"
                    let countryNameWithCode = "\(dictItem.object(forKey: "code") ?? "")"
                    let maxPhoneLength = dictItem.object(forKey: "max_phoneLength") as! Int
                    arrCountryList.append(CountryCodeModelClass(countryFlag: countryFlag, countryCode: countryCode, countryName: countryName, countryNameWithCode: countryNameWithCode, max_phoneLength: maxPhoneLength))
                    for i in 0..<arrCountryList.count {
                        if arrCountryList[i].countryCode == "+34" {
                            self.lblCountryCode.text = "\(arrCountryList[i].countryFlag) \(arrCountryList[i].countryCode)"
                            self.selectedCountryCodeInfo.countryCode = arrCountryList[i].countryCode
                        }
                    }
                }
            }
            else
            {
                print("error in formating")
            }
            
        } catch {
            print(error.localizedDescription)
        }
    }
}
