//
//  ForgotPasswordVC.swift
//  ClubBooks
//
//  Created by cis on 21/11/17.
//  Copyright © 2017 CIS. All rights reserved.
//

import UIKit
import MBProgressHUD
class ForgotPasswordVC: BaseViewController {
    @IBOutlet var txtFldEmailAddress : UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    @IBAction func btnResetPasswordClicked( _ sender : UIButton){
        if !Manager.sharedInstance.emailAddressValidation(txtFldEmailAddress.text!) {
            Manager.sharedInstance.showAlert(self, message: ManageLocalization.getLocalaizedString(key: "Alert.EmailAddressRequired"))
            return
        }else{
            resetPassword()
        }
    }
    @IBAction func btnBack(sender : UIButton)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    func resetPassword(){
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"
        let params: [String : Any] = [
            "email" : RV_CheckDataType.getString(anyString: txtFldEmailAddress?.text as AnyObject),
            "lang": lang as AnyObject]
        
        WebAPI.httpJSONRequest(viewController: self, url: APPURL.ForgotPasswordProfile, params: params as [String : AnyObject], SuccessHandler: { (jsonResponse) in
            // change 2 to desired number of seconds
            let when = DispatchTime.now() + 0.0
            
            let Status = jsonResponse.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                    DispatchQueue.main.asyncAfter(deadline: when) {
                    
                    
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: jsonResponse.dictionary!["msg"]?.stringValue as AnyObject) )
                
            }
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
}
