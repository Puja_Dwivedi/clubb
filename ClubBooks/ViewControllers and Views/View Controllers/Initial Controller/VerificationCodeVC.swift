//
//  VerificationCodeVC.swift
//  ClubBooks
//
//  Created by cis on 11/07/22.
//  Copyright © 2022 CIS. All rights reserved.
//

import UIKit

class VerificationCodeVC: BaseViewController {
    @IBOutlet var txtFldVerificationCode : UITextField!
    var strMobileNumber : String?
    var genderSelected: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        print("Mobile Number is \(strMobileNumber!)")
        // Do any additional setup after loading the view.
    }
    @IBAction func btnBack(sender : UIButton)
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmitClicked(sender : UIButton){
        txtFldVerificationCode.resignFirstResponder()
        if txtFldVerificationCode.text?.replacingOccurrences(of: " ", with: "").count == 0 {
            Manager.sharedInstance.showAlert(self, message: ManageLocalization.getLocalaizedString(key: "alert.VerificationCodeRequired"))
            return
        }
        verifyMobileNumber()
    }
    
    func verifyMobileNumber(){
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"
        let params: [String : Any] = [
        "mobile" : strMobileNumber ?? "",
        "lang": lang as AnyObject,
        "verification_code" : RV_CheckDataType.getString(anyString: txtFldVerificationCode?.text as AnyObject)]
        WebAPI.httpJSONRequest(viewController: self, url: APPURL.VerificationCode, params: params as [String : AnyObject], SuccessHandler: { (jsonResponse) in
            // change 2 to desired number of seconds
            let when = DispatchTime.now() + 0.0

            let Status = jsonResponse.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{

                if AppTheme.sharedInstance.currentUserData.count > 0
                {
                    AppTheme.sharedInstance.currentUserData[0] = UserDetails.init(userDict: jsonResponse)
                }
                else
                {
                    AppTheme.sharedInstance.currentUserData.append(UserDetails.init(userDict: jsonResponse))
                }
                AppTheme.sharedInstance.ClubBooksUserDefaults.synchronize()
                AppTheme.sharedInstance.saveUserDataToDefault()
                let currntUser = AppTheme.sharedInstance.currentUserData[0]
                HSRealTimeMessagingLIbrary.function_CreatAndUpdateUser(HSUser.init(isName: currntUser.Name, gender: UInt64(self.genderSelected ?? 0), isEmail: currntUser.email, isID: UInt64(currntUser.UserID) ?? 0, isPicture: "", isFCMToken: AppTheme.sharedInstance.fcmToken))
                DispatchQueue.main.asyncAfter(deadline: when) {
                    
                    let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "LYTabBar") as! LYTabBar
                    self.navigationController?.pushViewController(homeVC, animated: false)
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: jsonResponse.dictionary!["msg"]?.stringValue as AnyObject))
            }
        }) { (error) in
             
             Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
}
