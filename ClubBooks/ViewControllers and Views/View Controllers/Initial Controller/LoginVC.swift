//
//  LoginVC.swift
//  ClubBooks
//
//  Created by cis on 21/11/17.
//  Copyright © 2017 CIS. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import MBProgressHUD
import GeoFire
class LoginVC: BaseViewController, UITextFieldDelegate, CLLocationManagerDelegate{
    @IBOutlet var btnLogin : UIButton!
    @IBOutlet var txtFldEmailAddress : UITextField!
    @IBOutlet var txtFldPassword : UITextField!
    @IBOutlet var btnForgetPassword : UIButton!
    @IBOutlet var btnSignup : UIButton!
    @IBOutlet var btnFaceBookLogin : UIButton!
    @IBOutlet var btnGoogleLogin : UIButton!
//    var geoFireRef: DatabaseReference?
    var geoFire: GeoFire?
    
    override func viewDidLoad() {
        super.viewDidLoad()
            txtFldEmailAddress.superview?.addShadowAround(layer: (txtFldEmailAddress.superview?.layer)!)
            txtFldPassword.superview?.addShadowAround(layer: (txtFldPassword.superview?.layer)!)
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        let attributeFacebook = NSMutableAttributedString(string: "")
        var fbAttr = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: (btnFaceBookLogin?.titleLabel?.font.pointSize)!), NSAttributedString.Key.foregroundColor : UIColor.white
            ] as [NSAttributedString.Key : Any]
        let buttonFacebookStr = NSMutableAttributedString(string: ManageLocalization.getLocalaizedString(key: "btn,signin"), attributes:fbAttr)
        fbAttr = [
            NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: (btnFaceBookLogin?.titleLabel?.font.pointSize)!+1),
            NSAttributedString.Key.foregroundColor : UIColor.white,] as [NSAttributedString.Key : Any]
        let buttonFacebookStr1 = NSMutableAttributedString(string: ManageLocalization.getLocalaizedString(key: "btn.facebook"), attributes:fbAttr)
        
        attributeFacebook.setAttributedString(buttonFacebookStr)
        attributeFacebook.append(buttonFacebookStr1)
        btnFaceBookLogin?.setAttributedTitle(attributeFacebook, for: .normal)
        
        let attributedString1 = NSMutableAttributedString(string:"")
        var attrs1 = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: (btnSignup?.titleLabel?.font.pointSize)!),
            NSAttributedString.Key.foregroundColor : btnSignup.titleLabel?.textColor!
            ] as [NSAttributedString.Key : Any]
        let buttonTitleStr1 = NSMutableAttributedString(string: "btn.Title.Signup".localized, attributes:attrs1)
        attrs1 = [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: (btnSignup?.titleLabel?.font.pointSize)!+1),
            NSAttributedString.Key.foregroundColor : UIColor.init(red: 80/255.0, green: 211/255.0, blue: 226/255.0, alpha: 1.0),
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue
            ] as [NSAttributedString.Key : Any]
        let buttonTitleStr2 = NSMutableAttributedString(string: "btn.Title.ClickHere".localized, attributes:attrs1)
        attributedString1.setAttributedString(buttonTitleStr1)
        attributedString1.append(buttonTitleStr2)
        btnSignup?.setAttributedTitle(attributedString1, for: .normal)
    }
    override func viewDidAppear(_ animated: Bool) {
      
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   

    
    
    @IBAction func btnSignUp(sender : UIButton)
    {
        let SignUp : SignUpVC = storyBoard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(SignUp, animated: true)
    }
    
    @IBAction func btnForgotPassword(sender : UIButton)
    {
        let ForgotPassword : ForgotPasswordVC = storyBoard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        self.navigationController?.pushViewController(ForgotPassword, animated: true)
    }
    
    //MARK: Email Login Methods
    @IBAction func btnLoginClicked( _ sender : UIButton){
        if !Manager.sharedInstance.emailAddressValidation(txtFldEmailAddress.text!) {
            Manager.sharedInstance.showAlert(self, message: ManageLocalization.getLocalaizedString(key: "Alert.EmailAddressRequired"))
            return
        }
        let lang = (AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as? String) ?? "en"
        let params: [String : Any] = [
        "email" : RV_CheckDataType.getString(anyString: txtFldEmailAddress?.text as AnyObject),
        "password" : RV_CheckDataType.getString(anyString: txtFldPassword?.text as AnyObject),
        "device_token" : AppTheme.sharedInstance.strDeviceToken,
        "lang": lang as AnyObject,
        "is_firebase_login" : "1"]

        WebAPI.httpJSONRequest(viewController: self, url: APPURL.Login, params: params as [String : AnyObject], SuccessHandler: { (jsonResponse) in
            // change 2 to desired number of seconds
            let when = DispatchTime.now() + 0.0

            let Status = jsonResponse.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{

                if AppTheme.sharedInstance.currentUserData.count > 0
                {
                    AppTheme.sharedInstance.currentUserData[0] = UserDetails.init(userDict: jsonResponse)
                }
                else
                {
                    AppTheme.sharedInstance.currentUserData.append(UserDetails.init(userDict: jsonResponse))
                }
                var numberofappLogins = RV_CheckDataType.getInteger(anyString: AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: "numberOfLogins") as AnyObject)
                if numberofappLogins == 0{
                    AppTheme.sharedInstance.ClubBooksUserDefaults.set(1, forKey: "numberOfLogins")
                }else{
                    numberofappLogins = numberofappLogins + 1
                    AppTheme.sharedInstance.ClubBooksUserDefaults.set(numberofappLogins, forKey: "numberOfLogins")
                }
                AppTheme.sharedInstance.ClubBooksUserDefaults.synchronize()
                AppTheme.sharedInstance.saveUserDataToDefault()
                let currntUser = AppTheme.sharedInstance.currentUserData[0]
                var genderValue = 0
                if let gender = currntUser.Gender {
                    genderValue = Int(gender) ?? 0
                }
                HSRealTimeMessagingLIbrary.function_CreatAndUpdateUser(HSUser.init(isName: currntUser.Name, gender: UInt64(genderValue), isEmail: currntUser.email, isID: UInt64(currntUser.UserID) ?? 0, isPicture: "", isFCMToken: AppTheme.sharedInstance.fcmToken))
                DispatchQueue.main.asyncAfter(deadline: when) {
                    
                    let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "LYTabBar") as! LYTabBar
                    self.navigationController?.pushViewController(homeVC, animated: false)
                }
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: jsonResponse.dictionary!["msg"]?.stringValue as AnyObject))
            }
        }) { (error) in
             
             Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
    
    //MARK: FaceBook Login Methods
    @IBAction func btnFaceBookLoginClicked(_ sender : UIButton){
        self.view.endEditing(true)
        let facebookReadPermissions = ["public_profile", "email"]
        let loginManager = LoginManager()
        loginManager.logIn(permissions: facebookReadPermissions, from: self) { (result, error) in
            
            if error != nil {
                //According to Facebook:
                //Errors will rarely occur in the typical login flow because the login dialog
                //presented by Facebook via single sign on will guide the users to resolve any errors.
                
                // Process error
                LoginManager().logOut()
            } else if (result?.isCancelled)! {
                // Handle cancellations
                LoginManager().logOut()
            } else {
                // If you ask for multiple permissions at once, you
                // should check if specific permissions missing
                var allPermsGranted = true
                
                for permission in facebookReadPermissions {
                    if !(result?.grantedPermissions.contains(permission))! {
                        allPermsGranted = true
                        break
                    }
                }
                
                if allPermsGranted {
                    self.fetchProfile()
                    
                } else {
                }
            }
        }
       
    }
    func fetchProfile() {
        

        let parameters = ["fields": "id, name, first_name, last_name, age_range, link, gender, locale, timezone, picture, updated_time, verified, email"]
        
        //        
        GraphRequest(graphPath: "me", parameters: parameters).start(completionHandler: { (connection, user, requestError) -> Void in
            
            DispatchQueue.main.async( execute: {
                
                
                if requestError != nil {
                    
                    return
                }
                
                print(user as Any)
                let userData = user as! [String :Any]
                var dictionaryFB: [String: Any] = [:]
                
                dictionaryFB["name"] = String.init(format: "%@ %@", userData["first_name"] as! String,userData["last_name"] as! String)
                dictionaryFB["id"] = userData["id"]
                if let strMail = userData["email"] as? String {
                    dictionaryFB["provider"] = "FACEBOOK"
                    dictionaryFB["email"] = strMail
                    dictionaryFB ["device_token"] = AppTheme.sharedInstance.strDeviceToken
                    dictionaryFB ["device_type"] = Constant.DEVICE
                    dictionaryFB["photoUrl"] = userData["picture"] as? String
                    
                    //Calling LoginWithFB WS
                    self.callSocialLoginService(forUrl: APPURL.FacebookLogin, dictioary: dictionaryFB)
                }
            })
        })
    }
    func callSocialLoginService(forUrl: String, dictioary : [String : Any]){
        
        
        WebAPI.httpJSONRequest(viewController: self, url: forUrl, params: dictioary as [String : AnyObject], SuccessHandler: { (jsonResponse) in
            // change 2 to desired number of seconds
            let when = DispatchTime.now() + 2.1
            
            let Status = jsonResponse.dictionary!["status"]?.boolValue
            if RV_CheckDataType.getBoolValue(obj: Status as AnyObject) == true{
                
                if AppTheme.sharedInstance.currentUserData.count > 0
                {
                    AppTheme.sharedInstance.currentUserData[0] = UserDetails.init(userDict: jsonResponse)
                }
                else
                {
                    AppTheme.sharedInstance.currentUserData.append(UserDetails.init(userDict: jsonResponse))
                }
                AppTheme.sharedInstance.saveUserDataToDefault()
                let currntUser = AppTheme.sharedInstance.currentUserData[0]
                HSRealTimeMessagingLIbrary.function_CreatAndUpdateUser(HSUser.init(isName: currntUser.Name, gender: UInt64(currntUser.Gender) ?? 0, isEmail: currntUser.email, isID: UInt64(currntUser.UserID) ?? 0, isPicture: "", isFCMToken: AppTheme.sharedInstance.fcmToken))
                DispatchQueue.main.asyncAfter(deadline: when) {
                    let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "LYTabBar") as! LYTabBar
                    self.navigationController?.pushViewController(homeVC, animated: false)
                }
                
            }else{
                
                Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: jsonResponse.dictionary!["message"]?.stringValue as AnyObject) )
                
            }
        }) { (error) in
            
            Manager.sharedInstance.showAlert(self, message: RV_CheckDataType.getString(anyString: error.localizedDescription as AnyObject))
        }
    }
}
