//
//  IntroViewController.swift
//  ClubBooks
//
//  Created by cis on 20/12/18.
//  Copyright © 2018 CIS. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController {
    @IBOutlet var btnRegister : UIButton!
    @IBOutlet var btnSignin : UIButton!
    @IBOutlet var segLanguage : UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.addShadowAroundBtnRegister(layer: btnRegister.layer)
        btnRegister.backgroundColor = UIColor.white
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        if ManageLocalization.deviceLang == "en"{
            segLanguage.selectedSegmentIndex = 0
        }else{
            segLanguage.selectedSegmentIndex = 1
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        ManageLocalization.deviceLang == "en" ? (AppDelegate.shared.langId = LangId.english) : (AppDelegate.shared.langId = LangId.spanish)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setTextForSelectedLanguage(){
        btnRegister.setTitle("btn.register".localized, for: .normal)
        btnSignin.setTitle("btn.login".localized, for: .normal)
        self.segLanguage.setTitle("lbl.english".localized, forSegmentAt: 0)
        self.segLanguage.setTitle("lbl.spanish".localized, forSegmentAt: 1)
    }
    @IBAction func btnLoginClicked(_ sender : UIButton){
        let Login : LoginVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(Login, animated: true)
    }
    @IBAction func btnRegisterClicked(_ sender : UIButton){
        let SignUp : SignUpVC = storyBoard.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(SignUp, animated: true)
    }
    @IBAction func SegmentedControllerLanguage(_ sender: UISegmentedControl) {
       var selectedLanguage = ""
        switch sender.selectedSegmentIndex {
        case 0:
          selectedLanguage = "en"
        case 1:
           selectedLanguage = "es"
        default:
            return
        }
        if ManageLocalization.deviceLang != selectedLanguage{
            let alert : UIAlertController = UIAlertController(title: "ClubBooks", message: "alert.title.changeLanguage".localized, preferredStyle: .alert)
            
            let YesAction = UIAlertAction(title: "alert.ok".localized, style: .default, handler: { (action) in
                ManageLocalization.deviceLang = selectedLanguage
                AppTheme.sharedInstance.ClubBooksUserDefaults.setValue(selectedLanguage, forKey: KEY.UserDefaults.selectedLanguage)
                AppTheme.sharedInstance.ClubBooksUserDefaults.synchronize()
                self.setTextForSelectedLanguage()
                
            })
            let CancelAction = UIAlertAction.init(title: "alert.cancel".localized, style: .destructive) { (action) in
                if selectedLanguage == "en"{
                    selectedLanguage = "es"
                    self.segLanguage.selectedSegmentIndex = 1
                }else{
                    selectedLanguage = "en"
                    self.segLanguage.selectedSegmentIndex = 0
                }
            }
            alert.addAction(YesAction)
            alert.addAction(CancelAction)
            self.present(alert, animated: true, completion: {
            })
        }
    }
    func addShadowAroundBtnRegister(layer : CALayer)
    {
        let shadowSize : CGFloat = 5.0
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                   y: -shadowSize / 2,
                                                   width: layer.frame.size.width + (shadowSize * 2),
                                                   height: layer.frame.size.height + shadowSize))
        layer.masksToBounds = false
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        layer.shadowOpacity = 0.3
        layer.shadowPath = shadowPath.cgPath
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
