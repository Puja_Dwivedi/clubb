//
//  DropdownCell.swift
//  ClikaGram
//
//  Created by cis on 30/01/18.
//  Copyright © 2018 cis. All rights reserved.
//

import UIKit
class DropdownCell: UITableViewCell {
    @IBOutlet var lblSuffix : UILabel!
    @IBOutlet var imgViewUser : UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
