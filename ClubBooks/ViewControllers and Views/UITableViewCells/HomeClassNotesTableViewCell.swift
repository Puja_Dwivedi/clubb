//
//  HomeClassNotesTableViewCell.swift
//  ClubBooks
//
//  Created by cis on 02/03/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class HomeClassNotesTableViewCell: UITableViewCell {
    @IBOutlet weak var imgViewBookCover: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblProfessor: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblSchool: UILabel!
    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var lblCourse: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func ConfigureNotesCell(notesDetails : PostDetails){
        lblTitle.text = notesDetails.postTitle
        lblPrice.text = "$ " + notesDetails.postPrice
        self.lblProfessor.getAttibutedStringWithTitle(txtTitle: "lbl.title.professor", txtText: notesDetails.postProfessor)
        self.lblCity.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailCity", txtText: notesDetails.postCity)
        self.lblSchool.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailSchool", txtText: notesDetails.postSchool)
        self.lblDepartment.getAttibutedStringWithTitle(txtTitle: "lbl.title.department", txtText: notesDetails.postDepartment)
        self.lblCourse.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailCourse#", txtText: notesDetails.postCourse)
        imgViewBookCover.image = #imageLiteral(resourceName: "class_notes")
        if notesDetails.postDate != ""{
            lblTime.text = AppTheme.sharedInstance.getTimeDifferencefromDate(strDate: notesDetails.postDate)
        }
        
    }
}
