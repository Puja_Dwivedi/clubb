//
//  HomeMeetUpsCell.swift
//  ClubBooks
//
//  Created by cis on 09/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

protocol HomeMeetUpsCellDelegate {
    func openUserProfile(userID: String, username: String)
}

class HomeMeetUpsCell: UITableViewCell {

    @IBOutlet weak var imgViewMeetup: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMeetupCreator: UILabel!
    @IBOutlet weak var lblNote: UILabel!
    @IBOutlet weak var lblMeetUpTime: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblMeetup: UILabel!
    
    var userID = String()
    var delegate: HomeMeetUpsCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        lblMeetupCreator.isUserInteractionEnabled = true
        lblMeetupCreator.addGestureRecognizer(tap)
    }
    
    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        if !lblMeetupCreator.text!.isEmpty {
            delegate?.openUserProfile(userID: userID, username: lblMeetupCreator.text!)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func ConfigureMeetUpsCell(meetUpsDetails : PostDetails){
        userID = meetUpsDetails.userid
        lblTitle.text = meetUpsDetails.postTitle
        if meetUpsDetails.postIsAnnonymous == "true" || meetUpsDetails.postIsAnnonymous == "1"{
            lblMeetupCreator.text = ""
        }else{
            lblMeetupCreator.text = meetUpsDetails.postCreator
        }
        lblMeetup.text = "lbl.meetups".localized.lowercased().capitalizingFirstLetter()
        lblNote.text = meetUpsDetails.postMeetUpComment
        lblMeetUpTime.text = "\(AppTheme.sharedInstance.getFormatedDateForMeetup(strDate: meetUpsDetails.postDate)) \n \(meetUpsDetails.postMeetupTime)"
        if meetUpsDetails.postDate != ""{
            lblTime.text = AppTheme.sharedInstance.getTimeDifferencefromDate(strDate: meetUpsDetails.postDate)
        }
    }
}
