//
//  FriendCell.swift
//  ClubBooks
//
//  Created by cis on 24/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class FriendCell: UITableViewCell {

    @IBOutlet var lblUserName : UILabel!
    @IBOutlet var imgUserPic : UIImageView!
    @IBOutlet var lblDetail : UILabel!
    @IBOutlet var btnAccept : UIButton!
    @IBOutlet var btnReject : UIButton!
    @IBOutlet var viewBg : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewBg.addShadowAround(layer: self.viewBg.layer)
        self.viewBg.layer.borderColor = UIColor.lightGray.cgColor
        self.viewBg.layer.borderWidth = 0.2
        btnAccept.setTitle("seg.acceptTitile".localized, for: .normal)
        btnReject.setTitle("seg.rejectTitile".localized, for: .normal)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
