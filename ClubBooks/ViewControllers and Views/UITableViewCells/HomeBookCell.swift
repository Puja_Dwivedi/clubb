//
//  HomeBookCell.swift
//  ClubBooks
//
//  Created by cis on 05/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit
import SDWebImage
class HomeBookCell: UITableViewCell {

    @IBOutlet weak var imgViewBookCover: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAuthor: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblSchool: UILabel!
    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var lblCourse: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblReserved : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func ConfigureBookCell(bookDetails : PostDetails){
        lblTitle.text = bookDetails.postTitle
        lblPrice.text = "$ " + bookDetails.postPrice
        self.lblAuthor.getAttibutedStringWithTitle(txtTitle: "lbl.title.author", txtText: bookDetails.postAuthor)
        self.lblCity.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailCity", txtText: bookDetails.postCity)
        self.lblSchool.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailSchool", txtText: bookDetails.postSchool)
        self.lblDepartment.getAttibutedStringWithTitle(txtTitle: "lbl.title.department", txtText: bookDetails.postDepartment)
        self.lblCourse.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailCourse#", txtText: bookDetails.postCourse)
        imgViewBookCover.sd_setImage(with: URL.init(string: bookDetails.postPostImages), placeholderImage: #imageLiteral(resourceName: "btn_loginbackground"), options: [], completed: nil)
        if RV_CheckDataType.getBoolValue(obj: bookDetails.isReserved as AnyObject){
            lblReserved.text = "lbl.title.reserved".localized
            lblReserved.textColor = UIColor.red
        }else{
            lblReserved.text = "lbl.title.forsale".localized
            lblReserved.textColor = UIColor.init(red: 98/255.0, green: 162/255.0, blue: 89/255.0, alpha: 1.0)
        }
        if bookDetails.postDate != ""{
            lblTime.text = AppTheme.sharedInstance.getTimeDifferencefromDate(strDate: bookDetails.postDate)
        }
    }
    
}
