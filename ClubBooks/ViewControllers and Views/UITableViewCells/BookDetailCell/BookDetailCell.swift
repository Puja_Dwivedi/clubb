//
//  BookDetailCell.swift
//  ClubBooks
//
//  Created by cis on 28/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class BookDetailCell: UITableViewCell {

    @IBOutlet var lblTime : UILabel!
    @IBOutlet var lblUsername : UILabel!
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblCourse : UILabel!
    @IBOutlet var lblAuthor : UILabel!
    @IBOutlet var lblHashVal : UILabel!
    @IBOutlet var lblPrice : UILabel!
    @IBOutlet var lblISBN : UILabel!
    @IBOutlet var lblEdition : UILabel!
    @IBOutlet var lblCondition : UILabel!
    @IBOutlet var lblSchool : UILabel!
    @IBOutlet var lblCity : UILabel!
    @IBOutlet var imgPic : UIImageView!
    @IBOutlet var viewBackGround : UIView!
    @IBOutlet var lblReserved : UILabel!
    @IBOutlet var lblProfessor : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        
    }
    
    func configureBasic(BookDetails : SelectedPostDetail){
        self.imgPic.sd_setImage(with: URL.init(string: BookDetails.BookImage), placeholderImage: #imageLiteral(resourceName: "btn_loginbackground"), options: [], completed: nil)
        self.lblTitle.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailTitle", txtText: BookDetails.BookTitle)
        self.lblAuthor.getAttibutedStringWithTitle(txtTitle: "lbl.title.author", txtText: BookDetails.BookAuthor)
        self.lblPrice.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailPrice", txtText: BookDetails.BookPrice)
        self.lblHashVal.getAttibutedStringWithTitle(txtTitle: "lbl.title.department", txtText: BookDetails.BookDepartment)
        self.lblCourse.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailCourse#", txtText: BookDetails.BookCourse)
        self.lblEdition.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailEdition", txtText: BookDetails.BookEdition)
        self.lblISBN.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailISBN", txtText: BookDetails.BookISBN)
        self.lblCondition.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailCondition", txtText: BookDetails.BookCondition.localized)
        self.lblSchool.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailSchool", txtText: BookDetails.BookSchool)
        self.lblCity.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailCity", txtText: BookDetails.BookCity)
        self.lblProfessor.getAttibutedStringWithTitle(txtTitle: "lbl.title.professor", txtText: BookDetails.BookProfessor)
        self.lblUsername.text = BookDetails.BookCreator
        viewBackGround.addShadowAround(layer: viewBackGround.layer)
        if RV_CheckDataType.getBoolValue(obj: BookDetails.isReserved as AnyObject){
            lblReserved.text = "lbl.title.reserved".localized
            lblReserved.textColor = UIColor.red
        }else{
            lblReserved.text = "lbl.title.forsale".localized
            lblReserved.textColor = UIColor.init(red: 98/255.0, green: 162/255.0, blue: 89/255.0, alpha: 1.0)
        }
    }
}
