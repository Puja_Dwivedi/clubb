//
//  ChatSenderCell.swift
//  ClubBooks
//
//  Created by cis on 18/04/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

protocol ChatSenderCellDelegate {
    func openUserProfile(userID: String, username: String)
}

class ChatSenderCell: UITableViewCell {
    @IBOutlet weak var profilePic: RoundedImageView!
    @IBOutlet weak var message: UITextView!
    @IBOutlet weak var messageBackground: UIImageView!
    @IBOutlet weak var lblMessageDate : UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    
    var delegate: ChatReceivedCellDelegate?
    var userID = String()
    
    func clearCellData()  {
        self.message.text = nil
        self.message.isHidden = false
        self.messageBackground.image = nil
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.selectionStyle = .none
        clearCellData()
        messageBackground.layer.cornerRadius = 10.0
//        self.functionAddGradient()
//        setImage("chat_bubble_sent")
        
//        messageBackground.ima
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        lblUsername.isUserInteractionEnabled = true
        lblUsername.addGestureRecognizer(tap)
    }
    
    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        delegate?.openUserProfile(userID: userID, username: lblUsername.text!)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setImage(_ name: String) {
        guard let image = UIImage(named: name) else { return }
        messageBackground.image = image
            .resizableImage(withCapInsets:
                UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21),
                            resizingMode: .stretch)
            .withRenderingMode(.alwaysTemplate)
    }
    func functionAddGradient() {
        DispatchQueue.main.async {
            if let isLayer = self.messageBackground.layer.sublayers, isLayer[0] is CAGradientLayer {isLayer[0].removeFromSuperlayer()}
            let gradient = CAGradientLayer()
            gradient.frame = self.messageBackground.bounds
            gradient.colors = [UIColor.init(red: 65/255, green: 196/255, blue: 221/255, alpha: 1.0).cgColor, UIColor.init(red: 10/255, green: 124/255, blue: 175/255, alpha: 1.0).cgColor]
            self.messageBackground.layer.insertSublayer(gradient, at: 0)
        }
    }
}
