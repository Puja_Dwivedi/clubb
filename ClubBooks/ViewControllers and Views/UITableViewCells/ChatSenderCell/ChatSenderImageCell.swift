//
//  ChatSenderImageCell.swift
//  ClubBooks
//
//  Created by cis on 23/03/23.
//  Copyright © 2023 CIS. All rights reserved.
//

import UIKit

class ChatSenderImageCell: UITableViewCell {
    @IBOutlet weak var imgChatMessage : UIImageView?
    @IBOutlet weak var lblMessageDate : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
