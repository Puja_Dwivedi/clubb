//
//  SellerInfoCell.swift
//  ClubBooks
//
//  Created by cis on 28/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class SellerInfoCell: UITableViewCell {
    @IBOutlet var btnName : UIButton!
    @IBOutlet var btnCancel : UIButton!
    @IBOutlet var btnPayforPost : UIButton!
    @IBOutlet var lblUsername : UILabel!
    @IBOutlet var lblEmail : UILabel!
    @IBOutlet var lblLocation : UILabel!
    @IBOutlet var btnPaySeller : UIButton!
    @IBOutlet var viewBackground : UIView!
    @IBOutlet var viewReserveBookOptions : UIView!
    @IBOutlet var viewAnonynmousSellerInfo : UIView!
    @IBOutlet var viewRating : CosmosView!
    @IBOutlet var viewReserveBookOptionsHeight : NSLayoutConstraint!
    @IBOutlet var viewSellerInfoHeight : NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewRating.fillMode = 2
//        viewBackground.addShadowAround(layer: viewBackground.layer)

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    func configureSellerInfo(Seller : SelectedPostDetail, isForBookDetails : Bool){
        viewBackground.addShadowAround(layer: viewBackground.layer)
      
        viewRating.rating = RV_CheckDataType.getDouble(anyString: Seller.sellerRating as AnyObject)
        if (Seller.BookIsAnnonymous == "true" || Seller.BookIsAnnonymous == "1") && !isForBookDetails{
            btnName.setTitle("lbl.anonymous.post".localized, for: .normal)
            lblUsername.text = ""
//            btnName.isUnderLine = false
            btnName.setTitleColor(.black, for: .normal)
            self.viewSellerInfoHeight.constant = 0.0
        }else{
            btnName.setTitle(Seller.BookCreator, for: .normal)
            lblUsername.text = Seller.UserName
            btnName.isUnderLine = true
            btnName.setTitleColor(.blue, for: .normal)
            lblLocation.text = Seller.BookMeetUpLocation
            lblEmail.text = Seller.UserEmail
            if isForBookDetails{
                self.removeConstraint(viewSellerInfoHeight)
            }else{
                self.viewSellerInfoHeight.constant = 0.0
            }
        }
        self.layoutIfNeeded()
        self.layoutSubviews()
        if Seller.Userid == AppTheme.sharedInstance.currentUserData[0].UserID{
            self.btnPaySeller.isHidden = true
        }else{
            self.btnPaySeller.isHidden = false
        }
    }
}
