//
//  ChatReceiverCell.swift
//  ClubBooks
//
//  Created by cis on 17/04/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

protocol ChatReceivedCellDelegate {
    func openUserProfile(userID: String, username: String)
}

class ChatReceivedCell: UITableViewCell {
    @IBOutlet weak var profilePic: RoundedImageView!
    @IBOutlet weak var message: UITextView!
    @IBOutlet weak var messageBackground: UIImageView!
    @IBOutlet weak var lblUserName : UILabel!
    @IBOutlet weak var lblMessageDate : UILabel!
    
    var delegate: ChatReceivedCellDelegate?
    
    var userID = String()
    
    func clearCellData()  {
        self.message.text = nil
        self.message.isHidden = false
        self.messageBackground.image = nil
        self.lblUserName.text = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        clearCellData()
//        setImage("chat_bubble_received")
        messageBackground.backgroundColor = .white
        messageBackground.layer.cornerRadius = 10.0
        messageBackground.clipsToBounds = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        lblUserName.isUserInteractionEnabled = true
        lblUserName.addGestureRecognizer(tap)
        
    }
    
    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        delegate?.openUserProfile(userID: userID, username: lblUserName.text!)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setImage(_ name: String) {
        guard let image = UIImage(named: name) else { return }
        messageBackground.image = image
            .resizableImage(withCapInsets:
                UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0),
                            resizingMode: .stretch)
            .withRenderingMode(.alwaysTemplate)
    }
}
