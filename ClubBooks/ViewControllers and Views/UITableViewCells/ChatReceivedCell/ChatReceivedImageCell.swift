//
//  ChatReceivedImageCell.swift
//  ClubBooks
//
//  Created by cis on 22/03/23.
//  Copyright © 2023 CIS. All rights reserved.
//

import UIKit

class ChatReceivedImageCell: UITableViewCell {
    @IBOutlet weak var profilePic: RoundedImageView!
    @IBOutlet weak var imgChatMessage: UIImageView!
    @IBOutlet weak var lblUserName : UILabel!
    @IBOutlet weak var lblMessageDate : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
