//
//  ReserveBookOptionCell.swift
//  ClubBooks
//
//  Created by cis on 13/07/22.
//  Copyright © 2022 CIS. All rights reserved.
//

import UIKit

class ReserveBookOptionCell: UITableViewCell {
    @IBOutlet var viewBackground : UIView!
    @IBOutlet var btnReserveBook : UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBackground.addShadowAround(layer: viewBackground.layer)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
