//
//  MeetupDetailsCell.swift
//  ClubBooks
//
//  Created by cis on 16/11/22.
//  Copyright © 2022 CIS. All rights reserved.
//

import UIKit

protocol MeetupDetailsCellDelegate {
    func openUserProfile(userID: String, username: String)
}

class MeetupDetailsCell: UITableViewCell {
    @IBOutlet weak var imgViewExam: UIImageView!
    @IBOutlet weak var lblAuthorName: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblPlace: UILabel!
    
    var userID = String()
    var delegate: MeetupDetailsCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        lblAuthorName.isUserInteractionEnabled = true
        lblAuthorName.addGestureRecognizer(tap)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // function which is triggered when handleTap is called
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        if !lblAuthorName.text!.isEmpty {
            delegate?.openUserProfile(userID: userID, username: lblAuthorName.text!)
        }
    }
    
    func configureCell(PostDetails : MeetupDetails){
        self.userID = PostDetails.userID
        self.lblDate.getAttibutedStringWithTitle(txtTitle: "lbl.title.date", txtText: AppTheme.sharedInstance.getDateFromString(strDate: PostDetails.meetupDate))
        self.lblTime.getAttibutedStringWithTitle(txtTitle: "lbl.title.time", txtText: PostDetails.meetupTime)
        self.lblComment.getAttibutedStringWithTitle(txtTitle: "lbl.title.Comments", txtText: PostDetails.meetupComment)
        self.lblPlace.getAttibutedStringWithTitle(txtTitle: "lbl.title.place", txtText: PostDetails.meetupPlace)
        self.lblTitle.text = PostDetails.title
        self.lblAuthorName.text = PostDetails.meetupCreator
        self.imgViewExam.image = #imageLiteral(resourceName: "meetups")
    }
    
}
