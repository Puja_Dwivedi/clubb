//
//  NotificationCell.swift
//  ClubBooks
//
//  Created by cis on 24/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet var lblUserName : UILabel!
    @IBOutlet var imgUserPic : UIImageView!
    @IBOutlet var lblDetails : UILabel!
    @IBOutlet var lblDate : UILabel!
    @IBOutlet var viewBg : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
