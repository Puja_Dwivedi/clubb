//
//  HomeQuizCell.swift
//  ClubBooks
//
//  Created by cis on 17/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class HomeQuizCell: UITableViewCell {

    @IBOutlet weak var imgViewQuiz: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblSchool: UILabel!
    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var lblProfessor: UILabel!
    @IBOutlet weak var lblCourse: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func ConfigureQuizCell(quizDetails : PostDetails){
        lblTitle.text = quizDetails.postTitle
        lblPrice.text = "$ " + quizDetails.postPrice
        self.lblCity.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailCity", txtText: quizDetails.postCity)
        self.lblSchool.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailSchool", txtText: quizDetails.postSchool)
        self.lblDepartment.getAttibutedStringWithTitle(txtTitle: "lbl.title.department", txtText: quizDetails.postDepartment)
        self.lblProfessor.getAttibutedStringWithTitle(txtTitle: "lbl.title.professor", txtText: quizDetails.postProfessor)
        self.lblCourse.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailCourse#", txtText: quizDetails.postCourse)
        imgViewQuiz.image = #imageLiteral(resourceName: "quizzes")
        if quizDetails.postDate != ""{
            lblTime.text = AppTheme.sharedInstance.getTimeDifferencefromDate(strDate: quizDetails.postDate)
        }
    }
}
