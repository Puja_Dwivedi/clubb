//
//  ConnectCell.swift
//  ClubBooks
//
//  Created by cis on 25/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class ConnectCell: UITableViewCell {

    @IBOutlet var lblUserName : UILabel!
    @IBOutlet var imgUserPic : UIImageView!
    @IBOutlet var imgUserStatus : UIImageView!
    @IBOutlet var lblDetails : UILabel!
    @IBOutlet var viewBg : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
