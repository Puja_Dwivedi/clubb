//
//  HomeExamCell.swift
//  ClubBooks
//
//  Created by cis on 17/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class HomeExamCell: UITableViewCell {

    @IBOutlet weak var imgViewExam: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblAuthor: UILabel!
    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var lblProfessor: UILabel!
    @IBOutlet weak var lblCourse: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func ConfigureBookCell(examDetails : PostDetails){
        lblTitle.text = examDetails.postTitle
        lblPrice.text = "$ " + examDetails.postPrice
        self.lblAuthor.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailSchool", txtText: examDetails.postSchool)
        self.lblCity.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailCity", txtText: examDetails.postCity)
        self.lblProfessor.getAttibutedStringWithTitle(txtTitle: "lbl.title.professor", txtText: examDetails.postProfessor)
        self.lblDepartment.getAttibutedStringWithTitle(txtTitle: "lbl.title.department", txtText: examDetails.postDepartment)
        self.lblCourse.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailCourse#", txtText: examDetails.postCourse)
        if examDetails.postDate != ""{
            lblTime.text = AppTheme.sharedInstance.getTimeDifferencefromDate(strDate: examDetails.postDate)
        }
    }
}
