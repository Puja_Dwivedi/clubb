//
//  ChatsOnTopicCell.swift
//  ClubBooks
//
//  Created by cis on 11/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class ChatsOnTopicCell: UITableViewCell {

    @IBOutlet weak var imgViewChatOnTopic: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCreator: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet weak var lblFirstUserName : UILabel!
    @IBOutlet weak var lblSecondUserName : UILabel!
    @IBOutlet weak var lblThirdUserName : UILabel!
    
    @IBOutlet weak var lblChatOnTopic: UILabel!
    @IBOutlet weak var lblFirstMessage : UILabel!
    @IBOutlet weak var lblSecondMessage : UILabel!
    @IBOutlet weak var lblThirdMessage : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func ConfigureTopicChatCell(chatDetails : PostDetails){
        let arrlabelMessages = [lblFirstMessage, lblSecondMessage, lblThirdMessage]
        let arrlabelUserName = [lblFirstUserName, lblSecondUserName, lblThirdUserName]
        lblTitle.text = chatDetails.postTitle
        lblCreator.text = chatDetails.postCreator
        lblChatOnTopic.text = "nav.ChatsOnTopics".localized
        imgViewChatOnTopic.sd_setImage(with: URL.init(string: chatDetails.postPostImages), placeholderImage: #imageLiteral(resourceName: "btn_loginbackground"), options: [], completed: nil)
        if chatDetails.postDate != ""{
            lblTime.text = AppTheme.sharedInstance.getTimeDifferencefromDate(strDate: chatDetails.postDate)
        }
        for lbl in arrlabelMessages{
            lbl?.text = ""
        }
        for lbl in arrlabelUserName{
            lbl?.text = ""
        }
        if chatDetails.postIsAnnonymous == "true" || chatDetails.postIsAnnonymous == "1"{
            lblCreator.text = ""
        }else{
            lblCreator.text = chatDetails.postCreator
        }
        if chatDetails.RecentChat.count > 0 {
            for i in 0...chatDetails.RecentChat.count-1 {
                arrlabelMessages[i]?.text = chatDetails.RecentChat[i].ChatMessage
                arrlabelUserName[i]?.text = chatDetails.RecentChat[i].SenderName + " :"
            }
        }
    }
}
