//
//  ProfileBookPostCell.swift
//  ClubBooks
//
//  Created by cis on 18/08/22.
//  Copyright © 2022 CIS. All rights reserved.
//

import UIKit

class ProfileBookPostCell: UITableViewCell {
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblAuthor : UILabel!
    @IBOutlet var lblPrice : UILabel!
    @IBOutlet var lblCity : UILabel!
    @IBOutlet var lblSchool : UILabel!
    @IBOutlet var lblDepartment : UILabel!
    @IBOutlet var lblCourse : UILabel!
    @IBOutlet var lblProfessor : UILabel!
    @IBOutlet var btnCell: UIButton!
    @IBOutlet var lblEdition : UILabel!
    @IBOutlet var lblISBN : UILabel!
    @IBOutlet var lblCondition : UILabel!
    @IBOutlet var btnDelete: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblTitle.textColor = UIColor.init(red: 65/255.0, green: 196/255.0, blue: 221/255.0, alpha: 1.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellwith(post : PostDetails){
        self.lblTitle.text = post.postTitle
        self.lblAuthor.text = post.postAuthor
        self.lblPrice.text = post.postPrice
        self.lblCity.text = post.postCity
        self.lblSchool.text = post.postSchool
        self.lblDepartment.text = post.postDepartment
        self.lblCourse.text = post.postCourse
        self.lblProfessor.text = post.postProfessor
        self.lblEdition.text = post.postEdition
        self.lblISBN.text = post.postISBN
        self.lblCondition.text = post.postPostCondition.localized
    }
    
}
