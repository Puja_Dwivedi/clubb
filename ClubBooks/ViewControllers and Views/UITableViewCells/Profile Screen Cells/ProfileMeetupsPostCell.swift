//
//  ProfileMeetupsPostCell.swift
//  ClubBooks
//
//  Created by cis on 22/08/22.
//  Copyright © 2022 CIS. All rights reserved.
//

import UIKit

class ProfileMeetupsPostCell: UITableViewCell {
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblDate : UILabel!
    @IBOutlet var lblTime : UILabel!
    @IBOutlet var lblPlace : UILabel!
    @IBOutlet var lblComments : UILabel!
    @IBOutlet var btnCell: UIButton!
    @IBOutlet var btnDelete: UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblTitle.textColor = UIColor.init(red: 65/255.0, green: 196/255.0, blue: 221/255.0, alpha: 1.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCellwith(post : PostDetails){
        
        let date = AppTheme.sharedInstance.getDateFromString(strDate: post.postCreatedAt)
        let time = AppTheme.sharedInstance.getTimeFromString(strTime: post.postCreatedAt)
        self.lblTitle.text = post.postTitle
        self.lblDate.text = "\(date)"
        self.lblTime.text = "\(time)"
        self.lblPlace.text = post.postMeetupPlace
        self.lblComments.text = post.postMeetUpComment
    }
}
