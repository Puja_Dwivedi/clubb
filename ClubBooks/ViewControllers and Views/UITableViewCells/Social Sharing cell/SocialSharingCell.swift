//
//  SocialSharingCell.swift
//  ClubBooks
//
//  Created by cis on 20/06/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class SocialSharingCell: UITableViewCell {
    @IBOutlet var btnFaceBook : UIButton!
    @IBOutlet var btnMessanger : UIButton!
    @IBOutlet var btnEmail : UIButton!
    @IBOutlet var btnTwitter : UIButton!
    @IBOutlet var btnInstaGram : UIButton!
    @IBOutlet var btnSnapChat : UIButton!
    @IBOutlet var btnTikTok : UIButton!
    @IBOutlet var btnWhatsApp : UIButton!
    @IBOutlet var btnMessage : UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnFaceBook.layer.cornerRadius = btnFaceBook.frame.size.width/2
        btnMessanger.layer.cornerRadius = btnMessanger.frame.size.width/2
        btnEmail.layer.cornerRadius = btnEmail.frame.size.width/2
        btnTwitter.layer.cornerRadius = btnTwitter.frame.size.width/2
        btnInstaGram.layer.cornerRadius = btnInstaGram.frame.size.width/2
        btnSnapChat.layer.cornerRadius = btnSnapChat.frame.size.width/2
        btnTikTok.layer.cornerRadius = btnTikTok.frame.size.width/2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
