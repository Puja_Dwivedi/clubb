//
//  DetailCell.swift
//  ClubBooks
//
//  Created by cis on 25/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class DetailCell: UITableViewCell {

    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblPlace : UILabel!
    @IBOutlet var lblTime : UILabel!
    @IBOutlet var lblDate : UILabel!
    @IBOutlet var btnCell: UIButton!
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet weak var ViewBGDelete: UIView!
    @IBOutlet weak var bgConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
