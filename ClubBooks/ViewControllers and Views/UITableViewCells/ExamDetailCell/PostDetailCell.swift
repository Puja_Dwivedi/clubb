//
//  ExamDetailCell.swift
//  ClubBooks
//
//  Created by cis on 26/03/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class PostDetailCell: UITableViewCell {
    @IBOutlet weak var imgViewExam: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblSchool: UILabel!
    @IBOutlet weak var lblDepartment: UILabel!
    @IBOutlet weak var lblProfessor: UILabel!
    @IBOutlet weak var lblCourse: UILabel!
    @IBOutlet weak var lblNoOfPages: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    var cellType: PostType = PostType.init(rawValue: 0)!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(PostDetails : SelectedPostDetail){
        self.lblTitle.text = PostDetails.BookTitle
        self.lblPrice.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailPrice", txtText: PostDetails.BookPrice)
        self.lblDepartment.getAttibutedStringWithTitle(txtTitle: "lbl.title.department", txtText: PostDetails.BookDepartment)
        self.lblCourse.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailCourse#", txtText: PostDetails.BookCourse)
        self.lblNoOfPages.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailNoOfPages", txtText: PostDetails.NumberOfPages)
        self.lblProfessor.getAttibutedStringWithTitle(txtTitle: "lbl.title.professor", txtText: PostDetails.BookProfessor)
        self.lblSchool.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailSchool", txtText: PostDetails.BookSchool)
        self.lblCity.getAttibutedStringWithTitle(txtTitle: "lbl.title.DetailCity", txtText: PostDetails.BookCity)
        if cellType == .PostExam{
                self.imgViewExam.image = #imageLiteral(resourceName: "exams")
        }else if cellType == .PostQuiz{
                self.imgViewExam.image = #imageLiteral(resourceName: "quizzes")
        }else{
                self.imgViewExam.image = #imageLiteral(resourceName: "class_notes")
        }
        if PostDetails.BookDate != ""{
            lblTime.text = AppTheme.sharedInstance.getTimeDifferencefromDate(strDate: PostDetails.BookDate)
        }
    }
}
