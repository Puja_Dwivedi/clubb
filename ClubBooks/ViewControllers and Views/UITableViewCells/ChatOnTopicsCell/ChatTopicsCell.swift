//
//  DetailCell.swift
//  ClubBooks
//
//  Created by cis on 25/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class ChatTopicsCell: UITableViewCell {

    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblPlace : UILabel!
    @IBOutlet var lblTime : UILabel!
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var btnCell: UIButton!
    @IBOutlet var lblDate : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
