//
//  HomeHeaderCell.swift
//  ClubBooks
//
//  Created by cis on 29/12/18.
//  Copyright © 2018 CIS. All rights reserved.
//

import UIKit

class HomeHeaderCell: UICollectionViewCell {
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var lblHeading : UILabel!
    @IBOutlet var lblAllHeight : NSLayoutConstraint!
}
