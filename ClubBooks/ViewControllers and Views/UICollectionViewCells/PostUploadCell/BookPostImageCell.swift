//
//  BookPostImageCell.swift
//  ClubBooks
//
//  Created by cis on 25/01/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class BookPostImageCell: UICollectionViewCell {

    @IBOutlet var imgView : UIImageView!
    @IBOutlet var btnClose : UIButton!
    @IBOutlet var btnAdd : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
