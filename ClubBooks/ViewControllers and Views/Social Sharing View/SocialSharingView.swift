//
//  Social Sharing View.swift
//  ClubBooks
//
//  Created by cis on 18/06/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class SocialSharingView: UIView {
    @IBOutlet var contentView: UIView!
    var sendMessage:((String)->Void)! = nil
    //MARK: Function
    func function_init() {
        Bundle.main.loadNibNamed("SocialSharingView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth ]
    }
    //MARK: Button
    @IBAction func btn_Send(_ sender: UIButton) {
            self.sendMessage("\("")")
    }
    
}
