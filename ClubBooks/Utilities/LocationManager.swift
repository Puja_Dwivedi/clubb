//
//  LocationManager.swift
//  InternationalVideoEditingApplication
//
//  Created by cis on 28/08/19.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

protocol LocationManagerDelegate {
    func getTheDetails(userLocation:String, userLocationCoordinates: CLLocationCoordinate2D);
}

class LocationManagerVC : UIViewController {
    
    var delegate:LocationManagerDelegate?
    var locationManager = CLLocationManager()
    
    var coordinates: CLLocationCoordinate2D?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureLocationManager()
    }
    func configureLocationManager(){
        if #available(iOS 9.0, *) {
            locationManager.allowsBackgroundLocationUpdates = true
        }
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 100
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
    }
}


extension LocationManagerVC : CLLocationManagerDelegate {
    
    //MARK: LocationManager Delegate Methods
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == .authorizedAlways) || (status == .authorizedWhenInUse)
        {
            manager.startUpdatingLocation()
        }
    }
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Error:\(error.localizedDescription)")
    }
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let updatedLocation:CLLocation = locations.first!
        let newCoordinate: CLLocationCoordinate2D = updatedLocation.coordinate
        let usrDefaults:UserDefaults = UserDefaults.standard
        if AppTheme.sharedInstance.currentUserData.count > 0 {
            let currntUser = AppTheme.sharedInstance.currentUserData[0]
            usrDefaults.set("\(newCoordinate.latitude)", forKey: "current_latitude")
            usrDefaults.set("\(newCoordinate.longitude)", forKey: "current_longitude")
            usrDefaults.synchronize()
            HSRealTimeMessagingLIbrary.function_UpdateUserLocation(location: updatedLocation, forID: UInt64(currntUser.UserID) ?? 0)
        }
    }
}

