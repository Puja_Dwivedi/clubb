//
//  FriendProfileDetails.swift
//  ClubBooks
//
//  Created by cis on 24/04/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit
import SwiftyJSON
class FriendProfileDetails: NSObject {
    
    var email = String()
    var id = String()
    var userImg = String()
    var userId = String()
    var userName = String()
    var name = String()
    var isFirebaseLogin = String()
    var mobile = String()
    var schoolName = String()
    var favBook = String()
    var gender = String()
    var location = String()
    var city = String()
    var country = String()
    var zipCode = String()
    var friendFollowingStatus = String()
    var isAcceptedRequest = Bool()
    var leaderID = String()
    var followerID = String()
    var posttedBooksCount = String()
    var posttedMeetupsCount = String()
    var posttedChatsOnBooksCount = String()
    var posttedChatsOnTopicCount = String()
    var postedExamsCount = String()
    var posttedClassNotesCount = String()
    var posttedQuizesCount = String()
    var userRating = String()
    var isratedAlready = Bool()
    var isFollowing = Bool()
    var followersCount = String()
    var followingCount = String()
    var buddiesCount = String()
    var account_type = String()
    
    override init() {
        super.init()
    }
    
    func parser(profileData : JSON) {
        self.id = profileData["id"].stringValue
        self.email =  profileData["user_email"].stringValue
        self.userImg = profileData["user_img"].stringValue
        self.userId =  profileData["user_id"].stringValue
        if profileData["user_name"].stringValue == ""{
            self.userName =  profileData["username"].stringValue
        }else{
            self.userName =  profileData["user_name"].stringValue
        }
        self.name = profileData["name"].stringValue
        self.isFirebaseLogin =  profileData["is_firebase_login"].stringValue
        self.mobile = profileData["mobile"].stringValue
        self.schoolName =  profileData["school_name"].stringValue
        self.favBook =  profileData["fav_book"].stringValue
        self.gender = profileData["gender"].stringValue
        self.location =  profileData["location"].stringValue
        self.city = profileData["city"].stringValue
        self.country =  profileData["country"].stringValue
        self.zipCode =  profileData["zip_code"].stringValue
        self.friendFollowingStatus = profileData["friend_following_status"].stringValue
        self.isAcceptedRequest = RV_CheckDataType.getBoolValue(obj: profileData["is_accepted"].boolValue as AnyObject)
        self.leaderID =  RV_CheckDataType.getString(anyString: profileData["leader_id"].stringValue as AnyObject)
        self.followerID = RV_CheckDataType.getString(anyString: profileData["follower_id"].stringValue as AnyObject)
        self.posttedBooksCount = RV_CheckDataType.getString(anyString: profileData["posted_books"].stringValue as AnyObject)
        self.posttedMeetupsCount = RV_CheckDataType.getString(anyString: profileData["posted_meetups"].stringValue as AnyObject)
        self.posttedChatsOnBooksCount = RV_CheckDataType.getString(anyString: profileData["posted_chatonbooks"].stringValue as AnyObject)
        self.posttedChatsOnTopicCount = RV_CheckDataType.getString(anyString: profileData["posted_chatontopics"].stringValue as AnyObject)
        self.postedExamsCount = RV_CheckDataType.getString(anyString: profileData["posted_exams"].stringValue as AnyObject)
        self.posttedQuizesCount = RV_CheckDataType.getString(anyString: profileData["posted_quizzes"].stringValue as AnyObject)
        self.posttedClassNotesCount = RV_CheckDataType.getString(anyString: profileData["posted_classnotes"].stringValue as AnyObject)
        self.userRating = RV_CheckDataType.getString(anyString: profileData["UserAvgRating"].stringValue as AnyObject)
        self.isFollowing = RV_CheckDataType.getBoolValue(obj: profileData["is_following"].stringValue as AnyObject)
        self.followingCount = RV_CheckDataType.getString(anyString: profileData["following_count"].stringValue as AnyObject)
        self.followersCount = RV_CheckDataType.getString(anyString: profileData["follower_count"].stringValue as AnyObject)
        self.buddiesCount = RV_CheckDataType.getString(anyString: profileData["buddies_count"].stringValue as AnyObject)
        self.account_type = RV_CheckDataType.getString(anyString: profileData["account_type"].stringValue as AnyObject)
        if  RV_CheckDataType.getString(anyString: profileData["loggedInUserRatingForFriend"].stringValue as AnyObject) == "" {
            isratedAlready = false
        }else{
            isratedAlready = true
        }
    }
}
