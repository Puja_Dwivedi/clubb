//
//  MeetupDetails.swift
//  ClubBooks
//
//  Created by cis on 16/11/22.
//  Copyright © 2022 CIS. All rights reserved.
//

import UIKit
import SwiftyJSON
class MeetupDetails: NSObject {
    var PostID = String()
    var updatedAt = String()
    var PostUrl = String()
    var meetupPlace = String()
    var meetupComment = String()
    var meetupTime = String()
    var userID = String()
    var title = String()
    var meetupDate = String()
    var meetupCreator = String()
    
    override init() {
        super.init()
    }
    
    func parser(_ data: JSON) {
        PostID = data["id"].stringValue
        updatedAt = data["updated_at"].stringValue
        PostUrl = data["post_url"].stringValue
        meetupPlace = data["meetup_place"].stringValue
        meetupComment = data["meetup_comment"].stringValue
        meetupTime = data["meetup_time"].stringValue
        userID = data["user_id"].stringValue
        title = data["title"].stringValue
        meetupDate = data["meetup_date"].stringValue
        meetupCreator = data["post_creator"].stringValue
    }
}
