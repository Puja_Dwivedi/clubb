//
//  BookDetails.swift
//  ClubBooks
//
//  Created by cis on 20/03/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit
import SwiftyJSON
class SelectedPostDetail: NSObject {
    var PostID = String()
    var PostCat = String()
    var Userid = String()
    var UserName = String()
    var UserEmail = String()
    var userMobileNumber = String()
    var BookIsAnnonymous = String()
    var BookTitle = String()
    var BookImage = String()
    var BookAuthor = String()
    var BookEdition = String()
    var BookCity = String()
    var BookSchool = String()
    var BookDepartment = String()
    var BookProfessor = String()
    var BookCourse = String()
    var BookCondition = String()
    var BookISBN = String()
    var BookPhone = String()
    var BookMeetupPlace = String()
    var BookPrice = String()
    var BookCreator = String()
    var BookDate = String()
    var BookMeetUpLocation = String()
    var NumberOfPages = String()
    var isReserved = String()
    var reserveBookUserID = String()
    var reservebookTime = String()
    var reserveBookEndTime = String()
    var currentDateTime = String()
    var postURL = String()
    var sellerRating = String()
    var BookImages = [PostImages]()
    
    override init() {
        super.init()
    }
    
    func parser(_ data: [String : JSON]) {
        PostID = data["post_data"]!["id"].stringValue
        PostCat = data["post_data"]!["post_cat_id"].stringValue
        UserName = data["post_data"]!["username"].stringValue
        userMobileNumber = data["post_data"]!["mobile"].stringValue
        UserEmail = data["post_data"]!["email"].stringValue
        Userid = data["post_data"]!["user_id"].stringValue
        BookTitle = data["post_data"]!["title"].stringValue
        BookAuthor = data["post_data"]!["author"].stringValue
        BookEdition = data["post_data"]!["edition"].stringValue
        BookCity = data["post_data"]!["city"].stringValue
        BookSchool = data["post_data"]!["school"].stringValue
        BookDepartment = data["post_data"]!["department"].stringValue
        BookProfessor = data["post_data"]!["professor"].stringValue
        BookCourse = data["post_data"]!["course"].stringValue
        BookISBN = data["post_data"]!["isbn"].stringValue
        BookPhone = data["post_data"]!["phone"].stringValue
        BookMeetupPlace = data["post_data"]!["meetup_place"].stringValue
        BookPrice = data["post_data"]!["price"].stringValue
        BookIsAnnonymous = data["post_data"]!["is_anonymous"].stringValue
        BookImage = data["post_data"]!["post_img"].stringValue
        BookDate = data["post_data"]!["created_at"].stringValue
        BookCreator = data["post_data"]!["post_creator"].stringValue
        BookMeetUpLocation = data["post_data"]!["meetup_place"].stringValue
        isReserved = data["post_data"]!["is_reserved"].stringValue
        reserveBookUserID = data["post_data"]!["reserved_by"].stringValue
        reservebookTime = data["post_data"]!["reserved_date"].stringValue
        currentDateTime = data["post_data"]!["current_date_and_time"].stringValue
        reserveBookEndTime = data["post_data"]!["reserve_end_date"].stringValue
        postURL = data["post_data"]!["post_url"].stringValue
        sellerRating = data["post_data"]!["UserAvgRating"].stringValue
        
        if data["post_data"]!["no_of_pages"].stringValue != "" {
            NumberOfPages = data["post_data"]!["no_of_pages"].stringValue
        }
        for json in (data["post_data_images"]?.arrayValue)!{
            let bookimage = PostImages()
            bookimage.parser(json)
            self.BookImages.append(bookimage)
        }
        
        let condition = data["post_data"]!["post_condition"].stringValue
        switch condition{
        case "1" :
            BookCondition = "lbl.new"
        case "2" :
            BookCondition = "lbl.good"
        case "3" :
            BookCondition = "lbl.fair"
        default:
            BookCondition = "lbl.poor"
        }
    }
}
