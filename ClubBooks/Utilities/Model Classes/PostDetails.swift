//
//  BookDetails.swift
//  ClubBooks
//
//  Created by cis on 02/03/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit
import SwiftyJSON
class PostDetails: NSObject {
    var postID = String()
    var postCat = String()
    var userid = String()
    var postUserName = String()
    var postTitle = String()
    var postAuthor = String()
    var postEdition = String()
    var postCity = String()
    var postSchool = String()
    var postDepartment = String()
    var postProfessor = String()
    var postCourse = String()
    var postPostCondition = String()
    var postISBN = String()
    var postPhone = String()
    var postMeetupPlace = String()
    var postPrice = String()
    var postMeetupTime = String()
    var postMeetUPDate = String()
    var postMeetUpComment = String()
    var postBookAuthorForChat = String()
    var postChatType = String()
    var postPagesCount = String()
    var postIsAnnonymous = String()
    var postPostImages = String()
    var postCreatedAt = String()
    var postDate = String()
    var postCreator = String()
    var RecentChat = [PostRecentChat]()
    var isReserved = String()
    
    
    override init() {
        super.init()
    }
    
    func parser(_ data: JSON) {
        postID = data["id"].stringValue
        postCat = data["post_cat_id"].stringValue
        userid = data["user_id"].stringValue
        postTitle = data["title"].stringValue
        postAuthor = data["author"].stringValue
        postEdition = data["edition"].stringValue
        postCity = data["city"].stringValue
        postSchool = data["school"].stringValue
        postDepartment = data["department"].stringValue
        postProfessor = data["professor"].stringValue
        postCourse = data["course"].stringValue
        postISBN = data["isbn"].stringValue
        postPhone = data["phone"].stringValue
        postMeetupPlace = data["meetup_place"].stringValue
        postPrice = data["price"].stringValue
        postMeetupTime = data["meetup_time"].stringValue
        postMeetUPDate = data["meetup_date"].stringValue
        postMeetUpComment = data["meetup_comment"].stringValue
        postBookAuthorForChat = data["book_author_for_chat"].stringValue
        postChatType = data["chat_type"].stringValue
        postPagesCount = data["no_of_pages"].stringValue
        postIsAnnonymous = data["is_anonymous"].stringValue
        postPostImages = data["post_img"].stringValue
        postUserName = data[""].stringValue
        postDate = data["created_at"].stringValue
        postCreator = data["post_creator"].stringValue
        postCreatedAt = data["created_at"].stringValue
        isReserved = data["is_reserved"].stringValue
        for json in (data["chat_history"].arrayValue){
            let RecentChat = PostRecentChat()
            RecentChat.parser(json)
            self.RecentChat.append(RecentChat)
        }
        
        let condition = data["post_condition"].stringValue
        
        switch condition{
        case "1" :
            postPostCondition = "lbl.new"
        case "2" :
            postPostCondition = "lbl.good"
        case "3" :
            postPostCondition = "lbl.fair"
        default:
            postPostCondition = "lbl.poor"
        }
    }
}
class PostRecentChat: NSObject {
    var SenderName = String()
    var ChatID = String()
    var SenderEmail = String()
    var MessageID = String()
    var UpdatedAt = String()
    var UserID = String()
    var UserImage = String()
    var ChatMessage = String()
    
    override init() {
        super.init()
    }
    
    func parser(_ data: JSON) {
        SenderName = data["name"].stringValue
        ChatID = data["chat_id"].stringValue
        SenderEmail = data["email"].stringValue
        MessageID = data["id"].stringValue
        UpdatedAt = data["updated_at"].stringValue
        UserID = data["user_id"].stringValue
        UserImage = data["user_img"].stringValue
        ChatMessage = data["chat_message"].stringValue
    }
}
