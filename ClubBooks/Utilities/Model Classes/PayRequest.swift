//
//  PayRequest.swift
//  ClubBooks
//
//  Created by cis on 31/07/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit
import SwiftyJSON
class PayRequest: NSObject {

    var status = String()
    var updated_by = String()
    var updated_at = String()
    var id = String()
    var requested_by = String()
    var requested_to = String()
    var created_at = String()
    var amount = String()
    var message = String()
    var username = String()

    override init() {
        super.init()
    }
    
     func parser(requestData : JSON) {
        self.status = requestData["status"].stringValue
        self.updated_by = requestData["updated_by"].stringValue
        self.updated_at = requestData["updated_at"].stringValue
        self.id = requestData["id"].stringValue
        self.requested_by = requestData["requested_by"].stringValue
        self.requested_to = requestData["requested_to"].stringValue
        self.created_at = requestData["created_at"].stringValue
        self.amount = requestData["amount"].stringValue
        self.message = requestData["message"].stringValue
        self.username = requestData["requested_by_username"].stringValue
    }
}
