//
//  UserProfileDetails.swift
//  ClubBooks
//
//  Created by cis on 03/09/18.
//  Copyright © 2018 CIS. All rights reserved.
//

import UIKit

class UserProfileDetails: NSObject{
    var id : String
    var Address : String
    var AgeGroup : String
    var CityName : String
    var CommentCount : String
    var CountryID : String
    var Email : String
    var FolloweesCount : String
    var FollowersCount : String
    var Gender : String!
    var IsComment : String!
    var Name : String
    var TotalAmount : String
    var UserImage : String
    var UserName : String!
    var UserRole : String!
    var isFollowed : Bool
    
    init(profileData : NSDictionary) {
        self.id = RV_CheckDataType.getString(anyString: profileData.object(forKey: KEY.UserProfile.id) as AnyObject)
        self.Address = RV_CheckDataType.getString(anyString: profileData.object(forKey: KEY.UserProfile.address) as AnyObject)
        self.AgeGroup = RV_CheckDataType.getString(anyString: profileData.object(forKey: KEY.UserProfile.ageGroup) as AnyObject)
        self.CityName = RV_CheckDataType.getString(anyString: profileData.object(forKey: KEY.UserProfile.cityName) as AnyObject)
        self.CommentCount = RV_CheckDataType.getString(anyString: profileData.object(forKey: KEY.UserProfile.CommentCount) as AnyObject)
        self.CountryID = RV_CheckDataType.getString(anyString: profileData.object(forKey: KEY.UserProfile.countryID) as AnyObject)
        self.Email = RV_CheckDataType.getString(anyString: profileData.object(forKey: KEY.UserProfile.email) as AnyObject)
        self.FolloweesCount = RV_CheckDataType.getString(anyString: profileData.object(forKey: KEY.UserProfile.followeesCount) as AnyObject)
        self.FollowersCount = RV_CheckDataType.getString(anyString: profileData.object(forKey: KEY.UserProfile.followersCount) as AnyObject)
        self.Gender = RV_CheckDataType.getString(anyString: profileData.object(forKey: KEY.UserProfile.gender) as AnyObject)
        self.IsComment = RV_CheckDataType.getString(anyString: profileData.object(forKey: KEY.UserProfile.isComment) as AnyObject)
        self.Name = RV_CheckDataType.getString(anyString: profileData.object(forKey: KEY.UserProfile.name) as AnyObject)
        self.TotalAmount = RV_CheckDataType.getString(anyString: profileData.object(forKey: KEY.UserProfile.totalAmount) as AnyObject)
        self.UserImage = RV_CheckDataType.getString(anyString: profileData.object(forKey: KEY.UserProfile.userImage) as AnyObject)
        self.UserName = RV_CheckDataType.getString(anyString: profileData.object(forKey: KEY.UserProfile.userName) as AnyObject)
        self.UserRole = RV_CheckDataType.getString(anyString: profileData.object(forKey: KEY.UserProfile.userRole) as AnyObject)
        self.isFollowed = RV_CheckDataType.getBoolValue(obj: profileData.object(forKey: KEY.UserProfile.isFollowed) as AnyObject)
    }
}
