//
//  PostImages.swift
//  ClubBooks
//
//  Created by cis on 20/03/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit
import SwiftyJSON
class PostImages: NSObject {
    var PostImageID = String()
    var PostPrimaryImage = String()
    var PostImage = String()
   
    override init() {
        super.init()
    }
    
    func parser(_ data: JSON) {
        PostImageID = data["post_img_id"].stringValue
        PostPrimaryImage = data["primary_img"].stringValue
        PostImage = data["post_img"].stringValue
    }
}
