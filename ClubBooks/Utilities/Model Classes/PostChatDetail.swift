//
//  PostChatDetail.swift
//  ClubBooks
//
//  Created by cis on 18/04/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit
import SwiftyJSON
class PostChatDetail: NSObject {
    var SenderName = String()
    var ChatID = String()
    var Email = String()
    var MessageTime = String()
    var SenderID = String()
    var userImage = String()
    var chatMessage = String()
    var isMyMessage = Bool()
    var chatImage = String()
    
    func parser(_ data: JSON) {
        SenderName = data["name"].stringValue
        ChatID = data["chat_id"].stringValue
        Email = data["email"].stringValue
        MessageTime = data["updated_at"].stringValue
        SenderID = data["user_id"].stringValue
        userImage = data["user_img"].stringValue
        chatMessage = data["chat_message"].stringValue
        MessageTime = data["created_at"].stringValue
        chatImage = data["chat_image"].stringValue
        if SenderID == AppTheme.sharedInstance.currentUserData[0].UserID {
            isMyMessage = true
        }else{
            isMyMessage = false
        }
    }
    
}

