//
//  Notification.swift
//  ClubBooks
//
//  Created by cis on 20/12/22.
//  Copyright © 2022 CIS. All rights reserved.
//

import UIKit
import SwiftyJSON
class Notifications: NSObject {
    var title = String()
    var subTitle = String()
    var image = String()
    var reserved_Date = String()
    var reserved_Time = String()
    
    override init() {
        super.init()
    }
    
    func parser(_ data: JSON) {
        title = data["title"].stringValue
        subTitle = data["subtitle"].stringValue
        image = data["image"].stringValue
        reserved_Date = data["reserved_date"].stringValue
        reserved_Time = data["reserved_time"].stringValue
    }
}
