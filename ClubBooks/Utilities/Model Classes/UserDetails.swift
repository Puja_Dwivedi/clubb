//
//  UserDetails.swift
//  BudSeeker
//
//  Created by cis on 01/12/17.
//  Copyright © 2018 CIS. All rights reserved.
//

import Foundation
import SwiftyJSON
class UserDetails  : NSObject , NSCoding{
    var UserID : String
    var Name : String
    var email : String
    var FavBook : String
    var Location : String
    var Mobile : String!
    var UserName : String!
    var SchoolName : String!
    var Token : String!
    var appRaing : String!
    var Gender : String!
    var AccountType : String!
    
    init(userDict : JSON) {
        let dicResponse = userDict["responseData"].dictionary
        let userData = dicResponse!["UserData"]
        self.UserID = userData![KEY.NormalUserDetails.UserId].stringValue
        self.Name = userData![KEY.NormalUserDetails.Name].stringValue
        self.email = userData![KEY.NormalUserDetails.EmailId].stringValue
        self.UserName = userData![KEY.NormalUserDetails.UserName].stringValue
        self.FavBook = userData![KEY.NormalUserDetails.FavBook].stringValue
        self.Location = userData![KEY.NormalUserDetails.Location].stringValue
        self.Mobile = userData![KEY.NormalUserDetails.Mobile].stringValue
        self.SchoolName = userData![KEY.NormalUserDetails.SchoolName].stringValue
        self.Token = dicResponse![KEY.NormalUserDetails.Token]?.stringValue
        self.Gender = dicResponse![KEY.NormalUserDetails.Gender]?.stringValue
        self.appRaing = userData!["AppRating"].stringValue
        self.AccountType = userData!["account_type"].stringValue
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        self.UserID = RV_CheckDataType.getString(anyString:aDecoder.decodeObject(forKey: KEY.NormalUserDetails.UserId) as AnyObject)
        self.Name = RV_CheckDataType.getString(anyString:aDecoder.decodeObject(forKey: KEY.NormalUserDetails.Name) as AnyObject)
        self.email = RV_CheckDataType.getString(anyString:aDecoder.decodeObject(forKey: KEY.NormalUserDetails.EmailId) as AnyObject)
        self.UserName = RV_CheckDataType.getString(anyString: aDecoder.decodeObject(forKey: KEY.NormalUserDetails.UserName) as AnyObject)
        self.FavBook = RV_CheckDataType.getString(anyString: aDecoder.decodeObject(forKey: KEY.NormalUserDetails.FavBook) as AnyObject)
        self.Location = RV_CheckDataType.getString(anyString: aDecoder.decodeObject(forKey: KEY.NormalUserDetails.Location) as AnyObject)
        self.Mobile = RV_CheckDataType.getString(anyString: aDecoder.decodeObject(forKey: KEY.NormalUserDetails.Mobile) as AnyObject)
        self.SchoolName = RV_CheckDataType.getString(anyString: aDecoder.decodeObject(forKey: KEY.NormalUserDetails.SchoolName) as AnyObject)
        self.Token = RV_CheckDataType.getString(anyString: aDecoder.decodeObject(forKey: KEY.NormalUserDetails.Token) as AnyObject)
        self.Gender = RV_CheckDataType.getString(anyString: aDecoder.decodeObject(forKey: KEY.NormalUserDetails.Gender) as AnyObject)
        self.appRaing = RV_CheckDataType.getString(anyString: aDecoder.decodeObject(forKey: "AppRating") as AnyObject)
        self.AccountType = RV_CheckDataType.getString(anyString: aDecoder.decodeObject(forKey: "account_type") as AnyObject)
    }
    
    func encode(with aCoder: NSCoder) {

        aCoder.encode(UserID, forKey: KEY.NormalUserDetails.UserId)
        aCoder.encode(Name, forKey: KEY.NormalUserDetails.Name)
        aCoder.encode(email, forKey: KEY.NormalUserDetails.EmailId)
        aCoder.encode(UserName, forKey: KEY.NormalUserDetails.UserName)
        aCoder.encode(FavBook, forKey: KEY.NormalUserDetails.FavBook)
        aCoder.encode(Location, forKey: KEY.NormalUserDetails.Location)
        aCoder.encode(Mobile, forKey: KEY.NormalUserDetails.Mobile)
        aCoder.encode(SchoolName, forKey: KEY.NormalUserDetails.SchoolName)
        aCoder.encode(Token, forKey: KEY.NormalUserDetails.Token)
        aCoder.encode(Token, forKey: KEY.NormalUserDetails.Gender)
        aCoder.encode(appRaing, forKey: "AppRating")
        aCoder.encode(AccountType, forKey: "account_type")
    }
    
}
