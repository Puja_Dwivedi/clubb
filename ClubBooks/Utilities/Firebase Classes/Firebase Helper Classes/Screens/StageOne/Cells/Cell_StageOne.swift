//
//  Cell_StageOne.swift
//  HSRealTimeMessaging
//
//  Created by cis on 08/04/19.
//  Copyright © 2019 Cis. All rights reserved.
//

import UIKit

class Cell_StageOne: UITableViewCell {
    @IBOutlet var btn_ImgView: UIButton!
    @IBOutlet var lbl_Name: UILabel!
    @IBOutlet var btn_Status: UIButton!
    static let identifier = "Cell_StageOne"
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.btn_Status.layer.cornerRadius = 5
        self.btn_Status.clipsToBounds = true
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
