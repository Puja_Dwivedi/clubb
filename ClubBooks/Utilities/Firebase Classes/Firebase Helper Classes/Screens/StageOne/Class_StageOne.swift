//
//  Class_StageOne.swift
//  HSRealTimeMessaging
//
//  Created by cis on 08/04/19.
//  Copyright © 2019 Cis. All rights reserved.
//

import UIKit

class Class_StageOne: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: Variables
    @IBOutlet var tableView: UITableView!
    @IBOutlet var SegmentOnlineOfline: UISegmentedControl!
    @IBOutlet var tableHeaderView: UIView!
    private var tableData:[HSChatUser] = []
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Connect"
        self.tableView.register(UINib.init(nibName: Cell_StageOne.identifier, bundle: nil), forCellReuseIdentifier: Cell_StageOne.identifier)
        self.tableHeaderView.frame = CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width/5)
        self.tableView.tableHeaderView = self.tableHeaderView
        self.tableView.separatorStyle = .none
        
        HSRealTimeMessagingLIbrary.function_GetOnlineStatus(HSRealTimeMessagingLIbrary.HSCurrentUserID()) { (isOnline) in
            if isOnline {self.SegmentOnlineOfline.selectedSegmentIndex = 1} else {self.SegmentOnlineOfline.selectedSegmentIndex = 0}
        }
        self.function_LoadData()
    }
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override open func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: Buttons
    @IBAction func SegmentedControllerOnlineOfline(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            HSRealTimeMessagingLIbrary.function_UpdateOnlineStatus(HSRealTimeMessagingLIbrary.HSCurrentUserID(), false)
        case 1:
            HSRealTimeMessagingLIbrary.function_UpdateOnlineStatus(HSRealTimeMessagingLIbrary.HSCurrentUserID(), true)
        default:
            return
        }
    }
    //MARK: Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Cell_StageOne.identifier) as? Cell_StageOne else { return UITableViewCell.init(style: .default, reuseIdentifier: Cell_StageOne.identifier) }
        cell.lbl_Name.text = "\(self.tableData[indexPath.row].name)"
        HSRealTimeMessagingLIbrary.function_GetOnlineStatus(self.tableData[indexPath.row].id) { (isOnline) in
            if isOnline {
                cell.btn_Status.backgroundColor = UIColor.green
            } else {
                cell.btn_Status.backgroundColor = UIColor.lightGray
            }
        }
        let backView = UIView.init(frame: cell.contentView.frame); backView.backgroundColor = .clear
        cell.selectedBackgroundView = backView
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.width/7
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = Class_StageTwo.init(nibName: "Class_StageTwo", bundle: Bundle.main)
        vc.chatDetail = self.tableData[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK: Functions
    func function_LoadData() {
        HSRealTimeMessagingLIbrary.function_LoadChatUsers(isID: HSRealTimeMessagingLIbrary.HSCurrentUserID()) { (user) in
            self.tableData.append(user)
            self.tableView.reloadData()
        }
    }
}
