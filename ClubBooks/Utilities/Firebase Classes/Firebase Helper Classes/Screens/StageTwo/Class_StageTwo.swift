//
//  Class_StageTwo.swift
//  HSRealTimeMessaging
//
//  Created by cis on 08/04/19.
//  Copyright © 2019 Cis. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Photos
class Class_StageTwo: BaseViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, MyMediaCellDelegate,OtherMediaCellDelegate {

    //MARK: Variables
    @IBOutlet var tableView: UITableView!
    @IBOutlet var view_TextView: HSExpandableTextView!
    var chatDetail: HSChatUser! = nil
//    private var tableData:[HSMessage] = []
    var sectionArray = [String]()
    var chatArray = [String: [HSMessage]]()
    
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib.init(nibName: "Cell_MyMessage", bundle: nil), forCellReuseIdentifier: "Cell_MyMessage")
        self.tableView.register(UINib.init(nibName: "Cell_OtherMessage", bundle: nil), forCellReuseIdentifier: "Cell_OtherMessage")
        self.tableView.register(UINib.init(nibName: "Cell_MyMediaMessage", bundle: nil), forCellReuseIdentifier: "Cell_MyMediaMessage")
        self.tableView.register(UINib.init(nibName: "Cell_OtherMediaMessage", bundle: nil), forCellReuseIdentifier: "Cell_OtherMediaMessage")
        self.tableView.transform = CGAffineTransform.init(rotationAngle: -(CGFloat(Double.pi)))
        self.showLogoWithBackButon()
        AppTheme.sharedInstance.currentChatID = self.chatDetail.chatID
        self.view_TextView.sendMessage = { text in
            HSRealTimeMessagingLIbrary.function_SendTextMessage("\(text)", isSenderName: "\(HSRealTimeMessagingLIbrary.HSCurrentUserName())", isReciverName: "\(self.chatDetail.name)", isSenderID: HSRealTimeMessagingLIbrary.HSCurrentUserID(), isReciverID: self.chatDetail.id, isSuccess: {
            }, isError: { (message) in
                self.function_Pop("Unable to send the message, Please try again later.")
            })
        }
        self.view_TextView.sendMedia = {
            let alert = UIAlertController.init(title: "ClubBooks", message: "alert.shareImageVideo".localized, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction.init(title: "alert.image".localized, style: .default, handler: { (action) in
                guard UIImagePickerController.isCameraDeviceAvailable(.front) else {return}
                let picker = UIImagePickerController.init()
                picker.sourceType = .camera
                picker.allowsEditing = true
                picker.delegate = self
                self.navigationController?.present(picker, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction.init(title: "alert.choooseimage".localized, style: .default, handler: { (action) in
                let picker = UIImagePickerController.init()
                picker.sourceType = .photoLibrary
                picker.allowsEditing = true
                picker.delegate = self
                self.navigationController?.present(picker, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction.init(title: "alert,recordVideo".localized, style: .default, handler: { (action) in
                guard UIImagePickerController.isCameraDeviceAvailable(.front) else {return}
                let picker = UIImagePickerController.init()
                picker.sourceType = .camera
                picker.mediaTypes = ["public.movie"]
                picker.allowsEditing = true
                picker.delegate = self
                self.navigationController?.present(picker, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction.init(title: "alert.video".localized, style: .default, handler: { (action) in
                let picker = UIImagePickerController.init()
                picker.sourceType = .photoLibrary
                picker.mediaTypes = ["public.movie"]
                picker.allowsEditing = true
                picker.delegate = self
                self.navigationController?.present(picker, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction.init(title: "alert.cancel".localized, style: .cancel, handler: { (action) in
            }))
            self.navigationController?.present(alert, animated: true, completion: nil)
        }
        self.function_LoadData()
        self.navigationController?.setNavigationBarHidden(false, animated: true)

    }
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let titleButton =  UIButton(type: .custom)
        titleButton.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        titleButton.backgroundColor = .clear
        titleButton.setTitleColor(.black, for: .normal)
        titleButton.setTitle(self.chatDetail.name, for: .normal)
        titleButton.addTarget(self, action: #selector(userNameClicked(_:)), for: .touchUpInside)
        navigationItem.titleView = titleButton
    }
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override open func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        AppTheme.sharedInstance.currentChatID = ""
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @objc func userNameClicked(_ sender : UIButton){
        if String(self.chatDetail.id) != AppTheme.sharedInstance.currentUserData[0].UserID{
            let friendProfileVC : FriendProfileVC = storyBoard.instantiateViewController(withIdentifier: "FriendProfileVC") as! FriendProfileVC
            friendProfileVC.strFriendID = String(self.chatDetail.id)
            friendProfileVC.strFriendName = self.chatDetail.name
            self.navigationController?.pushViewController(friendProfileVC, animated: true)
        }else{
            let profile = storyBoard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            profile.isPreviousViewController = true
            self.navigationController?.pushViewController(profile, animated: true)
        }
    }
    
    
    //MARK :
    //MARK : Cell Delegates
    func btnOpenMedia(cell: Cell_MyMediaMessage) {
        guard let chatDict = chatArray[sectionArray[self.tableView.indexPath(for: cell)!.section]]?[self.tableView.indexPath(for: cell)!.row] else {
            return
        }
        if chatDict.type == "image" {
            let vc = HSImageViewer.init(nibName: "HSImageViewer", bundle: Bundle.main)
            vc.imgURL = "\(chatDict.message)"
            vc.returnErro = { error in
                vc.dismiss(animated: true, completion: {
                    let alert = UIAlertController.init(title: Constant.ALERT_TITLE, message: "\(error)", preferredStyle: .alert)
                    alert.addAction(UIAlertAction.init(title: "Cancel", style: .default, handler: nil))
                    self.navigationController?.present(alert, animated: true, completion: nil)
                })
            }
            self.navigationController?.present(vc, animated: true, completion: nil)
        }else if chatDict.type == "video" {
            if let videoURL = URL(string: "\(chatDict.message)") {
                let player = AVPlayer(url: videoURL)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    if let isPLayer = playerViewController.player {
                        isPLayer.play()
                    }
                }
            }
        }
    }
    func btnOpenOtherMedia(cell: Cell_OtherMediaMessage) {
        guard let chatDict = chatArray[sectionArray[self.tableView.indexPath(for: cell)!.section]]?[self.tableView.indexPath(for: cell)!.row] else {
            return
        }
        if chatDict.type == "image" {
            let vc = HSImageViewer.init(nibName: "HSImageViewer", bundle: Bundle.main)
            vc.imgURL = "\(chatDict.message)"
            vc.returnErro = { error in
                vc.dismiss(animated: true, completion: {
                    let alert = UIAlertController.init(title: Constant.ALERT_TITLE, message: "\(error)", preferredStyle: .alert)
                    alert.addAction(UIAlertAction.init(title: "Cancel", style: .default, handler: nil))
                    self.navigationController?.present(alert, animated: true, completion: nil)
                })
            }
            self.navigationController?.present(vc, animated: true, completion: nil)
        }else if chatDict.type == "video" {
            if let videoURL = URL(string: "\(chatDict.message)") {
                let player = AVPlayer(url: videoURL)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    if let isPLayer = playerViewController.player {
                        isPLayer.play()
                    }
                }
            }
        }
    }
    //MARK: Delegates
    //MARK: Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionArray.count
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let label = UILabel()
        label.frame = CGRect(x: 0.0, y: 10.0, width: self.view.frame.width, height: 30)
        label.text = sectionArray[section]
        label.textAlignment = .center
        label.font = UIFont(name: "Montserrat-Regular", size: 12)
        label.backgroundColor = .clear
        label.textColor = UIColor(red: 108/255, green: 108/255, blue: 108/255, alpha: 1.0)
        label.transform = CGAffineTransform.init(rotationAngle: (-CGFloat(Double.pi)))
        return label
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatArray[sectionArray[section]]?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let chatDict = chatArray[sectionArray[indexPath.section]]?[indexPath.row] else {
            return UITableViewCell()
        }
        if chatDict.senderID ==  HSRealTimeMessagingLIbrary.HSCurrentUserID() {
            if chatDict.type == "text" {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_MyMessage", for: indexPath) as? Cell_MyMessage else { return UITableViewCell.init(style: .default, reuseIdentifier: "Cell_MyMessage")}
                cell.lbl_Message.text = "\(chatDict.message)"
                cell.lbl_Time.text = "\(self.function_ReturnDate(chatDict.time))"
                cell.transform = CGAffineTransform.init(rotationAngle: (-CGFloat(Double.pi)))
                return cell
            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_MyMediaMessage", for: indexPath) as? Cell_MyMediaMessage else {
                    return UITableViewCell.init(style: .default, reuseIdentifier: "Cell_MyMediaMessage")}
                cell.lbl_Time.text = "\(self.function_ReturnDate(chatDict.time))"
                cell.btn_OpenMedia.tag = indexPath.row
                cell.delegate = self
                if chatDict.type == "image" {
                    cell.btn_OpenMedia.setTitle("", for: .normal)
                    cell.btn_OpenMedia.sd_setBackgroundImage(with: URL(string: chatDict.message), for: .normal)
                    
                } else if chatDict.type == "video" {
                    cell.btn_OpenMedia.setTitle("Video", for: .normal)
                    cell.btn_OpenMedia.sd_setBackgroundImage(with: URL(string: ""), for: .normal)

                } else {
                    cell.btn_OpenMedia.setTitle("", for: .normal)
                    cell.btn_OpenMedia.sd_setBackgroundImage(with: URL(string: ""), for: .normal)
                }
                cell.btn_OpenMedia.addTarget(cell, action: #selector(cell.btnOpenMediaClicked(_:)), for: .touchUpInside)
                cell.transform = CGAffineTransform.init(rotationAngle: (-CGFloat(Double.pi)))
                return cell
            }
        } else {
            if chatDict.type == "text" {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_OtherMessage", for: indexPath) as? Cell_OtherMessage else { return UITableViewCell.init(style: .default, reuseIdentifier: "Cell_OtherMessage")}
                cell.lbl_Message.text = "\(chatDict.message)"
                cell.lbl_Time.text = "\(self.function_ReturnDate(chatDict.time))"
                cell.transform = CGAffineTransform.init(rotationAngle: (-CGFloat(Double.pi)))
                return cell
            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell_OtherMediaMessage", for: indexPath) as? Cell_OtherMediaMessage else { return UITableViewCell.init(style: .default, reuseIdentifier: "Cell_OtherMediaMessage")}
                cell.lbl_Time.text = "\(self.function_ReturnDate(chatDict.time))"
                cell.btn_OpenMedia.tag = indexPath.row
                cell.delegate = self
                if chatDict.type == "image" {
                    cell.btn_OpenMedia.setTitle("", for: .normal)
                     cell.btn_OpenMedia.sd_setBackgroundImage(with: URL(string: chatDict.message), for: .normal)
                } else if chatDict.type == "video" {
                    cell.btn_OpenMedia.setTitle("Video", for: .normal)
                    cell.btn_OpenMedia.sd_setBackgroundImage(with: URL(string: ""), for: .normal)
                } else {
                    cell.btn_OpenMedia.setTitle("", for: .normal)
                    cell.btn_OpenMedia.sd_setBackgroundImage(with: URL(string: ""), for: .normal)
                }
                cell.btn_OpenMedia.addTarget(cell, action: #selector(cell.btnOpenMediaClicked(_:)), for: .touchUpInside)
                cell.transform = CGAffineTransform.init(rotationAngle: (-CGFloat(Double.pi)))
                return cell
            }
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.width/8
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? Cell_MyMessage {
            cell.functionAddGradient()
        }
    }
    var isserviceCallForNexrMessgae = Bool()
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        //        if self.chatArray.count >= 10 {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if maximumOffset - currentOffset <= 10.0 {
            if !isserviceCallForNexrMessgae{
                self.isserviceCallForNexrMessgae = true
                if let isMsgID =  self.chatArray[self.sectionArray.last ?? ""]?.last?.msgID {
                    
                    HSRealTimeMessagingLIbrary.function_GetAllMessages(self.chatDetail.chatID, isMsgID) { (messages) in
                        
                        //                        self.tableData.append(contentsOf: messages.sorted(by: {$0.time > $1.time }))
                        //                        self.tableView.reloadData()
                        let sortedMessages = messages.sorted(by: {$0.time > $1.time })
                        for message in sortedMessages {
                            let date = Convertor.convertDate(timeStamp: UInt(message.time), toFormat: "dd/MM/yyyy", sendDay: true)
                            if !self.sectionArray.contains(date) {
                                self.sectionArray.append(date)
                                self.chatArray[date] = [message]
                            } else {
                                self.chatArray[date]?.append(message)
                            }
                            self.tableView.reloadData()
                            self.isserviceCallForNexrMessgae = false
                        }
                    }
                }
            }
        }//        }
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {

    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true) {
            if let isImage = info[.originalImage]  {
                self.view_TextView.activityIndicator.isHidden = false
                HSRealTimeMessagingLIbrary.function_SendImageMessage(isImage as! UIImage, isSenderName: "\(HSRealTimeMessagingLIbrary.HSCurrentUserName())", isReciverName: "\(self.chatDetail.name)", isSenderID: HSRealTimeMessagingLIbrary.HSCurrentUserID(), isReciverID: self.chatDetail.id, isSuccess: {
                    self.view_TextView.activityIndicator.isHidden = true
                }, isError: { (message) in
                    self.view_TextView.activityIndicator.isHidden = true
                    self.function_Pop("Unable to share the image, Please try again later.")
                })
            } else if let isURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
                self.view_TextView.activityIndicator.isHidden = false
                self.sendVdeoWitURL(url: isURL)
            }else if let asset =  info[UIImagePickerController.InfoKey.phAsset] as? PHAsset {
                asset.getURL { (tempPath) in
                    DispatchQueue.main.async{
                        self.view_TextView.activityIndicator.isHidden = false
                        self.sendVdeoWitURL(url: tempPath!)
                    }
                }
            }else if let ref =  info[UIImagePickerController.InfoKey.referenceURL] as? URL {
                let res = PHAsset.fetchAssets(withALAssetURLs: [ref], options: nil)
                guard let asset = res.firstObject else { return}
                asset.getURL { (tempPath) in
                    DispatchQueue.main.async{
                        self.view_TextView.activityIndicator.isHidden = false
                        self.sendVdeoWitURL(url: tempPath!)
                    }
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
    func sendVdeoWitURL(url : URL){
        HSRealTimeMessagingLIbrary.function_SendVideoMessage(url, isSenderName: "\(HSRealTimeMessagingLIbrary.HSCurrentUserName())", isReciverName: "\(self.chatDetail.name)", isSenderID: HSRealTimeMessagingLIbrary.HSCurrentUserID(), isReciverID: self.chatDetail.id, isSuccess: {
            self.view_TextView.activityIndicator.isHidden = true
        }, isError: { (message) in
            self.view_TextView.activityIndicator.isHidden = true
            self.function_Pop("Unable to share the video, Please try again later.")
        })
    }
    //MARK: Functions
    func function_ReturnDate(_ timeStamp: UInt64) -> String {
        let date = Date.init(timeIntervalSince1970: Double(timeStamp))
        let formater = DateFormatter.init();
        formater.dateFormat = "hh:mm a"
        return formater.string(from: date)
    }
    func function_Pop(_ message: String) {
        let alert = UIAlertController.init(title: "ClubBooks", message: "\(message)", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "alert.cancel".localized, style: .default, handler: nil))
        self.navigationController?.present(alert, animated: true, completion: nil)
    }
    func function_LoadData() {
        HSRealTimeMessagingLIbrary.function_GetFirstTenMessages("\(self.chatDetail.chatID)") { (message) in
            var strDateFormat = "MM/dd/yyyy"
            if ManageLocalization.deviceLang != "en"{
                strDateFormat = "dd/MM/YYYY"
            }
            let date = Convertor.convertDate(timeStamp: UInt(message.time), toFormat: strDateFormat, sendDay: true)
            if !self.sectionArray.contains(date) {
                self.sectionArray.insert(date, at: 0)
                self.chatArray[date] = [message]
            } else {
                self.chatArray[date]?.insert(message, at: 0)
            }
            self.tableView.reloadData()
            if self.sectionArray.count != 0 {
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
            }
        }
    }
}
class Convertor {
    class func convertDate(timeStamp: UInt, toFormat: String = "hh:mm a", addDay: Bool = false, sendDay: Bool = false) -> String {
        let dateFormatter = DateFormatter()
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp))
        let hourFormat = "hh:mm a"
        dateFormatter.dateFormat = hourFormat
        
        if sendDay,
            NSCalendar.current.isDateInToday(date) {
            return "lbl.title.today".localized
        } else if sendDay,
            NSCalendar.current.isDateInYesterday(date) {
            return "lbl.title.yesterday".localized
        } else if addDay,
            NSCalendar.current.isDateInToday(date) {
            return "\("lbl.title.today".localized), \(dateFormatter.string(from: date))"
        } else if addDay,
            NSCalendar.current.isDateInYesterday(date) {
            return "\("lbl.title.yesterday".localized), \(dateFormatter.string(from: date))"
        } else {
            dateFormatter.dateFormat = toFormat
            return dateFormatter.string(from: date)
        }
    }
    
    class func convertDate(dateInString: String, fromFormat: String = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", toFormat: String = "hh:mm a") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        let date1 = dateFormatter.date(from: dateInString)!
        
        let diffBtwDatesInMin = Calendar.current.dateComponents([.minute], from: date1, to: Date()).minute ?? 0
        if diffBtwDatesInMin < 60 {
            return "\(diffBtwDatesInMin) min ago"
        }
        dateFormatter.dateFormat = toFormat
        return dateFormatter.string(from: date1)
    }
    
    class func convertStringToDate(dateInString: String, fromFormat: String = "yyyy-MM-dd") -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fromFormat
        return dateFormatter.date(from: dateInString)!
    }
}
