//
//  Cell_MyMessage.swift
//  Retailshop
//
//  Created by cis on 27/06/18.
//  Copyright © 2018 Famousming Software. All rights reserved.
//

import UIKit

class Cell_MyMessage: UITableViewCell {
    @IBOutlet var lbl_Message: UILabel!
    @IBOutlet var view_BackGround: UIView!
    @IBOutlet var lbl_Time: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        DispatchQueue.main.async {
            self.view_BackGround.layer.cornerRadius = 12.5
            self.view_BackGround.clipsToBounds = true
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    func functionAddGradient() {
        DispatchQueue.main.async {
            if let isLayer = self.view_BackGround.layer.sublayers, isLayer[0] is CAGradientLayer {isLayer[0].removeFromSuperlayer()}
            let gradient = CAGradientLayer()
            gradient.frame = self.view_BackGround.bounds
            gradient.colors = [UIColor.init(red: 65/255, green: 196/255, blue: 221/255, alpha: 1.0).cgColor, UIColor.init(red: 10/255, green: 124/255, blue: 175/255, alpha: 1.0).cgColor]
            self.view_BackGround.layer.insertSublayer(gradient, at: 0)
        }
    }
}
