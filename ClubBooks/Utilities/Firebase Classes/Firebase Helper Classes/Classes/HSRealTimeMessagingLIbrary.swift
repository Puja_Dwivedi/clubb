//
//  HSFireBase.swift
//  Blast
//
//  Created by cis on 02/08/18.
//  Copyright © 2018 cis. All rights reserved.
//

import Foundation
import AVKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import CoreLocation
import GeoFire
class HSUser {
    var name: String = ""
    var gender: UInt64 = 0
    var email: String = ""
    var id: UInt64 = 0
    var picture: String = ""
    var FCMToken : String = ""
    init(isName: String, gender: UInt64, isEmail: String, isID: UInt64, isPicture: String, isFCMToken: String) {
        self.name = isName
        self.gender = gender
        self.email = isEmail
        self.id = isID
        self.picture = isPicture
        self.FCMToken = isFCMToken
    }
}
class HSChatUser {
    var chatID: String = ""
    var id: UInt64 = 0
    var name: String = ""
    var time : UInt64 = 0
    init(isChatID: String, isID: UInt64, isName: String, time : UInt64 = 0) {
        self.chatID = isChatID
        self.id = isID
        self.name = isName
        self.time = time
    }
}
class HSMessage {
    var message: String = ""
    var type: String = ""
    var time: UInt64 = 0
    var senderName: String = ""
    var senderID: UInt64 = 0
    var chatID: String = ""
    var msgID: String = ""
    init(isMessage: String, isType: String, isTime: UInt64, isSenderName: String, isSenderID: UInt64, isChatID: String, isMsgID: String) {
        self.message = isMessage
        self.type = isType
        self.time = isTime
        self.senderName = isSenderName
        self.senderID = isSenderID
        self.chatID = isChatID
        self.msgID = isMsgID
    }
}
class HSRealTimeMessagingLIbrary {
    //Private Function
    class func function_CreatChatID(_ isSender: UInt64, _ isReceiver: UInt64) -> String {
        if isSender > isReceiver { return "\(isSender)_\(isReceiver)" } else { return "\(isReceiver)_\(isSender)" }
    }
    private class func function_UpdateChatList( _ isSenderName: String, _ isReciverName: String, _ isSenderID: UInt64, _ isReciverID: UInt64, _ isChatID: String) {
        let time = UInt64(Date().timeIntervalSince1970)
        for i in 0...1 {
            if i == 0 {
                let user:[String:Any] = [
                    "name":"\(isReciverName)",
                    "id":isReciverID,
                    "chatID":"\(isChatID)",
                    "time":time
                ]
                Database.database().reference().child("users").child("\(isSenderID)").child("chat").child("\(isReciverID)").setValue(user)
            } else {
                let user:[String:Any] = [
                    "name":"\(isSenderName)",
                    "id":isSenderID,
                    "chatID":isChatID,
                    "time":time
                ]
                Database.database().reference().child("users").child("\(isReciverID)").child("chat").child("\(isSenderID)").setValue(user)
            }
        }
    }
    //CreatUser
    class func function_CreatAndUpdateUser(_ HSUser: HSUser) {
        let user:[String:Any] = [
            "email":["key":"\(HSUser.email)"],
            "name":["key":"\(HSUser.name)"],
            "gender":["key":"\(HSUser.gender)"],
            "picture":["key":"\(HSUser.picture)"],
            "status":["key":false],
            "id":"\(HSUser.id)",
            "FCMToken" : "\(HSUser.FCMToken)"
        ]
        Database.database().reference().child("users").child("\(HSUser.id)").child("credentials").setValue(user) { (error, dataRef) in
            if let _ = error {
                self.function_CreatAndUpdateUser(HSUser)
            } else {
                let updateAllUserList:[String:Any] = [
                    "email":HSUser.email,
                    "name":HSUser.name,
                    "gender":HSUser.gender,
                    "picture":HSUser.picture,
                    "id":HSUser.id,
                    "FCMToken" : HSUser.FCMToken
                ]
                Database.database().reference().child("allChatUsers").child("\(HSUser.id)").setValue(updateAllUserList)
            }
        }
    }
    //LoadChat
    class func function_LoadChatUsers(isID: UInt64, success:@escaping(HSChatUser)-> Void) {
        Database.database().reference().child("users").child("\(isID)").child("chat").observe(.childAdded) { (dataSnap) in
            if let isUser = dataSnap.value as? [String:Any] {
                success(HSChatUser.init(isChatID: "\(isUser["chatID"] as? String ?? "")", isID: isUser["id"] as? UInt64 ?? 0, isName: "\(isUser["name"] as? String ?? "")", time: isUser["time"] as? UInt64 ?? 0))
            }
        }
    }
    //LoadMessages
    class func function_GetFirstTenMessages(_ chatID: String, _ success:@escaping(HSMessage)-> Void) {
        Database.database().reference().child("chat").child("\(chatID)").queryOrderedByKey().queryLimited(toLast: 20).observe(.childAdded) { (datashot) in
            if let isData = datashot.value as? [String:Any] {
                let message = HSMessage.init(isMessage: "\(isData["message"] as? String ?? "")", isType: "\(isData["type"] as? String ?? "")", isTime: isData["time"] as? UInt64 ?? 0, isSenderName: "\(isData["name"] as? String ?? "")", isSenderID: isData["id"] as? UInt64 ?? 0, isChatID: "\(isData["chatID"] as? String ?? "")", isMsgID: "\(isData["msgID"] as? String ?? "")")
                success(message)
            }
        }
    }
    class func function_GetAllMessages(_ chatID: String, _ lastMsgID: String, _ success:@escaping([HSMessage])-> Void) {
        Database.database().reference().child("chat").child("\(chatID)").queryOrderedByKey().queryLimited(toLast: 20).queryEnding(atValue: "\(lastMsgID)").observeSingleEvent(of: .value) { (datashot) in
            if let isData = datashot.value as? [String:Any], isData.count != 0 {
                var tempData:[HSMessage] = []
                for item in isData.keys {
                    if item != "\(lastMsgID)" {
                        if let isMessage = isData["\(item)"] as? [String:Any], isMessage.isEmpty != true {
                            let message = HSMessage.init(isMessage: "\(isMessage["message"] as? String ?? "")", isType: "\(isMessage["type"] as? String ?? "")", isTime: isMessage["time"] as? UInt64 ?? 0, isSenderName: "\(isMessage["name"] as? String ?? "")", isSenderID: isMessage["id"] as? UInt64 ?? 0, isChatID: "\(isMessage["chatID"] as? String ?? "")", isMsgID: "\(isMessage["msgID"] as? String ?? "")")
                            tempData.append(message)
                        }
                    }
                }
                success(tempData)
            }
        }
    }
    //SendMessage
    class func function_SendImageMessage(_ isImg: UIImage, isSenderName: String, isReciverName: String, isSenderID: UInt64, isReciverID: UInt64, isSuccess:@escaping()-> Void, isError:@escaping(String)-> Void) {
        if let isImageData = isImg.jpegData(compressionQuality: 0.5) {
            let imgName = "image\(UInt64(Date().timeIntervalSince1970)).png"
            let storageRef = Storage.storage().reference().child("images").child("\(imgName)")
            storageRef.putData(isImageData, metadata: nil) { (metaDataImage, uploadError) in
                if let isUploadError = uploadError {
                    isError(isUploadError.localizedDescription)
                } else {
                    storageRef.downloadURL { (url, linkError) in
                        if let isLinkError = linkError {
                            isError(isLinkError.localizedDescription)
                        } else {
                            let time = UInt64(Date().timeIntervalSince1970)
                            let message:[String:Any] = [
                                "message":"\(url?.absoluteString ?? "")",
                                "type":"image",
                                "time":time,
                                "name":"\(isSenderName)",
                                "id":isSenderID,
                                "chatID":"\(self.function_CreatChatID(isSenderID, isReciverID))",
                                "msgID":"\(time)"
                            ]
                            Database.database().reference().child("chat").child("\(self.function_CreatChatID(isSenderID, isReciverID))").child("\(time)").setValue(message) { (error, dataRefrence) in
                                if let isSomeError = error {
                                    isError(isSomeError.localizedDescription)
                                } else {
                                    sendPushNotification(toID: RV_CheckDataType.getString(anyString: isReciverID as AnyObject), message: "Image",chatID: "\(self.function_CreatChatID(isSenderID, isReciverID))")
                                    self.function_UpdateChatList(isSenderName, isReciverName, isSenderID, isReciverID, "\(self.function_CreatChatID(isSenderID, isReciverID))")
                                    isSuccess()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    class func function_SendVideoMessage(_ isURL: URL, isSenderName: String, isReciverName: String, isSenderID: UInt64, isReciverID: UInt64, isSuccess:@escaping()-> Void, isError:@escaping(String)-> Void) {
        let imgName = "video\(UInt64(Date().timeIntervalSince1970)).mov"
        let storageRef = Storage.storage().reference().child("videos").child("\(imgName)")
        storageRef.putFile(from: isURL, metadata: nil) { (metaDataImage, uploadError) in
            if let isUploadError = uploadError {
                isError(isUploadError.localizedDescription)
            } else {
                storageRef.downloadURL { (url, linkError) in
                    if let isLinkError = linkError {
                        isError(isLinkError.localizedDescription)
                    } else {
                        let time = UInt64(Date().timeIntervalSince1970)
                        let message:[String:Any] = [
                            "message":"\(url?.absoluteString ?? "")",
                            "type":"video",
                            "time":time,
                            "name":"\(isSenderName)",
                            "id":isSenderID,
                            "chatID":"\(self.function_CreatChatID(isSenderID, isReciverID))",
                            "msgID":"\(time)"
                        ]
                        
                        Database.database().reference().child("chat").child("\(self.function_CreatChatID(isSenderID, isReciverID))").child("\(time)").setValue(message) { (error, dataRefrence) in
                            if let isSomeError = error {
                                isError(isSomeError.localizedDescription)
                            } else {
                                sendPushNotification(toID: RV_CheckDataType.getString(anyString: isReciverID as AnyObject), message: "Video",chatID: "\(self.function_CreatChatID(isSenderID, isReciverID))")
                                self.function_UpdateChatList(isSenderName, isReciverName, isSenderID, isReciverID, "\(self.function_CreatChatID(isSenderID, isReciverID))")
                                isSuccess()
                            }
                        }
                    }
                }
            }
        }
    }
    class func function_SendTextMessage(_ isMsg: String, isSenderName: String, isReciverName: String, isSenderID: UInt64, isReciverID: UInt64, isSuccess:@escaping()-> Void, isError:@escaping(String)-> Void) {
        let time = UInt64(Date().timeIntervalSince1970)
        let message:[String:Any] = [
            "message":"\(isMsg)",
            "type":"text",
            "time":time,
            "name":"\(isSenderName)",
            "id":isSenderID,
            "chatID":"\(self.function_CreatChatID(isSenderID, isReciverID))",
            "msgID":"\(time)"
        ]
        Database.database().reference().child("chat").child("\(self.function_CreatChatID(isSenderID, isReciverID))").child("\(time)").setValue(message) { (error, dataRefrence) in
            if let isSomeError = error {
                isError(isSomeError.localizedDescription)
            } else {
               sendPushNotification(toID: RV_CheckDataType.getString(anyString: isReciverID as AnyObject), message: isMsg,chatID: "\(self.function_CreatChatID(isSenderID, isReciverID))")
                self.function_UpdateChatList(isSenderName, isReciverName, isSenderID, isReciverID, "\(self.function_CreatChatID(isSenderID, isReciverID))")
                isSuccess()
            }
        }
    }
    // Send notifications
    class func sendPushNotification(toID : String, message : String, chatID : String)
    {
        fcmKeyFor(userId: toID) { (fcmKey) in
            let sender = PushNotificationSender()
            let title = String(format: "%@", AppTheme.sharedInstance.currentUserData[0].Name)
            let dict : [String : String] = ["alert": title, "sound" : "default"]
            let dictBody : [String : AnyObject] = ["aps":dict as AnyObject]
            
            sender.sendPushNotification(to: fcmKey, title: title, body: message, data: dictBody,chatID: chatID)
        }
    }
    //Update Online Status
    class func function_GetOnlineStatus(_ isID: UInt64, _ Success:@escaping(Bool)-> Void) {
        Database.database().reference().child("users").child("\(isID)").child("credentials").child("status").observe(.value) { (datashot) in
            if let isData = datashot.value as? [String: Any], isData.isEmpty == false {
                Success(isData["key"] as? Bool ?? false)
            }
        }
    }
    class func function_UpdateOnlineStatus(_ isID: UInt64, _ isOnline: Bool) {
        Database.database().reference().child("users").child("\(isID)").child("credentials").child("status").setValue(["key":isOnline])
    }

    class func function_UpdateUserLocation( location : CLLocation, forID : UInt64) {
      let geoFire =  GeoFire.init(firebaseRef: Database.database().reference().child("Geolocs"))
      geoFire.setLocation(location, forKey: RV_CheckDataType.getString(anyString: forID as AnyObject))
    }
    class func function_LoadNearByUsers(forID: UInt64, success:@escaping(HSUser, CLLocation)-> Void) {
         let geoFire =  GeoFire.init(firebaseRef: Database.database().reference().child("Geolocs"))
        if  UserDefaults.standard.value(forKey: "current_latitude") as? String != nil{
        let userLat = UserDefaults.standard.value(forKey: "current_latitude") as! String
        let userLong = UserDefaults.standard.value(forKey: "current_longitude") as! String
        
        let location:CLLocation = CLLocation(latitude: CLLocationDegrees(Double(userLat)!), longitude: CLLocationDegrees(Double(userLong)!))
        var myQuery: GFQuery?
        myQuery = geoFire.query(at: location, withRadius: 10)
        myQuery?.observe(.keyEntered, with: { (key, location) in
            // print("KEY:\(String(describing: key)) and location:\(String(describing: location))")
            if key != RV_CheckDataType.getString(anyString: forID as AnyObject)
            {
                let ref = Database.database().reference().child("allChatUsers").child(key)
                
                ref.observeSingleEvent(of: .value, with: { (snapshot) in
                    let dic = RV_CheckDataType.getDictionary(anyDict: snapshot.value as AnyObject)
                    
                    if dic.allValues.count > 0{
                        let data = snapshot.value as! [String: Any]
                        let name = data["name"]!
                        let email = data["email"]!
                        let firebaseToken = data["FCMToken"]
                        let gender = data["gender"]
                        // let longitude = data["current_longitude"]
                        success(HSUser.init(isName: name as! String, gender: UInt64(gender as? Int ?? 0), isEmail: email as! String, isID: UInt64(key) ??  0, isPicture: "", isFCMToken: firebaseToken as! String), location)
                        
                    }
                    
                })
                
            }
        })
            
        }else{
            
        }
    }
    //User
    class func HSCurrentUserID() -> UInt64 {
        if AppTheme.sharedInstance.currentUserData.count > 0{
            return UInt64(AppTheme.sharedInstance.currentUserData[0].UserID) ?? 0
        }else{
            return 0
        }
    }
    class func HSCurrentUserName() -> String {
        return AppTheme.sharedInstance.currentUserData[0].Name
    }
    class func fcmKeyFor(userId : String, completion: @escaping (String) -> Swift.Void)
    {
        Database.database().reference().child("allChatUsers").child("\(userId)").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                //fcmKey
                let data = snapshot.value as! [String: AnyObject]
                let dictData = RV_CheckDataType.getDictionary(anyDict: data as AnyObject)
                let chatId = dictData.allKeys as NSArray
                if chatId.contains("FCMToken")
                {
                    let fcmvToken = RV_CheckDataType.getString(anyString: dictData.value(forKey: "FCMToken") as AnyObject)
                    completion(fcmvToken)
                }
                else { completion("") }
            } else {
                completion("")
            }
        })
    }
}
