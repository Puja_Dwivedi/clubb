//
//  HSExpandableTextView.swift
//  HSRealTimeMessaging
//
//  Created by cis on 09/04/19.
//  Copyright © 2019 Cis. All rights reserved.
//

import UIKit

class HSExpandableTextView: UIView, UITextViewDelegate {
    //MARK: Variable
    @IBOutlet var contentView: UIView!
    @IBOutlet var btnMedia : UIButton!
    @IBOutlet var btnMediaWidthConstraints : NSLayoutConstraint!
    @IBOutlet var activityIndicator: UIView!
    @IBOutlet var txtView_Msg: UITextView!
    var sendMessage:((String)->Void)! = nil
    var sendMedia:(()->Void)! = nil
    //MARK: Life Cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.function_init()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.function_init()
    }
    //MARK: Button
    @IBAction func btn_Send(_ sender: UIButton) {
        if let isString = self.txtView_Msg.text, isString.replacingOccurrences(of: " ", with: "") != "" {
            self.txtView_Msg.text = ""
            self.sendMessage("\(isString)")
        }
    }
    @IBAction func btn_SelectMedia(_ sender: UIButton) {
        self.sendMedia()
    }
    //MARK: Delegate
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" { textView.resignFirstResponder() }
        return true
    }
    //MARK: Function
    func function_init() {
        Bundle.main.loadNibNamed("HSExpandableTextView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth ]
        DispatchQueue.main.async {
            self.txtView_Msg.layer.cornerRadius = 5
            self.txtView_Msg.clipsToBounds = true
            self.txtView_Msg.textContainer.heightTracksTextView = true
            self.txtView_Msg.isScrollEnabled = false
        }
    }
}
