//
//  PushNotificationSender.swift
//  ezVerifi
//
//  Created by cis on 06/06/19.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation
import UIKit
class PushNotificationSender {
    func sendPushNotification(to token: String, title: String, body: String, data : [String : AnyObject], chatID : String) {
        let urlString = "https://fcm.googleapis.com/fcm/send"
        let url = NSURL(string: urlString)!
        let paramString: [String : Any] = ["to" : token,
                                           "notification" : ["title" : title, "body" : body],
                                           "data" : ["body" : body,
                                                     "title": title,
                                                     "user_ID" : AppTheme.sharedInstance.currentUserData[0].UserID,
                                                     "username" : AppTheme.sharedInstance.currentUserData[0].Name,
                                                     "chatID" : chatID]
        ]
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject:paramString, options: [.prettyPrinted])
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("key=AAAA6cQhBBg:APA91bHbpc_VyCqPieW1xrQumADFGeCQOZ_16Z4VZgEd8BqeXSmgqjcbOhaGBb5A_wb4KI27o_a3F3-gAOzJdMVBT2z0pPJEP6dC607BiyvWA8wpM4XiD0EW3CuJs9c8DB_gniFUZTyc", forHTTPHeaderField: "Authorization")
        let task =  URLSession.shared.dataTask(with: request as URLRequest)  { (data, response, error) in
            do {
                if let jsonData = data {
                    if let jsonDataDict  = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: AnyObject] {
                        NSLog("Received data:\n\(jsonDataDict))")
                    }
                }
            } catch let err as NSError {
                print(err.debugDescription)
            }
        }
        task.resume()
    }
}
