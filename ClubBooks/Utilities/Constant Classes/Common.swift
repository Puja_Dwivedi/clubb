//
//  Common.swift
//  ClikaGram
//
//  Created by cis on 06/01/18.
//  Copyright © 2018 cis. All rights reserved.
//

import Foundation
import UIKit

let appDelegate = UIApplication.shared.delegate as! AppDelegate

class Common {
    
    static func addToUserDefault(object : Any , key : String){
        let userDefault = UserDefaults.standard
        userDefault.set(object, forKey: key)
        userDefault.synchronize()
    }
    
    static func getFromUserDefaults(_ pstrKey:String) -> Any! {
        let objUserDefaults = UserDefaults.standard
        guard objUserDefaults.object(forKey: pstrKey) == nil else {
            return objUserDefaults.object(forKey: pstrKey)! as AnyObject?
        }
        return ""
    }
    
    static func removeFromDefault(key : String){
        let userDefault = UserDefaults.standard
        userDefault.removeObject(forKey: key)
        userDefault.synchronize()
    }
}
