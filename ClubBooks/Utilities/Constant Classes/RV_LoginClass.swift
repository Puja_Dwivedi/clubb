//
//  RV_LoginClass.swift
//  BudSeeker
//
//  Created by cis on 22/11/17.
//  Copyright © 2018 CIS. All rights reserved.
//

import Foundation
import UIKit


class RV_LoginClass: NSObject
{
    // MARK: - Shared Instance
    static let sharedInstance: RV_LoginClass = {
        let instance = RV_LoginClass()
        // setup code
        return instance
    }()
    
    // MARK: - Initialization Method
    override init() {
        super.init()
    }
    
    struct Registration {
        var UserName : String
        var Name : String
        var email : String
        var password : String
        var AgeGroup : String
        var City : String
        var isTermsAndConditionAccepted : NSInteger?
        var Gender : String
        
        init(regDict : [String : AnyObject]) {
            /*
             full_name: String, email: String, password: String, confirmPassword: String, phone_number: String, isTermsAndConditionAccepted : NSInteger
             */
            self.UserName = RV_CheckDataType.getString(anyString: regDict[KEY.SignUp.UserName]!)
            self.Name = RV_CheckDataType.getString(anyString: regDict[KEY.SignUp.Name]!)
            self.email = RV_CheckDataType.getString(anyString: regDict[KEY.SignUp.EmailId] as AnyObject)
            self.password = RV_CheckDataType.getString(anyString: regDict[KEY.SignUp.Password] as AnyObject)
            self.isTermsAndConditionAccepted = RV_CheckDataType.getInteger(anyString: regDict[KEY.SignUp.TermsAndCondition] as AnyObject)
            self.City = RV_CheckDataType.getString(anyString: regDict[KEY.SignUp.City] as AnyObject)
            self.Gender = RV_CheckDataType.getString(anyString: regDict[KEY.SignUp.Gender] as AnyObject)
            self.AgeGroup = RV_CheckDataType.getString(anyString: regDict[KEY.SignUp.AgeGroup] as AnyObject)
        }
    }
    
    class func loginAction(txtUsername : UITextField, userFieldType : String, txtPassword : UITextField, completion: @escaping (_ result: String) -> Void)
    {
        var strError : String = ""
        
        if RV_Validations.isEmptyField(testStr: txtUsername.text!)
        {
            if userFieldType == "UserName"
            {
                strError = ERROR.LoginSignUpError.UsernameRequired
            }
            else if userFieldType == "Email address"
            {
                strError = ERROR.LoginSignUpError.EmailAddressRequired
            }
        }
        else if userFieldType == "Email address" && !RV_Validations.isValidEmail(testStr: txtUsername.text!)
        {
            strError = ERROR.LoginSignUpError.InvalidEmailId
        }
        else if RV_Validations.isEmptyField(testStr: txtPassword.text!)
        {
            strError = ERROR.LoginSignUpError.PasswordRequired
        }
        else if !RV_Validations.checkTextSufficientComplexity(text: txtPassword.text!)
        {
            strError = "Incorrect password.\nPassword must contains at least 6 characters, including UPPER, lower case and a number." //and a special character (i.e. @#$%&)"
        }
        
        completion(strError)
    }
    
    
    class func forgotPasswordAction(emailAddress : String, completion: @escaping (_ result: String) -> Void)
    {
        var strError = ""
        if RV_Validations.isEmptyField(testStr: emailAddress)
        {
            strError = ERROR.LoginSignUpError.EmailAddressRequired
        }
        else if !RV_Validations.isValidEmail(testStr: emailAddress)
        {
            strError = ERROR.LoginSignUpError.InvalidEmailId
        }

        completion(strError)
    }
}
