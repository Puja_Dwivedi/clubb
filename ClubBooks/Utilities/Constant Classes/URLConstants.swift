//
//  URLConstants.swift
//  BudSeeker
//
//  Created by cis on 21/11/17.
//  Copyright © 2018 CIS. All rights reserved.
//

import Foundation

struct APPURL {
    struct Domains {
        static let Dev = "https://clubbooks.com/"
        static let Live = "https://clubbooks.com/"
    }
    private  struct Routes {
        static let Api = "api/"
    }
    
    #if DEBUG
        private  static let BaseURL = Domains.Dev + Routes.Api
    #else
        private  static let BaseURL = Domains.Live + Routes.Api
    #endif
    
    //MARK: -- Common Services --
    static var BaseUrl : String {
        return BaseURL
    }
    static var getAccessToken : String {
        return BaseURL + "getAPIAccessToken"
    }
    static var Login : String {
        return BaseURL + "login"
    }
    static var FacebookLogin: String {
        return BaseURL  + "socialLogin"
    }
    static var UserRegistration: String {
        return BaseURL  + "userRegistration"
    }
    static var resendVerificationEmail: String {
        return BaseURL  + "resendEmailActivationLink"
    }
    static var ForgotPasswordProfile : String {
        return BaseURL + "sendPasswordResetLink"
    }
    static var VerificationCode : String {
        return BaseURL + "verifyMobileOTP"
    }
    static var ChangePassword: String {
        return BaseURL  + "changePassword"
    }
    static var Logout : String {
        return BaseURL + "logout"
    }
    static var deleteAccount : String {
        return BaseURL + "deleteUserWithData"
    }
    static var PostBook : String {
        return BaseURL + "postBook"
    }
    static var PostMeetUps : String {
        return BaseURL + "postMeetUp"
    }
    static var PostchatonBooks : String {
        return BaseURL + "postChatsOnBook"
    }
    static var PostchatonTopic : String {
        return BaseURL + "postChatsOnTopic"
    }
    static var PostExam : String {
        return BaseURL + "postExam"
    }
    static var PostQuiz : String {
        return BaseURL + "postQuiz"
    }
    static var PostNotes : String {
        return BaseURL + "postClassNote"
    }
    static var getAllPost : String {
        return BaseURL + "getAllPosts"
    }
    static var getAllBooks : String {
        return BaseURL + "getBooks"
    }
    static var getChatsOnBook : String {
        return BaseURL + "getChatsOnBooks"
    }
    static var getChatsOnTopic : String {
        return BaseURL + "getChatsOnTopics"
    }
    static var getAllExams : String {
        return BaseURL + "getExams"
    }
    static var getAllQuizz : String {
        return BaseURL + "getQuizzes"
    }
    static var getAllNotes : String {
        return BaseURL + "getClassNotes"
    }
    static var getAllMeetups : String {
        return BaseURL + "getMeetUps"
    }
    static var getBookDetails : String{
        return BaseURL + "getBookDetails"
    }
    static var getExamDetails : String{
        return BaseURL + "getExamDetails"
    }
    static var getQuizDetails : String{
        return BaseURL + "getQuizDetails"
    }
    static var getNotesDetails : String{
        return BaseURL + "getClassNoteDetails"
    }
    static var getMeetupDetails : String{
        return BaseURL + "getMeetUpDetails"
    }
    static var getBookChatHistory : String{
        return BaseURL + "getChatOnBooksHistoryById"
    }
    static var getTopicChatHistory : String{
        return BaseURL + "getChatOnTopicsHistoryById"
    }
    static var postMessageOnBook : String{
        return BaseURL + "postChatMessageOnBooksById"
    }
    static var postMessageOnTopic : String{
        return BaseURL + "postChatMessageOnTopicsById"
    }
    static var postSubmitRating : String{
        return BaseURL + "submitRateAndReview"
    }
    static var getAllUser : String{
        return BaseURL + "getAllUsers"
    }
    static var getFriendList : String{
        return BaseURL + "getBuddies"
    }
    static var getFollower : String{
        return BaseURL + "getFollower"
    }
    static var getFollowing : String{
        return BaseURL + "getFollowing"
    }
    static var getFriendRequest : String{
        return BaseURL + "getFriendsRequests"
    }
    static var acceptFriendRequest : String{
        return BaseURL + "confirmFriendRequest"
    }
    static var rejectFriendRequest : String{
        return BaseURL + "deleteFriendRequest"
    }
    static var getFriendProfile : String{
        return BaseURL + "getFriendProfile"
    }
    static var sendFollowRequest : String{
        return BaseURL + "sendFriendRequest"
    }
    static var unFollow : String{
        return BaseURL + "unfollowFriend"
    }
    static var updateProfile : String{
        return BaseURL + "updateUserProfile"
    }
    static var reserveBook : String{
        return BaseURL + "reservePostById"
    }
    static var releaseBook : String{
        return BaseURL + "releasePostById"
    }
    static var addMoneyToWalletUsingStripeToken : String{
        return BaseURL + "addMoneyByStripeToken"
    }
    static var sendMoneyToFriend : String{
        return BaseURL + "sendMoneyToFrinedByWalletTransfer"
    }
    static var getPendingMoneyRequest : String{
        return BaseURL + "getPendingRequestsOfMoneyFromFriends"
    }
    static var releaseBookAfter48Hours : String{
        return BaseURL + "releasePostIfReservedDateIsOver"
    }
    static var payForPost : String{
        return BaseURL + "payMoneyToPostSeller"
    }
    static var sendMoneyReuestToFriend : String{
        return BaseURL + "requestMoneyFromFriendByWalletTransfer"
    }
    static var getAllUserRating : String{
        return BaseURL + "getRatingAndReviews"
    }
    static var submitRatngForUser : String{
        return BaseURL + "postUserReviewAndRating"
    }
    static var payMoneyToFriend : String{
        return BaseURL + "sendMoneyToFriendByWalletTransfer"
    }
    static var getCurrentWalletBalance : String{
        return BaseURL + "getCurrentWalletAmount"
    }
    static var getNotifications : String{
        return BaseURL + "getNotifications"
    }
    static var deletePostWithData : String{
        return BaseURL + "deletePostWithData"
    }
}

