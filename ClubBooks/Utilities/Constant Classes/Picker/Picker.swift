//
//  Picker.swift
//  AppFis
//
//  Created by cis on 4/19/18.
//  Copyright © 2018 cis. All rights reserved.
//

import UIKit
import SwiftyJSON
protocol GetPickerValueDelagate {
    func getValue(_ value:String, index:Int, iflag:Bool)
}
class Picker: BaseViewController {

    var delegate:GetPickerValueDelagate?
    var index = 0
    var selectedValue = ""
    var data = [String]()
    var isCustomer = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if data.count > 0 {
            self.index = 0
            let dict = data[self.index]
            self.selectedValue = dict
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func action_Cancel(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func action_Done(_ sender: UIBarButtonItem) {
        self.delegate?.getValue(self.selectedValue, index: self.index, iflag: isCustomer)
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension Picker:UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let dict = data[row]
        return ManageLocalization.getLocalaizedString(key: dict)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if data.count > 0 {
            self.index = row
            let dict = data[row]
            self.selectedValue = dict
        }
    }
}
