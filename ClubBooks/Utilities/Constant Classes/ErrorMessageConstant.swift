//
//  ErrorMessageConstant.swift
//  ClubBooks
//
//  Created by cis on 22/11/17.
//  Copyright © 2018 CIS. All rights reserved.
//





import Foundation

struct ERROR {
    
    struct LoginSignUpError {
        static let UsernameRequired = ManageLocalization.getLocalaizedString(key: "alert.usernameRequired")
        static let NameRequired = ManageLocalization.getLocalaizedString(key: "alert.nameRequired")
        static let PasswordShouldbeSame = ManageLocalization.getLocalaizedString(key:"alert.sampasswordRequired")
        static let EmailAddressRequired = ManageLocalization.getLocalaizedString(key: "alert.emailAddressRequired")
        static let PasswordRequired = ManageLocalization.getLocalaizedString(key: "alert.passwordRequired")
        static let InvalidEmailId = ManageLocalization.getLocalaizedString(key: "alert.invalidEmailAddress")
        static let PasswordStrengh = ManageLocalization.getLocalaizedString(key: "alert.passwordCriteria")
        static let SelectGender = ManageLocalization.getLocalaizedString(key: "alert.selectGender")
        static let SelectAgeGroup = ManageLocalization.getLocalaizedString(key: "alert.selectAgeGroup")
        static let SelectCity = ManageLocalization.getLocalaizedString(key: "alert.selectCity")
        static let MobileNoRequired = ManageLocalization.getLocalaizedString(key: "alert.mobileNumber")
        static let SchoolName = ManageLocalization.getLocalaizedString(key: "alert.schoolname")
        static let MobileNoStrength = ManageLocalization.getLocalaizedString(key: "alert.mobileNumberStrength")
        static let FavBookNameRequired = ManageLocalization.getLocalaizedString(key: "alert.favBookName")
    }
}
