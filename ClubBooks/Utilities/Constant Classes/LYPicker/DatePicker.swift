//
//  DatePicker.swift
//  Bonauf
//
//  Created by cis on 4/21/18.
//  Copyright © 2018 cis. All rights reserved.
//

import UIKit

protocol GetDateDelagate {
    func getDate(_ date:String, index:Int)
}

class DatePicker: BaseViewController {
    var pickerType : PickerType?
    @IBOutlet weak var datePicker: UIDatePicker!
    var delegate:GetDateDelagate?
    var index = 0
    var selectedValue = ""
    var dateFormatStr = ""
    var isExpDate = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let lang = AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.selectedLanguage) as! String
        let loc = Locale(identifier: lang)
        self.datePicker.locale = loc
        if #available(iOS 13.4, *) {
            self.datePicker.preferredDatePickerStyle = .wheels
        }
        if pickerType == PickerType.Date {
            self.datePicker.datePickerMode = .date
            self.dateFormatStr = "yyyy-MM-dd"
            self.isExpDate = true
        }else{
            self.datePicker.datePickerMode = .time
            self.dateFormatStr = "hh:mm a"
            self.isExpDate = false
        }
        if isExpDate == true {
            self.datePicker.minimumDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func action_Cancel(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func action_Done(_ sender: UIBarButtonItem) {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormatStr
        self.selectedValue = formatter.string(from: datePicker.date)
        self.delegate?.getDate(self.selectedValue, index: self.index)
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
