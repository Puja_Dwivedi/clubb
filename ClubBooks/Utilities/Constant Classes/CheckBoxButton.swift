//
//  CheckBoxButton.swift
//  ClubBooks
//
//  Created by cis on 27/02/19.
//  Copyright © 2019 CIS. All rights reserved.
//

import UIKit

class CheckBoxButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.isSelected = false
        self.setBackgroundImage(#imageLiteral(resourceName: "icon_uncheckbox"), for: .normal)
        self.setBackgroundImage(#imageLiteral(resourceName: "icon_checked"), for: .selected)
        self.addTarget(self, action: #selector(btnClicked(_:)), for: .touchUpInside)
    }
    @IBAction func btnClicked ( _ sender : UIButton){
        if self.isSelected == false {
            isSelected = true
        }else{
            isSelected = false
        }
    }
    
}
