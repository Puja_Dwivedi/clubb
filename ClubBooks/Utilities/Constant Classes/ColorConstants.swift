//
//  ColorConstants.swift
//  ClubBooks
//
//  Created by cis on 21/11/17.
//  Copyright © 2018 CIS. All rights reserved.
//

import Foundation
import UIKit

struct AppColor {
    
    private struct Alphas {
        static let Opaque = CGFloat(1)
        static let SemiOpaque = CGFloat(0.8)
        static let SemiTransparent = CGFloat(0.5)
        static let Transparent = CGFloat(0.3)
    }
    
    static let appPrimaryColor =  UIColor.init(red: 252/255.0, green: 204/255.0, blue: 02/255.0, alpha: 1.0)
    static let appSecondaryColor =  UIColor.lightGray.withAlphaComponent(Alphas.Transparent)
    static let appBlackColor = 0x414141
    
    struct TextColors {
        static let Error = AppColor.appSecondaryColor
        static let Success = UIColor(red: 0.1303, green: 0.9915, blue: 0.0233, alpha: Alphas.Opaque)
    }
    
    struct TabBarColors{
        static let Selected = UIColor.white
        static let NotSelected = UIColor.black
    }
    
    struct OverlayColor {
        static let SemiTransparentBlack = UIColor.black.withAlphaComponent(Alphas.Transparent)
        static let SemiOpaque = UIColor.black.withAlphaComponent(Alphas.SemiOpaque)
        static let demoOverlay = UIColor.black.withAlphaComponent(0.6)
    }
    
    struct BasicColors{
        static let white = UIColor.white
        static let black = UIColor.black
    }
    
    struct ThemeColor {
        static let Yellow = UIColor(red: 252/255, green: 204/255, blue: 0, alpha: Alphas.Opaque)
        
    }
}
