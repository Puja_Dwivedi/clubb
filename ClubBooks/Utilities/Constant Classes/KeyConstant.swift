//
//  KeyConstant.swift
//  BudSeeker
//
//  Created by cis on 21/11/17.
//  Copyright © 2018 CIS. All rights reserved.
//

import Foundation

struct KEY {
    
    static let DeviceType = "Ios"
    
    
    struct UserDefaults {
        static let k_App_Running_FirstTime = "userRunningAppFirstTime"
        static let User = "normalUserData"
        static let BusinessUser = "businessUserData"
        static let CurrentUserType = "currentUserType"
        static let userDetails = "userDetails"
        static let selectedLanguage = "selectedLanguage"
        
    }
    
    struct Headers {
        static let Authorization = "Authorization"
        static let ContentType = "Content-Type"
    }
   
    struct ErrorMessage{
        static let listNotFound = "ERROR_LIST_NOT_FOUND"
        static let validationError = "ERROR_VALIDATION"
    }
    
    struct ServiceKeys {
        static let Error = "error"
        static let Status = "success"
        static let ErrorMessage = "error_message"
        static let Data = "data"
        static let Response = "response"
        static let Message = "message"
        static let HasNext = "has_next"
        static let Limit = "limit"
        static let PageNo = "page_no"
        static let PageCount = "total_page"
    }
    
    struct DeviceDetails {
        static let DeviceType = "device_type"
        static let DeviceID = "device_id"
        static let DeviceKey = "device_token"
    }
    struct HomeFeedKeys{
        static let id = "id"
        static let UserID = "user_id"
        static let Video_title = "title"
        static let Video_image = "image"
        static let Video_status = "status"
        static let Video_URL = "video_name"
        static let CommentCount = "comment_count"
        static let LikeCount = "like_count"
        static let isLikedCount = "is_liked_count"
        static let ViewCount = "view_count"
        static let Rating = "average-rating"
        static let Price = "price_tag"
        
    }
    
    struct SuggestedUserDataKeys{
        static let id = "id"
        static let User_name = "user_name"
        static let User_image = "user_image"
        static let Followers_Count = "followers_count"
    }
    
    struct RecentVideoUserList{
        static let id = "id"
        static let averageRating = "average-rating"
        static let image = "image"
        static let price = "price"
        static let status = "status"
        static let title = "title"
        static let userDetail = "user"
        static let userID = "user_id"
        static let videoImage = "video_name"
    }
    
    struct User {
        static let followersCount = "followers_count"
        static let id = "id"
        static let userIamge = "user_image"
        static let userName = "user_name"
    }
    
    struct UserProfile {
        static let accessToken = "msg"
        static let id = "id"
        static let address = "address"
        static let ageGroup = "age_group"
        static let cityName = "city_name"
        static let CommentCount = "comments_count"
        static let countryID = "country_id"
        static let email = "email"
        static let followeesCount = "followees_count"
        static let followersCount = "followers_count"
        static let gender = "gender"
        static let isComment = "is_comment"
        static let name = "name"
        static let totalAmount = "total_amount"
        static let userImage = "user_image"
        static let userName = "user_name"
        static let userRole = "user_role"
        static let isFollowed = "is_follow_count"
    }
    
    struct Video {
        static let id = "id"
        static let averageRating = "average-rating"
        static let videoImage = "image"
        static let videoPrice = "price"
        static let videoStatus = "status    "
        static let videoTitle = "title"
        static let videoUserID = "user_id"
        static let videoURL = "video_name"
    }
    
    struct NotificationsKeys {
        static let id = "id"
        static let notifiedBy = "notify_by"
        static let notifiedTo = "notify_to"
        static let postID = "post_id"
        static let message = "message"
        static let userID = "id"
        static let userName = "user_name"
        static let userImage = "user_image"
        static let notificationsTime = "created_at"
    }
    
    struct CommentsKeys {
        static let id = "id"
        static let userId = "user_id"
        static let videoId = "video_id"
        static let commentText = "comment_text"
        static let userName = "user_name"
        static let userImage = "user_image"
    }
    
    struct Social {
        static let SocialId = "social_id"
        static let SocialType = "social_type"
        static let FacebookId = "facebook_id"
        static let EmailAddress = "email_address"
    }
    
    struct Login
    {
        static let Email = "email_address"
        static let Password = "password"
        static let Badges = "badges"
        static let LoginSessionKey = "login_session_key"
        static let Name = "name"
    }
    
    struct SignUp
    {
        static let UserName = "user_name"
        static let Name = "name"
        static let EmailId = "email"
        static let Password = "password"
        static let TermsAndCondition = "t&c"
        static let City = "city"
        static let AgeGroup = "age_group"
        static let Gender = "gender"
        
    }
    
    
    struct ForgotPassword
    {
        static let EmailAddress = "email_address"
    }
    
    struct UserType
    {
        static let User_type = "user_type"
        static let User = "USER"
        static let Business = "BUSINESS"
    }
    
    struct NormalUserDetails
    {
        static let UserId = "id"
        static let Name = "name"
        static let EmailId = "email"
        static let FavBook = "fav_book"
        static let Location = "location"
        static let Mobile = "mobile"
        static let UserName = "username"
        static let SchoolName = "school_name"
        static let Token = "token"
        static let Gender = "gender"
        
        
    }
    
    struct ChangePassword {
        static let UserId = "user_id"
        static let CurrentPassword = "current_password"
        static let NewPassword = "new_password"
    }
    
    struct ContactUs {
        static let UserType = "user_type"
        static let Name = "name"
        static let ContactEmail = "contact_email"
        static let ReceiveEmail = "receive_email"
        static let Subject = "subject"
        static let ContactMessage = "contact_message"
    }
    
    
    
    struct FindDispensary{
        static let  Latitude = "latitude"
        static let  Longitude = "longitude"
        static let  Radius = "radius"
        static let  SearchType = "search_type"
    }
    
    struct FindProduct{
        static let  UserId = "user_id"
        static let  ProductText = "product_text"
    }
    
    struct DispensaryResult{
        static let LatLongOnApp = "lat_long_app"
        static let LatLongOnGoogle = "lat_long_google"
        
        static let Address = "address"
        static let Distance = "distance"
        static let Lat = "lat"
        static let Lng = "lng"
        static let Name = "name"

        static let GooglePlaceId = "place_id"
    }
    
    
    struct DispensaryRating{
        static let SenderId = "sender_id"
        static let ReceiverId = "receiver_id"
        static let RateCount = "rate_count"
    }
    
    struct GoogleAddMobKey{
        static let AddMobAppId = "ca-app-pub-8612965669897002~1331605650"
        static let AddMobBannerId1 = "ca-app-pub-8612965669897002/2397075624"
        static let AddMobBannerId2 = "ca-app-pub-8612965669897002/4995318802"
    }
    
    
    /*
     "lat"= "22.7243999",
     "lng"= "75.86453030000007",
     "business_id"= 3,
     "name"= "Rv014",
     "email"= "Rv01@gmail.com",
     "address"= "Indore GPO, Indore, Madhya Pradesh, India",
     "business_name"= "",
     "opening_time"= "",
     "closing_time"= "",
     "profile_rating"= "0",
     "contact_number"= "1234567890",
     "profile_image"= "",
     "distance"= 0.533733582408
    */
}
