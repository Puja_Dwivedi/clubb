//
//  CurveView.swift
//  ClubBooks
//
//  Created by cis on 21/11/17.
//  Copyright © 2018 CIS. All rights reserved.
//

import UIKit

class CurveView: UIView {
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
//        self.layer.cornerRadius = 25.0
//        self.clipsToBounds = true
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 0.5
    }

}
