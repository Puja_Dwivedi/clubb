//
//  KeyConstants.swift
//  ClubBooks
//
//  Created by cis on 09/06/23.
//  Copyright © 2023 CIS. All rights reserved.
//

import Foundation


struct LangId {
    static let english = "en"
    static let spanish = "es"
}
