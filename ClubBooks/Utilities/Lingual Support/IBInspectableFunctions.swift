 //
//  IBInspectableFunctions.swift
//  L&D Digital
//
//  Created by cis on 12/09/17.
//  Copyright © 2018 cis. All rights reserved.
//

import Foundation
import UIKit
import StoreKit
import Photos
class  ManageLocalization {
    static var deviceLang = ""
    class func getdeviceLangBundle()-> Bundle{
        // To get Language of device ...
        var path : String = String()

        if deviceLang.isEmpty {
            if let preferredLanguage = NSLocale.preferredLanguages[0] as String? {
                 let language = (preferredLanguage as NSString).substring(to: 2)
                deviceLang = language
            }
        }
            path = Bundle.main.path(forResource: deviceLang, ofType: "lproj")!
        return Bundle(path: path)!
    }
    
    class func getLocalaizedString(key:String)-> String{
        return NSLocalizedString(key, tableName: nil, bundle: getdeviceLangBundle(), value: "", comment: "")
    }
}
 extension UIViewController {
    func topMostViewController() -> UIViewController {
        if self.presentedViewController == nil {
            return self
        }
        if let navigation = self.presentedViewController as? UINavigationController {
            return navigation.visibleViewController!.topMostViewController()
        }
        if let tab = self.presentedViewController as? UITabBarController {
            if let selectedTab = tab.selectedViewController {
                return selectedTab.topMostViewController()
            }
            return tab.topMostViewController()
        }
        return self.presentedViewController!.topMostViewController()
    }
     func alertWithTextField(title: String? = nil, message: String? = nil, placeholder: String? = nil, cancelTitle : String, okTitle : String, completion: @escaping ((String) -> Void) = { _ in })
     {
         let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
         alert.addTextField() { newTextField in
             newTextField.placeholder = placeholder
         }
         alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in completion("") })
         alert.addAction(UIAlertAction(title: "Ok", style: .default) { action in
             if
                 let textFields = alert.textFields,
                 let tf = textFields.first,
                 let result = tf.text
             { completion(result) }
             else
             { completion("") }
         })
         self.navigationController?.present(alert, animated: true)
     }
 }
 extension UIApplication {
    func topMostViewController() -> UIViewController? {
        return self.keyWindow?.rootViewController?.topMostViewController()
    }
 }
 extension SKProduct {
    var localizedPrice: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        let USLocale = Locale.init(identifier: "en_US")
        formatter.locale = USLocale
        return formatter.string(from: price)!
    }
 }
extension UITextField {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
    }
   
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var localizedPlaceholder: String {
        get {
            return ""
        }
        set {
            self.placeholder = ManageLocalization.getLocalaizedString(key: newValue)
        }
    }
    
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
    
    @IBInspectable var localizedText: String {
        get {
            return ""
        }
        set {
            self.text = ManageLocalization.getLocalaizedString(key: newValue)
        }
    }
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
}
 enum VerticalLocation: String {
    case bottom
    case top
 }
 
 extension UIView {
    
    @IBInspectable var isShadow: Bool {
        get {
            return false
        }
        set {
            self.layer.shadowOffset = CGSize(width: CGFloat(0.0), height: CGFloat(0.0))
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowRadius = 3
            self.layer.shadowOpacity = 0.35
            self.layer.masksToBounds = false;
            self.clipsToBounds = false;
        }
    }
    
    
    func showShadow() {
        self.layer.shadowOffset = CGSize(width: CGFloat(2.0), height: CGFloat(5.0))
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 0.25
        self.layer.masksToBounds = false;
        self.clipsToBounds = false;
    }
    
    
    func addShadowAround(layer : CALayer)
    {
        DispatchQueue.main.async {
        let shadowSize : CGFloat = 2.0
        let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
                                                   y: -shadowSize / 2,
                                                   width: layer.frame.size.width + (shadowSize * 2),
                                                   height: layer.frame.size.height + shadowSize))
        layer.masksToBounds = false
        
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        layer.shadowOpacity = 0.3
        layer.shadowPath = shadowPath.cgPath
        }
    }
    
    func addShadow(location: VerticalLocation, color: UIColor = .black, opacity: Float = 0.5, radius: CGFloat = 5.0) {
        switch location {
        case .bottom:
            addShadow(offset: CGSize(width: 0, height: 10), color: color, opacity: opacity, radius: radius)
        case .top:
            addShadow(offset: CGSize(width: 0, height: -10), color: color, opacity: opacity, radius: radius)
        }
    }
    
    func addShadow(offset: CGSize, color: UIColor = .black, opacity: Float = 0.5, radius: CGFloat = 5.0) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = offset
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = radius
    }
    
 }
 
extension UITextView {
    
    @IBInspectable var localizedText: String {
        get {
            return ""
        }
        set {
            self.text = ManageLocalization.getLocalaizedString(key: newValue)
        }
    }
    
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
}

extension UIBarItem {
    
    @IBInspectable var localizedTitle: String {
        get {
            return ""
        }
        set {
            self.title = ManageLocalization.getLocalaizedString(key: newValue)
        }
    }
}

extension UILabel {
    
    @IBInspectable var localizedText: String {
        get {
            return ""
        }
        set {
            self.text = ManageLocalization.getLocalaizedString(key: newValue)
        }
    }
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        
    }
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var isUnderLine : Bool {
        get {
            return false
        }
        set {
            let attributedString = NSAttributedString(string: self.text!)
            let textRange = NSMakeRange(0, attributedString.length)
            let underlinedMessage = NSMutableAttributedString(attributedString: attributedString)
            underlinedMessage.addAttribute(NSAttributedString.Key.underlineStyle,
                                           value:NSUnderlineStyle.single.rawValue,
                                           range: textRange)
            self.attributedText = underlinedMessage
        }
    }
    
    func getAttibutedStringWithTitle(txtTitle : String, txtText : String){
        let lblAttributedString = NSMutableAttributedString(string: "")
        var titleAttribute = [NSAttributedString.Key.font : UIFont.init(name: "Muli-Bold", size: 15.0)!, NSAttributedString.Key.foregroundColor : UIColor.black
            ] as [NSAttributedString.Key : Any]
        let strTitleAttribute = NSMutableAttributedString(string: txtTitle.localized, attributes:titleAttribute)
        titleAttribute = [
            NSAttributedString.Key.font : UIFont.init(name: "Muli", size: 15.0)!,
            NSAttributedString.Key.foregroundColor : UIColor.gray] as [NSAttributedString.Key : Any]
        let textAttribute = NSMutableAttributedString(string: " \(txtText)", attributes:titleAttribute)
        
        lblAttributedString.setAttributedString(strTitleAttribute)
        lblAttributedString.append(textAttribute)
        self.attributedText = lblAttributedString
    }
}

extension UINavigationItem {
    
    @IBInspectable var localizedTitle: String {
        get {
            return ""
        }
        set {
            self.title = ManageLocalization.getLocalaizedString(key: newValue)
        }
    }
}

 
extension UIButton {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        
    }
    
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }

    @IBInspectable var localizedTitle: String {
        get {
            return ""
        }
        set {
            self.setTitle(ManageLocalization.getLocalaizedString(key: newValue), for: UIControl.State.normal)
        }
    }
    func alignVertical(spacing: CGFloat = 6.0) {
        guard let imageSize = self.imageView?.image?.size,
            let text = self.titleLabel?.text,
            let font = self.titleLabel?.font
            else { return }
        self.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -imageSize.width, bottom: -(imageSize.height + spacing), right: 0.0)
        let labelString = NSString(string: text)
        let titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: font])
        self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacing), left: 0.0, bottom: 0.0, right: -titleSize.width)
        let edgeOffset = abs(titleSize.height - imageSize.height) / 2.0;
        self.contentEdgeInsets = UIEdgeInsets(top: edgeOffset, left: 0.0, bottom: edgeOffset, right: 0.0)
    }
    
    @IBInspectable var isUnderLine : Bool {
        get {
            return false
        }
        set {
            if self.title(for: .normal) != nil{
                let attributedString = NSAttributedString(string: self.title(for: .normal)!)
                let textRange = NSMakeRange(0, attributedString.length)
                let underlinedMessage = NSMutableAttributedString(attributedString: attributedString)
                underlinedMessage.addAttribute(NSAttributedString.Key.underlineStyle,
                                               value:NSUnderlineStyle.single.rawValue,
                                               range: textRange)
                self.setAttributedTitle(underlinedMessage, for: .normal)
            }
        }
    }
    
}

extension UISearchBar {
    
    @IBInspectable var localizedPrompt: String {
        get {
            return ""
        }
        set {
            self.prompt = ManageLocalization.getLocalaizedString(key: newValue)
        }
    }
    
    @IBInspectable var localizedPlaceholder: String {
        get {
            return ""
        }
        set {
            self.placeholder = ManageLocalization.getLocalaizedString(key: newValue)
        }
    }
}

extension UISegmentedControl {
    
    @IBInspectable var localized: Bool {
        get {
            return true
        }
        set {
            for index in 0..<numberOfSegments {
                let title = ManageLocalization.getLocalaizedString(key: titleForSegment(at: index)!)
                setTitle(title, forSegmentAt: index)
            }
        }
    }
}
 extension UICollectionViewCell {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
 }
}

 
 extension UIImageView {
    func CircularImageWith(color : CGColor, borderWidth : CGFloat){
        self.layer.cornerRadius = self.frame.size.height/2
        self.clipsToBounds = true
        self.layer.masksToBounds = true
        self.layer.borderColor = color
        self.layer.borderWidth = borderWidth
    }
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
    }
    
 }
 
extension UIImage {
    
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    func tintedWithLinearGradientColors(colorsArr: [CGColor]) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale);
        guard let context = UIGraphicsGetCurrentContext() else {
            return UIImage()
        }
        context.translateBy(x: 0, y: self.size.height)
        context.scaleBy(x: 1, y: -1)
        context.setBlendMode(.normal)
        let rect = CGRect.init(x: 0, y: 0, width: size.width, height: size.height)
        
        // Create gradient
        let colors = colorsArr as CFArray
        let space = CGColorSpaceCreateDeviceRGB()
        let gradient = CGGradient(colorsSpace: space, colors: colors, locations: nil)
        
        // Apply gradient
        context.clip(to: rect, mask: self.cgImage!)
        context.drawLinearGradient(gradient!, start: CGPoint(x: 0, y: 0), end: CGPoint(x: 0, y: self.size.height), options: .drawsAfterEndLocation)
        let gradientImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return gradientImage!
    }
    
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    func jpeg(_ jpegQuality: JPEGQuality) -> UIImage? {
        
        return UIImage.init(data: (jpegData(compressionQuality: jpegQuality.rawValue)!))
    }
}
extension PHAsset {

    func getURL(completionHandler : @escaping ((_ responseURL : URL?) -> Void)){
        if self.mediaType == .image {
            let options: PHContentEditingInputRequestOptions = PHContentEditingInputRequestOptions()
            options.canHandleAdjustmentData = {(adjustmeta: PHAdjustmentData) -> Bool in
                return true
            }
            self.requestContentEditingInput(with: options, completionHandler: {(contentEditingInput: PHContentEditingInput?, info: [AnyHashable : Any]) -> Void in
                completionHandler(contentEditingInput!.fullSizeImageURL as URL?)
            })
        } else if self.mediaType == .video {
            let options: PHVideoRequestOptions = PHVideoRequestOptions()
            options.version = .original
            PHImageManager.default().requestAVAsset(forVideo: self, options: options, resultHandler: {(asset: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable : Any]?) -> Void in
                if let urlAsset = asset as? AVURLAsset {
                    let localVideoUrl: URL = urlAsset.url as URL
                    completionHandler(localVideoUrl)
                } else {
                    completionHandler(nil)
                }
            })
        }
    }
}
 extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    var localized : String {
        return ManageLocalization.getLocalaizedString(key: self)
    }
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
 }
 extension UIDevice {
    var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
    var iPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    enum ScreenType: String {
        case iPhone4_4S = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhoneX = "iPhone X"
        case unknown
    }
    var screenType: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4_4S
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhoneX
        default:
            return .unknown
        }
    }
 }
 
 extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 { return "\(years(from: date))y"   }
        if months(from: date)  > 0 { return "\(months(from: date))M"  }
        if weeks(from: date)   > 0 { return "\(weeks(from: date))w"   }
        if days(from: date)    > 0 { return "\(days(from: date))d"    }
        if hours(from: date)   > 0 { return "\(hours(from: date))h"   }
        if minutes(from: date) > 0 { return "\(minutes(from: date))m" }
        if seconds(from: date) > 0 { return "\(seconds(from: date))s" }
        return ""
    }
    func offsetHours(from date: Date) -> String {
        return "\(hours(from: date))h \(minutes(from: date))m \(seconds(from: date))s"
    }
     
     func dayOfTheWeek() -> String? {
         let weekdays = [
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday"
         ]
         
         let calendar: NSCalendar = NSCalendar.current as NSCalendar
         let components: NSDateComponents = calendar.components(.weekday, from: self) as NSDateComponents
         return weekdays[components.weekday - 1]
     }
 }
 extension UITableView {
    func setEmptyView(title: String, message: String) {
        let emptyView = UIView(frame: CGRect(x: self.center.x, y: self.center.y, width: self.bounds.size.width, height: self.bounds.size.height))
        let titleLabel = UILabel()
        let messageLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont(name: "Poppins-Bold", size: 17)
        messageLabel.textColor = UIColor.lightGray
        messageLabel.font = UIFont(name: "Poppins-Regular", size: 14)
        emptyView.addSubview(titleLabel)
        emptyView.addSubview(messageLabel)
        titleLabel.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
        if title == "" {
            messageLabel.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor).isActive = true
            messageLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
        }else{
            messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20).isActive = true
            messageLabel.leftAnchor.constraint(equalTo: emptyView.leftAnchor, constant: 20).isActive = true
            messageLabel.rightAnchor.constraint(equalTo: emptyView.rightAnchor, constant: -20).isActive = true
        }
        
        titleLabel.text = title
        messageLabel.text = message
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        // The only tricky part is here:
        self.backgroundView = emptyView
        self.separatorStyle = .none
    }
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}
