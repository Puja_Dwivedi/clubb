//
//  BaseViewController.swift
//  ClubBooks
//
//  Created by cis on 21/11/17.
//  Copyright © 2018 CIS. All rights reserved.
//

import UIKit
import MBProgressHUD
import UserNotifications
import FBSDKShareKit
import MessageUI
import Social
import CoreLocation
class BaseViewController: UIViewController, MFMailComposeViewControllerDelegate{
    var locationVC = LocationManagerVC()
    @IBOutlet var viewTitle : UIView?
    @IBOutlet var lblTitle : UILabel?
    @IBOutlet var btnSlideMenu : UIButton?
    @IBOutlet var btnRightSide : UIButton?
    @IBOutlet var imgBackground : UIImageView?
    var postURL = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationController?.navigationBar.showShadow()
        locationVC.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .authorizedAlways, .authorizedWhenInUse:
                locationVC.configureLocationManager()
                break
            case .restricted, .notDetermined, .denied:
                print(" Location Services are not enabled. ")
                break
            default:
                break
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    func showBackButtonWith(Image : UIImage) {
        navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: Image, style: .plain, target: self, action: #selector(btnBackClicked(sender:)))
    }
    func showBackButtononPresentedViewControllerWith(Image : UIImage) {
        navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: Image, style: .plain, target: self, action: #selector(btnBackToDismiss(_:)))
    }
    func setTitleWithLocalizedString(title : String){
        navigationItem.title = title.localized
    }
    func showlogoLeft(){
        if navigationItem.leftBarButtonItem != nil{
            navigationItem.leftBarButtonItem = nil
        }
        let btnlogo = UIButton.init(type: .custom)
        btnlogo.setBackgroundImage(UIImage(named: "btn.navigationlogo"), for: .normal)
        let stackview = UIStackView.init(arrangedSubviews: [btnlogo])
        stackview.distribution = .fillEqually
        stackview.axis = .horizontal
        stackview.alignment = .center
        stackview.spacing = 15
        let rightBarButton = UIBarButtonItem(customView: stackview)
        self.navigationItem.leftBarButtonItem = rightBarButton
    }
    func showLogoWithBackButon(){
        self.navigationController?.navigationBar.tintColor = UIColor.lightGray
        self.navigationItem.leftBarButtonItems = [UIBarButtonItem.init(image: #imageLiteral(resourceName: "btn.back"), style: .plain, target: self, action: #selector(btnBackClicked(sender:))), UIBarButtonItem.init(image: UIImage(named:"btn.navigationlogo")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: nil)]
    }
    func showSettingButton(){
         self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named:"icon.verticalDots")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(btnOpenSettingClicked(_:)))
    }
    // MARK: - Button Actions
    @IBAction func btnOpenSettingClicked( _ sender : UIButton){
        let settingVC : SettingVC = storyBoard.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
        settingVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(settingVC, animated: true)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if #available(iOS 15, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            self.navigationController?.navigationBar.isTranslucent = true  // pass "true" for fixing iOS 15.0 black bg issue
            self.navigationController?.navigationBar.tintColor = UIColor.lightGray // We need to set tintcolor for iOS 15.0
            appearance.shadowColor = .clear    //removing navigationbar 1 px bottom border.
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
        }
    }
    @IBAction func btnBackClicked(sender : UIButton)
    {
      self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnBackToDismiss( _ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:
    //MARK: Social Share Button Clicked
    @IBAction func btnFacebookShareClicked( _ Sender : UIButton){
//        let content = FBSDKShareLinkContent()
//        let contentUrl = self.postURL
//        content.contentURL = URL(string: contentUrl)
//        FBSDKShareDialog.show(from: self, with: content, delegate: self)
        let properties = [
                "og:title": "ClubBooks",
                "og:description": "",
                "og:image" : self.postURL,
                "og:url" : "",
            ]

        let object : ShareOpenGraphObject = ShareOpenGraphObject.init(properties: properties)

            // Create an action
        let action : ShareOpenGraphAction = ShareOpenGraphAction.init(type: "")
            action.actionType = "news.publishes"
        action.set(object, forKey: "article")
            

            // Create the content
        let content : ShareOpenGraphContent = ShareOpenGraphContent()
            content.action = action
            content.previewPropertyName = "article"
        ShareDialog.init(fromViewController: self, content: content, delegate: nil)
        
    }
    @IBAction func btnMessangerShareClicked( _ sender : UIButton){
        shareWithNativeShareExtension()
    }
    @IBAction func btnEmailClicked( _ sender : UIButton){
        let emailTitle = "ClubBooks"
        let mc: MFMailComposeViewController = MFMailComposeViewController()
        mc.mailComposeDelegate = self
        mc.setSubject(emailTitle)
        mc.setMessageBody(self.postURL, isHTML: false)
        if mc != nil{
            self.present(mc, animated: true) {
            }
        }
    }
    
    func mailComposeController(_ controller:MFMailComposeViewController, didFinishWith result:MFMailComposeResult, error:Error?) {
        switch result {
        case .cancelled:
            print("Mail cancelled")
        case .saved:
            print("Mail saved")
        case .sent:
            print("Mail sent")
        case .failed:
            print("Mail sent failure: \(String(describing: error?.localizedDescription))")
        }
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnTwitterClicked( _ sender : UIButton){
       shareWithNativeShareExtension()
    }
    
    @IBAction func btnSnapChatClicked( _ sender : UIButton){
        shareWithNativeShareExtension()
    }
     
    
    @IBAction func btnTiktokClicked( _ sender : UIButton){
        shareWithNativeShareExtension()
    }
    
    func shareWithNativeShareExtension(){
        let text = self.postURL
        
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
}

class LYTabBar: UITabBarController, UITabBarControllerDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationItem.hidesBackButton = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
    }
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
    }
}

extension BaseViewController : SharingDelegate{
    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
        
    }
    
    func sharer(_ sharer: Sharing, didFailWithError error: Error) {
        
    }
    
    func sharerDidCancel(_ sharer: Sharing) {
        
    }
    
    
    
    
}
