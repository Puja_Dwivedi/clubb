//
//  Constant.swift
//  Bonauf
//
//  Created by cis on 4/4/18.
//  Copyright © 2018 cis. All rights reserved.
//

import Foundation
import UIKit

public struct Constant {
    static let STATUS_BAR_COLOR : UIColor =  UIColor(red: 36.0/255.0, green: 115.0/255.0, blue: 166.0/255.0, alpha: 1.0)
    static let TAB_NORMAL_COLOR : UIColor =  UIColor(red: 131.0/255.0, green: 206.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    static let TAB_COLOR : UIColor =  UIColor(red: 254.0/255.0, green: 206.0/255.0, blue: 18.0/255.0, alpha: 1.0)
    static let LIGHT_BLUE_COLOR : UIColor =  UIColor(red: 246.0/255.0, green: 253.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    static let NAV_COLOR : UIColor = UIColor(red: 41.0/255.0, green: 128.0/255.0, blue: 185.0/255.0, alpha: 1.0)
    static let GREEN_COLOR : UIColor = UIColor(red: 134.0/255.0, green: 134.0/255.0, blue: 134.0/255.0, alpha: 1.0)
    static let YELLOW_COLOR : UIColor = UIColor(red: 238.0/255.0, green: 180.0/255.0, blue: 9.0/255.0, alpha: 1.0)
    static let VOILET_TEXT_COLOR : UIColor = UIColor(red: 77.0/255.0, green: 100.0/255.0, blue: 140.0/255.0, alpha: 1.0)
    static let ALERT_TITLE = "ClubBooks"
    static let ALERT_TEXT = "Oops! Something went wrong!\nPlease try again."
    static let ALERT_NO_INTERNET = "Internet connection not found, Please make sure that you are connected with internet."
    static let hudSize = CGSize(width: 90, height: 90)
    static let hudType = 29
    static let hudMessage = ""
    static let DEVICE = "1"
    static let PAGE_LIMIT = 20
    static let APP_OBJ = UIApplication.shared.delegate as! AppDelegate
    static let ENGLISH = "en"
    static let userDefault = UserDefaults.standard
    static let HEBREW = "he"
    static let ROBOTO_MED = "Roboto-Medium"
    
    static let GoogleClientKey = "454188419246-eggh0ab5jrb3u5eotqa5pr1ar04p1faf.apps.googleusercontent.com"
    static let GoogleApiKey = "AIzaSyANiD1JjW7wMLU1loNZ8ZX0EoUhIgESTeI"
    static let Base_URL = "https://clubbooks.kr.cisinlive.com/api/param"
}
