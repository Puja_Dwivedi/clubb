//
//  WebAPI.swift
//  Bonauf
//
//  Created by cis on 4/4/18.
//  Copyright © 2018 cis. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MBProgressHUD
import SystemConfiguration
public class WebAPI: NSObject {
    class func isInternetAvailable() -> Bool{
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
//        WebAPI_Testing.testing()
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    class func httpSessionRequest(viewController : UIViewController, url : String, params : [String : AnyObject], SuccessHandler: @escaping (_ result : JSON) -> Void, ErrorHandler : @escaping ( _ error : NSError) -> Void){
        
        if viewController.isKind(of: ViewController.self){
            MBProgressHUD.showAdded(to: viewController.view, animated: true)
        }
        let request = NSMutableURLRequest.init(url: URL.init(string: url)!)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if url != APPURL.getAccessToken {
            if url == APPURL.Login || url == APPURL.UserRegistration || url == APPURL.ForgotPasswordProfile || url == APPURL.FacebookLogin || url == APPURL.VerificationCode{
                request.setValue(UserAccessToken.sharedInstance.message, forHTTPHeaderField: "API-ACCESS-TOKEN")
            }else{
                request.setValue(UserAccessToken.sharedInstance.message, forHTTPHeaderField: "API-ACCESS-TOKEN")
                request.setValue("Bearer \(AppTheme.sharedInstance.currentUserData[0].Token!)", forHTTPHeaderField: "Authorization")
            }
        }
        if params.keys.count != 0{
            request.httpBody = try? JSONSerialization.data(withJSONObject: params)
        }
        request.httpMethod = "Post"
        if isInternetAvailable(){
            let task = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                if error != nil {
                    ErrorHandler(error as? NSError ?? NSError.init())
                    return
                }
                if let httpResponse = response as? HTTPURLResponse {
                    let parsedData: Any = try! JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                    if httpResponse.statusCode == 200 {
                        let json = try! JSON.init(data: data!)
                        if json.dictionary!["status"] == 0 {
                            if json.dictionary!["msgCode"] == 407 ||  json.dictionary!["msgCode"] == 401 {
                                AppTheme.sharedInstance.currentUserData.removeAll()
                                AppTheme.sharedInstance.saveUserDataToDefault()
                                AppTheme.sharedInstance.LogoutFromApplication(viewController: viewController, json: json)
                                DispatchQueue.main.async {
                                    MBProgressHUD.hide(for: viewController.view, animated: true)
                                }
                            }else{
                                SuccessHandler(json)
                                DispatchQueue.main.async {
                                    MBProgressHUD.hide(for: viewController.view, animated: true)
                                }
                            }
                        }else{
                            SuccessHandler(json)
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: viewController.view, animated: true)
                            }
                        }
                    }
                }
            })
            task.resume()
        }
    }
    
    class func httpJSONRequest(viewController: UIViewController, url:String,params:[String:AnyObject],SuccessHandler:@escaping (_ result: JSON) ->Void ,Errorhandler:@escaping ( _ error:NSError) -> Void){
            if viewController.isKind(of: ViewController.self) || viewController.isKind(of: LoginVC.self) || viewController.isKind(of: SignUpVC.self) || viewController.isKind(of: CreateMeetupsVC.self) || viewController.isKind(of: EditProfileVC.self){
                MBProgressHUD.showAdded(to: viewController.view, animated: true)
            }
            var headers = [String : String]()
            if url != APPURL.getAccessToken {
                if url == APPURL.Login || url == APPURL.UserRegistration || url == APPURL.ForgotPasswordProfile || url == APPURL.FacebookLogin || url == APPURL.VerificationCode{
                    headers = ["API-ACCESS-TOKEN" : UserAccessToken.sharedInstance.message]
                }else{
                    headers = ["API-ACCESS-TOKEN" : UserAccessToken.sharedInstance.message,
                               "Authorization" : "Bearer \(AppTheme.sharedInstance.currentUserData[0].Token!)"]
                }
            }
            if isInternetAvailable(){
                Alamofire.request(URL.init(string: url)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).validate().responseJSON { response in
                    
                    switch response.result{
                    case .success:
                        if let value = response.result.value {
                            let json = JSON(value)
                            if json.dictionary!["status"] == 0 {
                                if json.dictionary!["msgCode"] == 407 ||  json.dictionary!["msgCode"] == 401 {
                                    AppTheme.sharedInstance.currentUserData.removeAll()
                                    AppTheme.sharedInstance.saveUserDataToDefault()
                                    AppTheme.sharedInstance.LogoutFromApplication(viewController: viewController, json: json)
                                    MBProgressHUD.hide(for: viewController.view, animated: true)
                                }else{
                                    SuccessHandler(json)
                                    MBProgressHUD.hide(for: viewController.view, animated: true)
                                }
                            }else{
                                SuccessHandler(json)
                                MBProgressHUD.hide(for: viewController.view, animated: true)
                            }
                        }
                        break
                    case .failure(let error):
                        Errorhandler(error as NSError)
                        MBProgressHUD.hide(for: viewController.view, animated: true)
                        break
                    }
                }
            }else{
                Manager.sharedInstance.showAlert(viewController, message:"no_Internet_Connection".localized)
                MBProgressHUD.hide(for: viewController.view, animated: true)
            }
            
        }
    class func multipartHttpRequestWithImageArray(viewController:UIViewController,imageKey: String, arrayImages : [UIImage],  url:String,params:[String:AnyObject],SuccessHandler:@escaping (_ result: JSON) ->Void,Errorhandler:@escaping ( _ error:NSError) -> Void) {
        MBProgressHUD.showAdded(to: viewController.view, animated: true)
        var headers = [String : String]()
        if url != APPURL.getAccessToken {
            headers = ["API-ACCESS-TOKEN" : UserAccessToken.sharedInstance.message]
            if url == APPURL.Login || url == APPURL.UserRegistration || url == APPURL.ForgotPasswordProfile {
                headers = ["API-ACCESS-TOKEN" : UserAccessToken.sharedInstance.message]
            }else{
                headers = ["API-ACCESS-TOKEN" : UserAccessToken.sharedInstance.message,
                           "Authorization" : String(format : "Bearer %@",AppTheme.sharedInstance.currentUserData[0].Token)]
            }
        }
        if isInternetAvailable(){
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key,value) in params {
                    if key == imageKey {
                        var i = 0
                        for (image) in arrayImages {
                            if  let imageData = image.jpegData(compressionQuality: 0.5) {
                                multipartFormData.append(imageData, withName: key, fileName: String(format : "bonauf%d.jpg",i), mimeType: "image/jpg")
                                i = i+1
                            }
                        }
                    } else {
                        multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }
                }
                
            }, to: url,headers:headers,
                             encodingCompletion: { (encodingResult) in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON(completionHandler: { response in
                        if let value = response.result.value {
                            
                            let json = JSON(value)
                            SuccessHandler(json)
                            MBProgressHUD.hide(for: viewController.view, animated: true)
                            
                            
                        } else {
                            Errorhandler(response.error! as NSError)
                            MBProgressHUD.hide(for: viewController.view, animated: true)
                            
                        }
                    })
                    upload.responseString(completionHandler: { (response) in
                        print("Result String \(response)")
                        MBProgressHUD.hide(for: viewController.view, animated: true)
                        
                    })
                case .failure(let encodingError):
                    ManagerSharedInstance.showAlert(viewController)
                    Errorhandler(encodingError as NSError)
                    print("Encoding Result was FAILURE \(encodingError)")
                    MBProgressHUD.hide(for: viewController.view, animated: true)
                }
            })
            
        }else{
            MBProgressHUD.hide(for: viewController.view, animated: true)
            Manager.sharedInstance.showAlert(viewController, message:"no_Internet_Connection".localized)
        }
    }
}
