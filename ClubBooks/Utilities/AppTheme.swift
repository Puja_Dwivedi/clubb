 //
//  AppTheme.swift
//  ClubBooks
//
//  Created by cis on 21/11/17.
//  Copyright © 2018 CIS. All rights reserved.
//

import UIKit
import SwiftyJSON
let storyBoard = UIStoryboard(name: "Main", bundle: nil)
let SecondStoryBoard = UIStoryboard.init(name: "Second", bundle: nil)
let ApplicationTitle = "ClubBooks"


class AppTheme: NSObject {
    
    public var strDeviceToken :String = String()
    public var fcmToken = ""
    // for hidding the notification on the same chat window.
    public var currentChatID = ""
    public var isPresentRatngViewForThisSession = false
    //--- User session management --
    public let ClubBooksUserDefaults = UserDefaults.standard
    public lazy var currentUserData = [UserDetails]()
    // MARK: - Shared Instance
    static let sharedInstance: AppTheme = {
        let instance = AppTheme()
        // setup code
        return instance
    }()
    
    
    
    lazy var regDict = [String : AnyObject]()
    lazy var emailNotifications = true
    
    
    func makeImageToCircle(image : UIImageView, radiusValue : CGFloat)
    {
        image.clipsToBounds = true
        image.layer.cornerRadius = radiusValue
    }
    
    func saveUserDataToDefault()
    {
        let dictCurrentUser = NSMutableDictionary()
            let data : NSData = NSKeyedArchiver.archivedData(withRootObject: AppTheme.sharedInstance.currentUserData) as NSData
            dictCurrentUser.setValue(data, forKey: KEY.UserDefaults.User)
            AppTheme.sharedInstance.ClubBooksUserDefaults.setValue(dictCurrentUser, forKey: KEY.UserDefaults.userDetails)
            AppTheme.sharedInstance.ClubBooksUserDefaults.synchronize()
        
    }
    
    func getUserDataFromDefault()
    {
        let dictUserDetails = RV_CheckDataType.getDictionary(anyDict: AppTheme.sharedInstance.ClubBooksUserDefaults.value(forKey: KEY.UserDefaults.userDetails) as AnyObject)
            if let data : NSData = dictUserDetails.value(forKey: KEY.UserDefaults.User) as? NSData
            {
                if let array : [UserDetails] = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as? [UserDetails]
                {
                    AppTheme.sharedInstance.currentUserData = array
                }
            }
    }
    
    func setupCollectionView(collectionAds : UICollectionView)
    {
        let screenSize = UIScreen.main.bounds
        let SCREEN_WIDTH = screenSize.size.width
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            // It's an iPhone
            layout.itemSize = CGSize(width: SCREEN_WIDTH, height: 100 )
            break
        case .pad:
            // It's an iPad
            //layout.sectionInset = UIEdgeInsets(top: 0, left: 35, bottom: 0, right: 35)
            break
        case .unspecified:
            // Uh, oh! What could it be?
            break
        default:
            break
        }
        
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = 0;
        layout.scrollDirection = .horizontal
        collectionAds.collectionViewLayout = layout
    }
    
    func ageRange(ageValue : NSInteger) -> String {
        switch ageValue {
        case 1:
            return "Under-18"
        case 2:
            return "18-24"
        case 3:
            return "25-54"
        case 4:
            return "55-64"
        case 5:
            return "65-and over"
        default:
            return ""
        }
    }
    
    func GetCategoryOfAge(ageValue : NSInteger) -> NSInteger {
        switch ageValue {
        case 1...18:
            return 1
        case 19...24:
            return 2
        case 25...54:
            return 3
        case 55...64:
            return 4
        case 65...120:
            return 5
        default:
            return 2
        }
    }
    
    func ratingValue(starNumber: Double) -> Double
    {
        let strValue = "\(starNumber)"
        let arr = strValue.components(separatedBy: ".")
        var strNewValue = ""
        if arr.count == 2
        {
            let mainValue = RV_CheckDataType.getInteger(anyString: arr[0] as AnyObject)
            let pointValue = starNumber - Double(mainValue)
            if pointValue < 0.5
            {
                //0.0
                strNewValue = "\(arr[0]).0"
            }
            else if pointValue >= 0.5 && pointValue <= 0.7
            {
                //0.5
                strNewValue = "\(arr[0]).5"
            }
            else if pointValue > 0.7
            {
                //1.0 add it
                let intRating = RV_CheckDataType.getInteger(anyString: arr[0] as AnyObject)
                strNewValue = "\(intRating + 1).0"
            }
        }
        return RV_CheckDataType.getDouble(anyString: strNewValue as AnyObject)
    }
    
    func shake(layer: CALayer)
    {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    func showAlert(message : String, viewcontroller : UIViewController){
        let alert : UIAlertController = UIAlertController(title: "ClubBooks", message: message, preferredStyle: .alert)
        
        let YesAction = UIAlertAction(title: "alert.ok".localized, style: .default, handler: { (action) in
        })
        alert.addAction(YesAction)
        viewcontroller.present(alert, animated: true, completion: {
        })
    }
    
    func LogoutFromApplication(viewController : UIViewController, json : JSON){
        let alert = UIAlertController(title: Constant.ALERT_TITLE, message: json.dictionary!["msg"]?.stringValue , preferredStyle: .alert)
        
        alert.addAction(UIAlertAction.init(title: ManageLocalization.getLocalaizedString(key: "btn.title.signout"), style: .cancel, handler: { (action) in
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appdelegate.logoutUser()
        }))
        viewController.present(alert, animated: true, completion: nil)
    }
    
    func moveToSafari(viewController : UIViewController, postURL : inout String){
        let postURLToken = "\(postURL)/\(AppTheme.sharedInstance.currentUserData[0].Token!)"
        let alert = UIAlertController(title: Constant.ALERT_TITLE, message : "alert.move to Website".localized, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction.init(title: ManageLocalization.getLocalaizedString(key: "alert.ok"), style: .default, handler: { (action) in
            if let url = URL(string: postURLToken) {
                UIApplication.shared.open(url)
            }
        }))
        alert.addAction(UIAlertAction.init(title: "alert.cancel".localized, style: .destructive, handler: { (action) in
            
        }))
        viewController.present(alert, animated: true, completion: nil)
    }
    func dropShadow(view : UIView, scale: Bool = true){
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 3
        
        view.layer.shadowPath = UIBezierPath(rect: view.bounds).cgPath
        view.layer.shouldRasterize = true
        view.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    func convertStringToDate(from : String) -> NSDate{
        let dateFormate = DateFormatter()
        dateFormate.timeZone = TimeZone(identifier:"UTC")
        dateFormate.dateFormat = "yyyy-MM-ddHH:mm:ss"
        let date = dateFormate.date(from: from)
        return date! as NSDate
    }
    func convertDateToString(from : NSDate) -> String{
        let dateFormate = DateFormatter()
        dateFormate.timeZone = TimeZone(identifier:"UTC")
        dateFormate.dateFormat = "yyyy-MM-ddHH:mm:ss"
        let date = dateFormate.string(from: from as Date)
        return date
    }
    func getTimeDifferencefromDate(strDate : String) -> String{
        
        let currentDate = AppTheme.sharedInstance.convertStringToDate(from: strDate) as Date
        let now = Date()
        let strDifference = now.offset(from: currentDate)
        return strDifference
    }
    func getTimeDifferenceBookReservation(strDate : String) -> String{
        let currentDate = AppTheme.sharedInstance.convertStringToDate(from: strDate) as Date
        let now = Date()
        let strDifference = now.offsetHours(from: currentDate)
        return strDifference
    }
    func get24HoursTimeFormat(strTime : String) -> String{
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "hh:mm a"
        let time = dateFormat.date(from: strTime)
        if time != nil{
            dateFormat.dateFormat = "HH:MM"
            return dateFormat.string(from: time!)
        }
        return ""
    }
    func getDateFromString(strDate : String) -> String{
        let dateFormat = DateFormatter()
        dateFormat.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormat.date(from: strDate)
        if date != nil {
            if ManageLocalization.deviceLang == "en"{
                dateFormat.dateFormat = "MM/dd/yyyy"
            }else{
                dateFormat.dateFormat = "dd/MM/yyyy"
            }
            dateFormat.timeZone = NSTimeZone.local
            return dateFormat.string(from: date!)
        }else{
            return strDate
        }
    }
    
    func getFormatedDateForMeetup(strDate : String) -> String{
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormat.date(from: strDate)
        if date != nil {
            if ManageLocalization.deviceLang == "en"{
                dateFormat.dateFormat = "MMM dd, YYYY"
            }else{
                dateFormat.dateFormat = "dd MMM, YYYY"
            }
            return "\(date!.dayOfTheWeek() ?? "")  \(dateFormat.string(from: date!))"
        }else{
            return strDate
        }
    }
    func setMeetupDateForm(strDate : String) -> String{
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        let date = dateFormat.date(from: strDate)
        if date != nil {
            dateFormat.dateFormat = "MM-dd-YYYY"
            return dateFormat.string(from: date!)
        }else{
            return strDate
        }
    }
    func getTimeFromString(strTime : String) -> String{
        let dateFormat = DateFormatter()
        dateFormat.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        dateFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let time = dateFormat.date(from: strTime)
        if time != nil {
            dateFormat.dateFormat = "hh:mm a"
            dateFormat.timeZone = NSTimeZone.local
            return dateFormat.string(from: time!)
        }else{
            return ""
        }
    }
}

